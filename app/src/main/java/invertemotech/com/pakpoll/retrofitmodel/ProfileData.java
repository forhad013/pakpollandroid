
package invertemotech.com.pakpoll.retrofitmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ProfileData {

    @SerializedName("questions")
    @Expose
    private ArrayList<Question> questions = new ArrayList<Question>();
    @SerializedName("totalpollscreated")
    @Expose
    private Integer totalpollscreated;
    @SerializedName("totalvotes")
    @Expose
    private Integer totalvotes;
    @SerializedName("following")
    @Expose
    private Integer following;
    @SerializedName("totalfollowers")
    @Expose
    private Integer totalfollowers;


    public String getProfileuserpic() {
        return profileuserpic;
    }

    public void setProfileuserpic(String profileuserpic) {
        this.profileuserpic = profileuserpic;
    }

    @SerializedName("profileusername")
    @Expose
    private String profileusername;
    @SerializedName("profileuserpic")
    @Expose
    private String profileuserpic;

    /**
     * 
     * @return
     *     The questions
     */
    public ArrayList<Question> getQuestions() {
        return questions;
    }

    /**
     * 
     * @param questions
     *     The questions
     */
    public void setQuestions(ArrayList<Question> questions) {
        this.questions = questions;
    }

    /**
     * 
     * @return
     *     The totalpollscreated
     */
    public Integer getTotalpollscreated() {
        return totalpollscreated;
    }

    /**
     * 
     * @param totalpollscreated
     *     The totalpollscreated
     */
    public void setTotalpollscreated(Integer totalpollscreated) {
        this.totalpollscreated = totalpollscreated;
    }

    /**
     * 
     * @return
     *     The totalvotes
     */
    public Integer getTotalvotes() {
        return totalvotes;
    }

    /**
     * 
     * @param totalvotes
     *     The totalvotes
     */
    public void setTotalvotes(Integer totalvotes) {
        this.totalvotes = totalvotes;
    }

    /**
     * 
     * @return
     *     The following
     */
    public Integer getFollowing() {
        return following;
    }

    /**
     * 
     * @param following
     *     The following
     */
    public void setFollowing(Integer following) {
        this.following = following;
    }

    /**
     * 
     * @return
     *     The totalfollowers
     */
    public Integer getTotalfollowers() {
        return totalfollowers;
    }

    /**
     * 
     * @param totalfollowers
     *     The totalfollowers
     */
    public void setTotalfollowers(Integer totalfollowers) {
        this.totalfollowers = totalfollowers;
    }



    public String getProfileusername() {
        return profileusername;
    }

    public void setProfileusername(String profileusername) {
        this.profileusername = profileusername;
    }

}
