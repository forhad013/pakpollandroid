
package invertemotech.com.pakpoll.retrofitmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegData {

    @SerializedName("userid")
    @Expose
    private Integer userid;





    /**
     * 
     * @return
     *     The userid
     */
    public Integer getUserid() {
        return userid;
    }

    /**
     * 
     * @param userid
     *     The userid
     */
    public void setUserid(Integer userid) {
        this.userid = userid;
    }



}
