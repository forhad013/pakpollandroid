
package invertemotech.com.pakpoll.retrofitmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HotPollData {

    @SerializedName("pollid")
    @Expose
    private Integer pollid;
    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("name")
    @Expose
    private String name;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    @SerializedName("question")
    @Expose
    private String question;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("topics")
    @Expose
    private String topics;
    @SerializedName("commented_by_name")
    @Expose
    private String commentedByName;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("totalcomments")
    @Expose
    private Integer totalcomments;

    /**
     * 
     * @return
     *     The pollid
     */
    public Integer getPollid() {
        return pollid;
    }

    /**
     * 
     * @param pollid
     *     The pollid
     */
    public void setPollid(Integer pollid) {
        this.pollid = pollid;
    }

    /**
     * 
     * @return
     *     The total
     */
    public Integer getTotal() {
        return total;
    }

    /**
     * 
     * @param total
     *     The total
     */
    public void setTotal(Integer total) {
        this.total = total;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The picture
     */
    public String getPicture() {
        return picture;
    }

    /**
     * 
     * @param picture
     *     The picture
     */
    public void setPicture(String picture) {
        this.picture = picture;
    }

    /**
     * 
     * @return
     *     The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * 
     * @param createdAt
     *     The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 
     * @return
     *     The topics
     */
    public String getTopics() {
        return topics;
    }

    /**
     * 
     * @param topics
     *     The topics
     */
    public void setTopics(String topics) {
        this.topics = topics;
    }

    /**
     * 
     * @return
     *     The commentedByName
     */
    public String getCommentedByName() {
        return commentedByName;
    }

    /**
     * 
     * @param commentedByName
     *     The commented_by_name
     */
    public void setCommentedByName(String commentedByName) {
        this.commentedByName = commentedByName;
    }

    /**
     * 
     * @return
     *     The comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * 
     * @param comment
     *     The comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * 
     * @return
     *     The totalcomments
     */
    public Integer getTotalcomments() {
        return totalcomments;
    }

    /**
     * 
     * @param totalcomments
     *     The totalcomments
     */
    public void setTotalcomments(Integer totalcomments) {
        this.totalcomments = totalcomments;
    }

}
