
package invertemotech.com.pakpoll.retrofitmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfileResponse {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("data")
    @Expose
    private ProfileData profileData;
    @SerializedName("message")
    @Expose
    private String message;

    /**
     * 
     * @return
     *     The error
     */
    public Boolean getError() {
        return error;
    }

    /**
     * 
     * @param error
     *     The error
     */
    public void setError(Boolean error) {
        this.error = error;
    }

    /**
     * 
     * @return
     *     The profileData
     */
    public ProfileData getProfileData() {
        return profileData;
    }

    /**
     * 
     * @param profileData
     *     The profileData
     */
    public void setProfileData(ProfileData profileData) {
        this.profileData = profileData;
    }

    /**
     * 
     * @return
     *     The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * 
     * @param message
     *     The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

}
