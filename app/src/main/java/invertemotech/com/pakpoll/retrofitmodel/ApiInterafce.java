package invertemotech.com.pakpoll.retrofitmodel;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by Mac on 8/11/16.
 */
public interface ApiInterafce {


    public static final String SERVER_DOMAIN = "http://goodtill.rskyinvestment.com/api/v1.1/";
    String refreshUrl ="v1/refresh/";


//    //categoryList
//    @GET("customcategories/{store_id}")
//    Call<CategoryList> getCatgoryList(@Path("store_id") String store_id, @Query("appid") String appid);
//
//
//
//    //categoryEdit
//    //{@updatebody
//    //private String appid;
//    //private String storeid;
//    //private String title;
//    //private String created_by;
//    //}
//    @POST("customcatupdate/{cat_id}")
//    Call<UpdateResponse> updateCatgory(@Path("cat_id") String cat_id, @Body UpdateBodyForCategory body);
//
//
//
    //registration
    @POST("register")
    Call<RegResponse> regitser(@Query("appid") String appid, @Query("name") String name,
                               @Query("gender") String gender, @Query("age") String age,
                               @Query("province") String province, @Query("email") String email,
                               @Query("password") String password, @Query("gcm") String gcm);


    @POST("login")
    Call<LoginResponse> login(@Query("appid") String appid, @Query("email") String email,
                               @Query("password") String password, @Query("gcm") String gcm);


        //topicsList
    @GET("topics")
    Call<TopicsResponse> getTopicsList(@Query("appid") String appid);

      //topicsList
    @GET("welcome")
    Call<SettingsResponse> getSettings(@Query("appid") String appid);

      //topicsList
    @GET("forgotpassword")
    Call<SettingsResponse> forgetPassword(@Query("appid") String appid);


    @POST("usertopicstore")
    Call<UpdateResponse> sendTopics(@Query("appid") String appid, @Query("userid") String userid,
                              @Query("topics") String topics);


    //submitPoll
    @POST("pollstore")
    Call<UpdateResponse> submitPoll(@Query("appid") String appid, @Query("created_by") int created_by,
                               @Query("topics") String topics, @Query("question") String question,
                               @Query("option1") String option1, @Query("option2") String option2,
                               @Query("option3") String option3, @Query("option4") String option4);

    //vote appid, pollid, liked_by
    @POST("dolike")
    Call<UpdateResponse> liked(@Query("appid") String appid, @Query("pollid") int pollid,
                               @Query("liked_by") int likedby);

    //vote
    @POST("dovote")
    Call<UpdateResponse> vote(@Query("appid") String appid, @Query("voted_by") int voted_by,
                               @Query("pollid") int pollid,@Query("selectedoption") String selectedoption);

    //vote
    @POST("dofollow")
    Call<UpdateResponse> follow(@Query("appid") String appid, @Query("userid") int userid,
                               @Query("followed_by") int followed_by);
    //vote
    @POST("docomment")
    Call<UpdateResponse> comment(@Query("appid") String appid, @Query("commented_by") int commented_by,
                               @Query("pollid") int pollid,
                               @Query("comment") String comment);

    //vote
    @POST("dosuggestion")
    Call<UpdateResponse> suggestion(@Query("appid") String appid, @Query("userid") int userid,
                                    @Query("suggestion") String followed_by);

    //counter
    @POST("setcounter")
    Call<UpdateResponse> counter(@Query("userid") int userid);

    //counter
    @POST("setonlinestatus")
    Call<UpdateResponse> setonlinestatus(@Query("userid") int userid ,@Query("isonline") int isonline);


    @GET("pollcomments")
    Call<CommentsResponse> getComments(@Query("appid") String appid, @Query("pollid") int pollid );

    @GET("getpoll")
    Call<PollsResponse> getPoll(@Query("appid") String appid, @Query("userid") int userid,
                                @Query("pagenumber") int pagenumber);
    @GET("getallpolls")
    Call<PollsResponse> getOldPoll(@Query("appid") String appid, @Query("userid") int userid,
                                @Query("pagenumber") int pagenumber);

    @GET("polldetails")
    Call<PollByIdResponose> getPollById(@Query("appid") String appid, @Query("userid") int userid,
                                @Query("pollid") int pollid);

    @GET("profileinfo")
    Call<ProfileResponse> getProfile(@Query("appid") String appid, @Query("profileuserid") int profileuserid,
                                     @Query("logged_in_user") int logged_in_user);


    @GET("votesinfo")
    Call<GraphResponse> getPollStats(@Query("appid") String appid, @Query("pollid") int pollid);

    @GET("appstats")
    Call<AppStatsResponse> getAppStats(@Query("appid") String appid);

    @GET("hotpolls")
    Call<HotPollResponse> getHotPoll(@Query("appid") String appid);


    @GET("notifications")
    Call<AdminNotificationResponse> getAdminNotificition(@Query("appid") String appid);


    @Multipart
    @POST("profilepicchange")
    Call<UpdateImageResponse> postImage(@Query("appid") String appid, @Query("userid") int userid,@Part MultipartBody.Part image);

    //appid, created_by, topics, question, option1.....option4

//    //CategoryDelete
//    @GET("customcatdelete/{cat_id}")
//    Call<UpdateResponse> deleteCategory(@Path("cat_id") String cat_id, @Query("appid") String appid);
//
//
//    //login
//       @POST("login")
//    Call<RegResponse> Login(@Query("email") String email, @Query("password") String password, @Query("usertype") String usertype, @Query("appid") String appid);
//
//
//    //ChangePassword
//    @POST("changepassword")
//    Call<UpdateResponse> changePassword(@Query("newpassword") String newpassword, @Query("confirmpassword") String confirmpassword,
//                                                                                             @Query("currentpass") String currentpass, @Query("userid") String userid, @Query("appid") String appid);
//
//
//    //productAdd
//    @POST("customprodadd")
//    Call<UpdateResponse> productAdd(@Query("storeid") String storeid, @Query("categoryid") String categoryid,
//                                                                                         @Query("title") String title, @Query("shelftime") String shelftime,
//                                                                                         @Query("thawtime") String thawtime, @Query("appid") String appid);
//
//
//    //productEdit
//    @POST("customprodupdate/{pro_id}")
//    Call<UpdateResponse> updateProduct(@Path("pro_id") String pro_id, @Query("categoryid") String categoryid,
//                                                                                            @Query("title") String title, @Query("shelftime") String shelftime,
//                                                                                            @Query("thawtime") String thawtime, @Query("appid") String appid, @Query("storeid") String storeid);
//
//
//
//    //productList
//    @GET("customproducts/{store_id}")
//    Call<ProductResponse> getProductList(@Path("store_id") String store_id, @Query("appid") String appid);
//
//
//    //product&categorylist
//    @GET("catproducts/{store_id}")
//    Call<CatProductResponse> getCatProducts(@Path("store_id") String store_id, @Query("appid") String appid);
//
//
//    //productDelete
//    @GET("customproddelete/{pro_id}")
//    Call<UpdateResponse> deleteProduct(@Path("pro_id") String pro_id, @Query("appid") String appid);
//
//
//    //userAdd
//    @POST("useradd")
//    Call<UpdateResponse> userAdd(@Query("email") String email, @Query("password") String password,
//                                                                                      @Query("company") String company, @Query("usertype") String usertype,
//                                                                                      @Query("created_by") String thawtime, @Query("storeid") String storeid,
//                                                                                      @Query("appid") String appid, @Query("fullname") String fullname);
//
//
//
//    //userList
//    @GET("allusers/{store_id}")
//    Call<UserResponse> getUserList(@Path("store_id") String store_id, @Query("appid") String appid);
//
//
//    //userEdit
//    @POST("userupdate/{user_id}")
//    Call<UpdateResponse> updateUser(@Path("user_id") String user_id, @Query("appid") String appid,
//                                                                                         @Query("email") String email, @Query("usertype") String usertype,
//                                                                                         @Query("status") String status, @Query("fullname") String fullname);
//
//    //userDelete
//    @GET("userdelete/{user_id}")
//    Call<UpdateResponse> deleteUser(@Path("user_id") String user_id, @Query("appid") String appid);
//
//
//
//    //product for specific category
//    @GET("customproductscat/{store_id}/{category_id}")
//    Call<CatProductResponse> getProductForCategory(@Path("store_id") String store_id, @Path("category_id") String category_id);
//

}
