
package invertemotech.com.pakpoll.retrofitmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SettingData {

    @SerializedName("poster")
    @Expose
    private String poster;
    @SerializedName("posterupdated")
    @Expose
    private String posterupdated;
    @SerializedName("themecolor")
    @Expose
    private String themecolor;

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getPosterupdated() {
        return posterupdated;
    }

    public void setPosterupdated(String posterupdated) {
        this.posterupdated = posterupdated;
    }

    public String getThemecolor() {
        return themecolor;
    }

    public void setThemecolor(String themecolor) {
        this.themecolor = themecolor;
    }

}
