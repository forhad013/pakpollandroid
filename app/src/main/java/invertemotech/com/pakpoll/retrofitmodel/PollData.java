
package invertemotech.com.pakpoll.retrofitmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PollData {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("profilepic")
    @Expose
    private String profilepic;
    @SerializedName("poll")
    @Expose
    private Poll poll;
    @SerializedName("totalfollowers")
    @Expose
    private Integer totalfollowers;
    @SerializedName("isliked")
    @Expose
    private Integer isliked;
    @SerializedName("isfollowed")
    @Expose
    private Integer isfollowed;
    @SerializedName("totalvotes")
    @Expose
    private Integer totalvotes;
    @SerializedName("option1")
    @Expose
    private Integer option1;
    @SerializedName("option2")
    @Expose
    private Integer option2;
    @SerializedName("option3")
    @Expose
    private Integer option3;
    @SerializedName("option4")
    @Expose
    private Integer option4;
    @SerializedName("totalpolls")
    @Expose
    private Integer totalpolls;

    public Integer getNextpollid() {
        return nextpollid;
    }

    public void setNextpollid(Integer nextpollid) {
        this.nextpollid = nextpollid;
    }

    @SerializedName("nextpollid")
    @Expose
    private Integer nextpollid;

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The profilepic
     */
    public String getProfilepic() {
        return profilepic;
    }

    /**
     * 
     * @param profilepic
     *     The profilepic
     */
    public void setProfilepic(String profilepic) {
        this.profilepic = profilepic;
    }

    /**
     * 
     * @return
     *     The poll
     */
    public Poll getPoll() {
        return poll;
    }

    /**
     * 
     * @param poll
     *     The poll
     */
    public void setPoll(Poll poll) {
        this.poll = poll;
    }

    /**
     * 
     * @return
     *     The totalfollowers
     */
    public Integer getTotalfollowers() {
        return totalfollowers;
    }

    /**
     * 
     * @param totalfollowers
     *     The totalfollowers
     */
    public void setTotalfollowers(Integer totalfollowers) {
        this.totalfollowers = totalfollowers;
    }

    /**
     * 
     * @return
     *     The isliked
     */
    public Integer getIsliked() {
        return isliked;
    }

    /**
     * 
     * @param isliked
     *     The isliked
     */
    public void setIsliked(Integer isliked) {
        this.isliked = isliked;
    }

    /**
     * 
     * @return
     *     The isfollowed
     */
    public Integer getIsfollowed() {
        return isfollowed;
    }

    /**
     * 
     * @param isfollowed
     *     The isfollowed
     */
    public void setIsfollowed(Integer isfollowed) {
        this.isfollowed = isfollowed;
    }

    /**
     * 
     * @return
     *     The totalvotes
     */
    public Integer getTotalvotes() {
        return totalvotes;
    }

    /**
     * 
     * @param totalvotes
     *     The totalvotes
     */
    public void setTotalvotes(Integer totalvotes) {
        this.totalvotes = totalvotes;
    }

    /**
     * 
     * @return
     *     The option1
     */
    public Integer getOption1() {
        return option1;
    }

    /**
     * 
     * @param option1
     *     The option1
     */
    public void setOption1(Integer option1) {
        this.option1 = option1;
    }

    /**
     * 
     * @return
     *     The option2
     */
    public Integer getOption2() {
        return option2;
    }

    /**
     * 
     * @param option2
     *     The option2
     */
    public void setOption2(Integer option2) {
        this.option2 = option2;
    }

    /**
     * 
     * @return
     *     The option3
     */
    public Integer getOption3() {
        return option3;
    }

    /**
     * 
     * @param option3
     *     The option3
     */
    public void setOption3(Integer option3) {
        this.option3 = option3;
    }

    /**
     * 
     * @return
     *     The option4
     */
    public Integer getOption4() {
        return option4;
    }

    /**
     * 
     * @param option4
     *     The option4
     */
    public void setOption4(Integer option4) {
        this.option4 = option4;
    }

    /**
     * 
     * @return
     *     The totalpolls
     */
    public Integer getTotalpolls() {
        return totalpolls;
    }

    /**
     * 
     * @param totalpolls
     *     The totalpolls
     */
    public void setTotalpolls(Integer totalpolls) {
        this.totalpolls = totalpolls;
    }

}
