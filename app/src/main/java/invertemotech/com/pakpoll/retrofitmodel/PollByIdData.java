
package invertemotech.com.pakpoll.retrofitmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PollByIdData {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("topics")
    @Expose
    private String topics;
    @SerializedName("question")
    @Expose
    private String question;
    @SerializedName("option1")
    @Expose
    private String option1;
    @SerializedName("option2")
    @Expose
    private String option2;
    @SerializedName("option3")
    @Expose
    private String option3;
    @SerializedName("option4")
    @Expose
    private String option4;
    @SerializedName("option5")
    @Expose
    private Object option5;
    @SerializedName("created_by")
    @Expose
    private Integer createdBy;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("liked")
    @Expose
    private Integer liked;
    @SerializedName("hasvoted")
    @Expose
    private Integer hasvoted;
    @SerializedName("totalvotes")
    @Expose
    private Integer totalvotes;
    @SerializedName("option1selected")
    @Expose
    private Integer option1selected;
    @SerializedName("option2selected")
    @Expose
    private Integer option2selected;
    @SerializedName("option3selected")
    @Expose
    private Integer option3selected;
    @SerializedName("option4selected")
    @Expose
    private Integer option4selected;
    @SerializedName("pollcreatedbyname")
    @Expose
    private String pollcreatedbyname;
    @SerializedName("pollcreatedbypic")
    @Expose
    private String pollcreatedbypic;
    @SerializedName("isfollowed")
    @Expose
    private Integer isfollowed;
    @SerializedName("totalfollowers")
    @Expose
    private Integer totalfollowers;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTopics() {
        return topics;
    }

    public void setTopics(String topics) {
        this.topics = topics;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getOption1() {
        return option1;
    }

    public void setOption1(String option1) {
        this.option1 = option1;
    }

    public String getOption2() {
        return option2;
    }

    public void setOption2(String option2) {
        this.option2 = option2;
    }

    public String getOption3() {
        return option3;
    }

    public void setOption3(String option3) {
        this.option3 = option3;
    }

    public String getOption4() {
        return option4;
    }

    public void setOption4(String option4) {
        this.option4 = option4;
    }

    public Object getOption5() {
        return option5;
    }

    public void setOption5(Object option5) {
        this.option5 = option5;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getLiked() {
        return liked;
    }

    public void setLiked(Integer liked) {
        this.liked = liked;
    }

    public Integer getHasvoted() {
        return hasvoted;
    }

    public void setHasvoted(Integer hasvoted) {
        this.hasvoted = hasvoted;
    }

    public Integer getTotalvotes() {
        return totalvotes;
    }

    public void setTotalvotes(Integer totalvotes) {
        this.totalvotes = totalvotes;
    }

    public Integer getOption1selected() {
        return option1selected;
    }

    public void setOption1selected(Integer option1selected) {
        this.option1selected = option1selected;
    }

    public Integer getOption2selected() {
        return option2selected;
    }

    public void setOption2selected(Integer option2selected) {
        this.option2selected = option2selected;
    }

    public Integer getOption3selected() {
        return option3selected;
    }

    public void setOption3selected(Integer option3selected) {
        this.option3selected = option3selected;
    }

    public Integer getOption4selected() {
        return option4selected;
    }

    public void setOption4selected(Integer option4selected) {
        this.option4selected = option4selected;
    }

    public String getPollcreatedbyname() {
        return pollcreatedbyname;
    }

    public void setPollcreatedbyname(String pollcreatedbyname) {
        this.pollcreatedbyname = pollcreatedbyname;
    }

    public String getPollcreatedbypic() {
        return pollcreatedbypic;
    }

    public void setPollcreatedbypic(String pollcreatedbypic) {
        this.pollcreatedbypic = pollcreatedbypic;
    }

    public Integer getIsfollowed() {
        return isfollowed;
    }

    public void setIsfollowed(Integer isfollowed) {
        this.isfollowed = isfollowed;
    }

    public Integer getTotalfollowers() {
        return totalfollowers;
    }

    public void setTotalfollowers(Integer totalfollowers) {
        this.totalfollowers = totalfollowers;
    }

}
