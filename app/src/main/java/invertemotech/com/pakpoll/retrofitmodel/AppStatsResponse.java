
package invertemotech.com.pakpoll.retrofitmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AppStatsResponse {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("data")
    @Expose
    private AppStatsData appStatsData;
    @SerializedName("message")
    @Expose
    private String message;

    /**
     * 
     * @return
     *     The error
     */
    public Boolean getError() {
        return error;
    }

    /**
     * 
     * @param error
     *     The error
     */
    public void setError(Boolean error) {
        this.error = error;
    }

    /**
     * 
     * @return
     *     The appStatsData
     */
    public AppStatsData getAppStatsData() {
        return appStatsData;
    }

    /**
     * 
     * @param appStatsData
     *     The appStatsData
     */
    public void setAppStatsData(AppStatsData appStatsData) {
        this.appStatsData = appStatsData;
    }

    /**
     * 
     * @return
     *     The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * 
     * @param message
     *     The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

}
