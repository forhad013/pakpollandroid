
package invertemotech.com.pakpoll.retrofitmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Poll {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("topics")
    @Expose
    private String topics;
    @SerializedName("question")
    @Expose
    private String question;
    @SerializedName("option1")
    @Expose
    private String option1;
    @SerializedName("option2")
    @Expose
    private String option2;
    @SerializedName("option3")
    @Expose
    private String option3;
    @SerializedName("option4")
    @Expose
    private String option4;
    @SerializedName("option5")
    @Expose
    private Object option5;
    @SerializedName("created_by")
    @Expose
    private Integer createdBy;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    /**
     * 
     * @return
     *     The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The topics
     */
    public String getTopics() {
        return topics;
    }

    /**
     * 
     * @param topics
     *     The topics
     */
    public void setTopics(String topics) {
        this.topics = topics;
    }

    /**
     * 
     * @return
     *     The question
     */
    public String getQuestion() {
        return question;
    }

    /**
     * 
     * @param question
     *     The question
     */
    public void setQuestion(String question) {
        this.question = question;
    }

    /**
     * 
     * @return
     *     The option1
     */
    public String getOption1() {
        return option1;
    }

    /**
     * 
     * @param option1
     *     The option1
     */
    public void setOption1(String option1) {
        this.option1 = option1;
    }

    /**
     * 
     * @return
     *     The option2
     */
    public String getOption2() {
        return option2;
    }

    /**
     * 
     * @param option2
     *     The option2
     */
    public void setOption2(String option2) {
        this.option2 = option2;
    }

    /**
     * 
     * @return
     *     The option3
     */
    public String getOption3() {
        return option3;
    }

    /**
     * 
     * @param option3
     *     The option3
     */
    public void setOption3(String option3) {
        this.option3 = option3;
    }

    /**
     * 
     * @return
     *     The option4
     */
    public String getOption4() {
        return option4;
    }

    /**
     * 
     * @param option4
     *     The option4
     */
    public void setOption4(String option4) {
        this.option4 = option4;
    }

    /**
     * 
     * @return
     *     The option5
     */
    public Object getOption5() {
        return option5;
    }

    /**
     * 
     * @param option5
     *     The option5
     */
    public void setOption5(Object option5) {
        this.option5 = option5;
    }

    /**
     * 
     * @return
     *     The createdBy
     */
    public Integer getCreatedBy() {
        return createdBy;
    }

    /**
     * 
     * @param createdBy
     *     The created_by
     */
    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * 
     * @return
     *     The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * 
     * @param createdAt
     *     The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * 
     * @return
     *     The updatedAt
     */
    public String getUpdatedAt() {
        return updatedAt;
    }

    /**
     * 
     * @param updatedAt
     *     The updated_at
     */
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
