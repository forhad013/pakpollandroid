
package invertemotech.com.pakpoll.retrofitmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GraphData {

    @SerializedName("malevoters")
    @Expose
    private Integer malevoters;
    @SerializedName("femalevoters")
    @Expose
    private Integer femalevoters;
    @SerializedName("sindhvoters")
    @Expose
    private Integer sindhvoters;
    @SerializedName("punjabvoters")
    @Expose
    private Integer punjabvoters;
    @SerializedName("khybervoters")
    @Expose
    private Integer khybervoters;
    @SerializedName("balochistanvoters")
    @Expose
    private Integer balochistanvoters;
    @SerializedName("kashmirvoters")
    @Expose
    private Integer kashmirvoters;
    @SerializedName("gilgitvoters")
    @Expose
    private Integer gilgitvoters;

    /**
     * 
     * @return
     *     The malevoters
     */
    public Integer getMalevoters() {
        return malevoters;
    }

    /**
     * 
     * @param malevoters
     *     The malevoters
     */
    public void setMalevoters(Integer malevoters) {
        this.malevoters = malevoters;
    }

    /**
     * 
     * @return
     *     The femalevoters
     */
    public Integer getFemalevoters() {
        return femalevoters;
    }

    /**
     * 
     * @param femalevoters
     *     The femalevoters
     */
    public void setFemalevoters(Integer femalevoters) {
        this.femalevoters = femalevoters;
    }

    /**
     * 
     * @return
     *     The sindhvoters
     */
    public Integer getSindhvoters() {
        return sindhvoters;
    }

    /**
     * 
     * @param sindhvoters
     *     The sindhvoters
     */
    public void setSindhvoters(Integer sindhvoters) {
        this.sindhvoters = sindhvoters;
    }

    /**
     * 
     * @return
     *     The punjabvoters
     */
    public Integer getPunjabvoters() {
        return punjabvoters;
    }

    /**
     * 
     * @param punjabvoters
     *     The punjabvoters
     */
    public void setPunjabvoters(Integer punjabvoters) {
        this.punjabvoters = punjabvoters;
    }

    /**
     * 
     * @return
     *     The khybervoters
     */
    public Integer getKhybervoters() {
        return khybervoters;
    }

    /**
     * 
     * @param khybervoters
     *     The khybervoters
     */
    public void setKhybervoters(Integer khybervoters) {
        this.khybervoters = khybervoters;
    }

    /**
     * 
     * @return
     *     The balochistanvoters
     */
    public Integer getBalochistanvoters() {
        return balochistanvoters;
    }

    /**
     * 
     * @param balochistanvoters
     *     The balochistanvoters
     */
    public void setBalochistanvoters(Integer balochistanvoters) {
        this.balochistanvoters = balochistanvoters;
    }

    /**
     * 
     * @return
     *     The kashmirvoters
     */
    public Integer getKashmirvoters() {
        return kashmirvoters;
    }

    /**
     * 
     * @param kashmirvoters
     *     The kashmirvoters
     */
    public void setKashmirvoters(Integer kashmirvoters) {
        this.kashmirvoters = kashmirvoters;
    }

    /**
     * 
     * @return
     *     The gilgitvoters
     */
    public Integer getGilgitvoters() {
        return gilgitvoters;
    }

    /**
     * 
     * @param gilgitvoters
     *     The gilgitvoters
     */
    public void setGilgitvoters(Integer gilgitvoters) {
        this.gilgitvoters = gilgitvoters;
    }

}
