
package invertemotech.com.pakpoll.retrofitmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AppStatsData {

    @SerializedName("registeredusers")
    @Expose
    private Integer registeredusers;
    @SerializedName("registereduserspic")
    @Expose
    private String registereduserspic;
    @SerializedName("registereduserstext")
    @Expose
    private String registereduserstext;
    @SerializedName("pollscreated")
    @Expose
    private Integer pollscreated;
    @SerializedName("pollscreatedpic")
    @Expose
    private String pollscreatedpic;
    @SerializedName("pollscreatedtext")
    @Expose
    private String pollscreatedtext;
    @SerializedName("popularcategory")
    @Expose
    private String popularcategory;
    @SerializedName("popularcatimage")
    @Expose
    private String popularcatimage;
    @SerializedName("popularcattext")
    @Expose
    private String popularcattext;

    /**
     * 
     * @return
     *     The registeredusers
     */
    public Integer getRegisteredusers() {
        return registeredusers;
    }

    /**
     * 
     * @param registeredusers
     *     The registeredusers
     */
    public void setRegisteredusers(Integer registeredusers) {
        this.registeredusers = registeredusers;
    }

    /**
     * 
     * @return
     *     The registereduserspic
     */
    public String getRegistereduserspic() {
        return registereduserspic;
    }

    /**
     * 
     * @param registereduserspic
     *     The registereduserspic
     */
    public void setRegistereduserspic(String registereduserspic) {
        this.registereduserspic = registereduserspic;
    }

    /**
     * 
     * @return
     *     The registereduserstext
     */
    public String getRegistereduserstext() {
        return registereduserstext;
    }

    /**
     * 
     * @param registereduserstext
     *     The registereduserstext
     */
    public void setRegistereduserstext(String registereduserstext) {
        this.registereduserstext = registereduserstext;
    }

    /**
     * 
     * @return
     *     The pollscreated
     */
    public Integer getPollscreated() {
        return pollscreated;
    }

    /**
     * 
     * @param pollscreated
     *     The pollscreated
     */
    public void setPollscreated(Integer pollscreated) {
        this.pollscreated = pollscreated;
    }

    /**
     * 
     * @return
     *     The pollscreatedpic
     */
    public String getPollscreatedpic() {
        return pollscreatedpic;
    }

    /**
     * 
     * @param pollscreatedpic
     *     The pollscreatedpic
     */
    public void setPollscreatedpic(String pollscreatedpic) {
        this.pollscreatedpic = pollscreatedpic;
    }

    /**
     * 
     * @return
     *     The pollscreatedtext
     */
    public String getPollscreatedtext() {
        return pollscreatedtext;
    }

    /**
     * 
     * @param pollscreatedtext
     *     The pollscreatedtext
     */
    public void setPollscreatedtext(String pollscreatedtext) {
        this.pollscreatedtext = pollscreatedtext;
    }

    /**
     * 
     * @return
     *     The popularcategory
     */
    public String getPopularcategory() {
        return popularcategory;
    }

    /**
     * 
     * @param popularcategory
     *     The popularcategory
     */
    public void setPopularcategory(String popularcategory) {
        this.popularcategory = popularcategory;
    }

    /**
     * 
     * @return
     *     The popularcatimage
     */
    public String getPopularcatimage() {
        return popularcatimage;
    }

    /**
     * 
     * @param popularcatimage
     *     The popularcatimage
     */
    public void setPopularcatimage(String popularcatimage) {
        this.popularcatimage = popularcatimage;
    }

    /**
     * 
     * @return
     *     The popularcattext
     */
    public String getPopularcattext() {
        return popularcattext;
    }

    /**
     * 
     * @param popularcattext
     *     The popularcattext
     */
    public void setPopularcattext(String popularcattext) {
        this.popularcattext = popularcattext;
    }

}
