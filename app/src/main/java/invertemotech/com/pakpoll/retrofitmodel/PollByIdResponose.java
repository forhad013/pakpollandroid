
package invertemotech.com.pakpoll.retrofitmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PollByIdResponose {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("data")
    @Expose
    private PollByIdData pollByIdData;
    @SerializedName("message")
    @Expose
    private String message;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public PollByIdData getPollByIdData() {
        return pollByIdData;
    }

    public void setPollByIdData(PollByIdData pollByIdData) {
        this.pollByIdData = pollByIdData;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
