
package invertemotech.com.pakpoll.retrofitmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PollsResponse {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("data")
    @Expose
    private PollData pollData;
    @SerializedName("message")
    @Expose
    private String message;

    /**
     * 
     * @return
     *     The error
     */
    public Boolean getError() {
        return error;
    }

    /**
     * 
     * @param error
     *     The error
     */
    public void setError(Boolean error) {
        this.error = error;
    }

    /**
     * 
     * @return
     *     The pollData
     */
    public PollData getPollData() {
        return pollData;
    }

    /**
     * 
     * @param pollData
     *     The pollData
     */
    public void setPollData(PollData pollData) {
        this.pollData = pollData;
    }

    /**
     * 
     * @return
     *     The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * 
     * @param message
     *     The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

}
