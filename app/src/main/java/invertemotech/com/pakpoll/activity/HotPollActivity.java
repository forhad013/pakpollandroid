package invertemotech.com.pakpoll.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import invertemotech.com.pakpoll.R;
import invertemotech.com.pakpoll.adapter.HotPollAdapter;
import invertemotech.com.pakpoll.retrofitmodel.ApiClient;
import invertemotech.com.pakpoll.retrofitmodel.ApiInterafce;
import invertemotech.com.pakpoll.retrofitmodel.HotPollData;
import invertemotech.com.pakpoll.retrofitmodel.HotPollResponse;
import invertemotech.com.pakpoll.util.ConnectionDetector;
import invertemotech.com.pakpoll.util.SharePref;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HotPollActivity extends AppCompatActivity {

    ListView list;

    ConnectionDetector cd;
    SharePref sharePref;
    String userId;

    ArrayList<HotPollData> hotPollDataArrayList;
    HotPollData hotPollData;
    boolean isInternetPresent;
    SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hot_poll);


        list = (ListView) findViewById(R.id.list);


        hotPollDataArrayList = new ArrayList<>();
        ImageButton backBtn = (ImageButton) findViewById(R.id.menu);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        progressSweetAlertDialog = new SweetAlertDialog(HotPollActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);


        sharePref = new SharePref(getApplicationContext());
        cd = new ConnectionDetector(getApplicationContext());

        isInternetPresent = cd.isConnectingToInternet();

        userId = sharePref.getshareprefdatastring(SharePref.USERID);


        setTheme();


        if(isInternetPresent){

            getHotPolls();
        }else{

        }
    }

    public void getHotPolls(){

        String appid = "d6847a8652cc7cbf9b92f8f634110fff";
        progressSweetAlertDialog.show();
        ApiInterafce apiService =
                ApiClient.getClient().create(ApiInterafce.class);



        Call<HotPollResponse> call = apiService.getHotPoll(appid);
        call.enqueue(new Callback<HotPollResponse>() {
            @Override
            public void onResponse(Call<HotPollResponse>call, Response<HotPollResponse> response) {

                //      Log.e("asd", "Number of movies received: " + response.body().getError()+"");


                boolean error = response.body().getError();

                String msg = response.body().getMessage();

                Log.e("error",error+"");
                Log.e("msg",msg+"");

                if(!error){

                    hotPollDataArrayList = response.body().getData();

                    setData();

                    progressSweetAlertDialog.dismiss();



                }else {


                    progressSweetAlertDialog.dismiss();
                    Toast.makeText(getApplicationContext(),"Something is wrong, Try again later",Toast.LENGTH_LONG).show();

                }





            }

            @Override
            public void onFailure(Call<HotPollResponse>call, Throwable t) {
                // Log error here since request failed
                Log.e("das", t.toString());
                progressSweetAlertDialog.dismiss();
            }
        });


    }

    public void setTheme(){
        RelativeLayout topPanel = (RelativeLayout) findViewById(R.id.topPanel);

        int color = sharePref.getshareprefdata(SharePref.THEMECOLOR);

        topPanel.setBackgroundColor(color);


    }

    public void setData(){



        HotPollAdapter topicAdapter = new HotPollAdapter(getApplicationContext(),hotPollDataArrayList);

        list.setAdapter(topicAdapter);

    }
}
