package invertemotech.com.pakpoll.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import invertemotech.com.pakpoll.R;
import invertemotech.com.pakpoll.fragment.Fragment_login;
import invertemotech.com.pakpoll.fragment.Fragment_registration;
import invertemotech.com.pakpoll.util.SharePref;

public class RegLoginActivity extends AppCompatActivity {

    RelativeLayout registration,login;

    TextView viewReg,viewLog;

    SharePref sharePref;

    boolean isLogedIn;
    LinearLayout tab;
    int colorValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg_login);

        registration = (RelativeLayout) findViewById(R.id.registration);
        login = (RelativeLayout) findViewById(R.id.login);
        viewReg = (TextView)findViewById(R.id.regView);
        viewLog = (TextView)findViewById(R.id.logView);
        tab = (LinearLayout) findViewById(R.id.tab);

        sharePref = new SharePref(getApplicationContext());

        colorValue = sharePref.getshareprefdata(SharePref.THEMECOLOR);





        if(colorValue!=160){

            //int color =  Integer.parseInt(colorValue);

            tab.setBackgroundColor(colorValue);


        }

        displayViewFirstPage(0);

        sharePref = new SharePref(getApplicationContext());

        isLogedIn = sharePref.getshareprefdataBoolean(SharePref.LOGEDIN);

        if(isLogedIn){
            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
            startActivity(intent);
            finish();
        }


        registration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayViewFirstPage(0);
            }
        });
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayViewFirstPage(1);
            }
        });

    }

    private void displayViewFirstPage(int position) {
        // update the main content by replacing fragments

        Fragment fragment = null;

        switch (position) {
            case 0:



                //	if(token.isEmpty()) {
                fragment = new Fragment_registration();
                viewReg.setVisibility(View.VISIBLE);
                viewLog.setVisibility(View.INVISIBLE);
                //	}else{
                //	fragment = new Fragment_hot_list();
                //	}

                break;
            case 1:
                fragment = new Fragment_login();

                viewReg.setVisibility(View.INVISIBLE);
                viewLog.setVisibility(View.VISIBLE);
                break;


            default:
                break;
        }

        if (fragment != null) {

            FragmentManager mFragmentManager = getSupportFragmentManager();

            FragmentTransaction ft = mFragmentManager.beginTransaction();


            ft.replace(R.id.frame, fragment);

            ft.commit();

        }
    }
}
