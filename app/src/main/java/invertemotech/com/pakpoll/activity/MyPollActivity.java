package invertemotech.com.pakpoll.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONObject;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;
import invertemotech.com.pakpoll.R;
import invertemotech.com.pakpoll.adapter.MyPollAdapter;
import invertemotech.com.pakpoll.retrofitmodel.ApiClient;
import invertemotech.com.pakpoll.retrofitmodel.ApiInterafce;
import invertemotech.com.pakpoll.retrofitmodel.ProfileData;
import invertemotech.com.pakpoll.retrofitmodel.ProfileResponse;
import invertemotech.com.pakpoll.retrofitmodel.UpdateResponse;
import invertemotech.com.pakpoll.util.ConnectionDetector;
import invertemotech.com.pakpoll.util.SharePref;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static invertemotech.com.pakpoll.R.id.numberOfVote;

public class MyPollActivity extends AppCompatActivity {

    ListView list;
    int id,current_user_id;


    int numberOfFollower;
    String created_by_name;

    String userID;

    TextView title;

    Boolean isInternetPresent = false;
    boolean isFollowed,isLiked;

    JSONObject json;
    private Uri mCropImageUri;
    ConnectionDetector cd;
    SharePref sharePref;

    int colorInt;
    RelativeLayout emptyView;

    int created_by_id;

    SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;

    View v;



    ProfileData profileData;

    CircleImageView profileImage;

    TextView followBtn,followers,votes,polls,name,create_new_poll;

    Context context;
    MyPollAdapter myPollAdapter;
    ImageButton changePicture;
    LinearLayout layout1,layout2;
    int x,y;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_poll);
        id = getIntent().getIntExtra("id",0);

        list = (ListView) findViewById(R.id.list);

        title = (TextView) findViewById(R.id.title);

        emptyView = (RelativeLayout) findViewById(R.id.emptyView);
        changePicture = (ImageButton) findViewById(R.id.changePicture);


        layout1 = (LinearLayout) findViewById(R.id.layout1);
        layout2 = (LinearLayout) findViewById(R.id.layout2);

        context = MyPollActivity.this;
        sharePref = new SharePref(getApplicationContext());

        profileImage= (CircleImageView) findViewById(R.id.profile_image);

        name = (TextView) findViewById(R.id.name);

        followBtn = (TextView) findViewById(R.id.followeBtn);
        followers = (TextView) findViewById(R.id.follower);
        votes = (TextView) findViewById(numberOfVote);
        polls = (TextView) findViewById(R.id.noOfPoll);
        create_new_poll = (TextView) findViewById(R.id.addNew);
        current_user_id = Integer.parseInt(sharePref.getshareprefdatastring(SharePref.USERID));

        progressSweetAlertDialog = new SweetAlertDialog(MyPollActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);


        sharePref = new SharePref(getApplicationContext());
        cd = new ConnectionDetector(getApplicationContext());

        isInternetPresent = cd.isConnectingToInternet();

        userID = sharePref.getshareprefdatastring(SharePref.USERID);

        if(userID.equals(id+"")){

            title.setText("My Profile");
            followBtn.setEnabled(false);
        }else{
            title.setText("User Profile");
            changePicture.setVisibility(View.INVISIBLE);

        }


        changePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              if(userID.equals(id+"")) {
//                  x = view.getTop();
//                  y = view.getRight();
//
//
//                  showOptions();

                  if (CropImage.isExplicitCameraPermissionRequired(getApplicationContext())) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA}, CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE);
                } else {
                    CropImage.startPickImageActivity(MyPollActivity.this);
                }
              }
            }
        });


        create_new_poll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getApplicationContext(), NewPoll.class);

                startActivity(intent);
                finish();

            }
        });



        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                int id = profileData.getQuestions().get(i).geId();

                Intent intent = new Intent(getApplicationContext(), SinglePollActivity.class);
                intent.putExtra("id",id);
                startActivity(intent);

            }
        });

        followBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isInternetPresent) {


                    Log.e("isFollw",profileData.getFollowing()+"");
                    if(profileData.getFollowing()==0){
                        follow();
                    }else{
                        unFollow();
                    }

                }else{

                    Toast.makeText(context,"No Internet Connection",Toast.LENGTH_LONG).show();
                }
            }
        });

        ImageButton backBtn = (ImageButton) findViewById(R.id.menu);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });



        if(isInternetPresent){
            profileInfo();
        }


        setTheme();



        GradientDrawable shape =  new GradientDrawable();
        shape.setCornerRadius(45);

        // add some color
        // You can add your random color generator here
        // and set color

        // now find your view and add background to it

//        followBtn.setBackground(shape);
//        followers.setBackground(shape);

    }

    @Override
    @SuppressLint("NewApi")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == AppCompatActivity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage,
            // but we don't know if we need to for the URI so the simplest is to try open the stream and see if we get error.
            boolean requirePermissions = false;
            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {

                // request permissions and handle the result in onRequestPermissionsResult()
                requirePermissions = true;
                mCropImageUri = imageUri;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
            } else {

        //        mCurrentFragment.setImageUri(imageUri);

                Intent intent = new Intent(getApplicationContext(), ImageCropActivity.class);
                intent.putExtra("imageuri",imageUri.toString());
                startActivity(intent);

            }
        }
    }


    public void showOptions(){
//        AlertDialog.Builder builder = new AlertDialog.Builder(context,R.style.optionsdialog);
//
//
//
//
//        builder.setPositiveButton("Change Profile Picture",
//                new DialogInterface.OnClickListener()
//                {
//                    public void onClick(DialogInterface dialog, int id)
//                    {
//                        Intent intent = new Intent(context, ImageUploadActivity.class);
//
//                        context.startActivity(intent);
//                        dialog.cancel();
//                    }
//                });
//        AlertDialog alertDialog = builder.create();
//
//
//        // show it
//        alertDialog.show();

        CharSequence colors[] = new CharSequence[] {"Change Profile Picture"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setItems(colors, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(context, ImageUploadActivity.class);

                     context.startActivity(intent);
                         dialog.cancel();
            }
        });
        AlertDialog alertDialog = builder.create();
//
//
//        // show it
//        alertDialog.show();
        WindowManager.LayoutParams wmlp = alertDialog.getWindow().getAttributes();

        wmlp.gravity = Gravity.TOP | Gravity.LEFT;
        wmlp.x = x;   //x position
        wmlp.y = y;

        alertDialog.show();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                CropImage.startPickImageActivity(this);
            } else {
                Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
            }
        }
        if (requestCode == CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE) {
            if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // mCurrentFragment.setImageUri(mCropImageUri);

                Intent intent = new Intent(getApplicationContext(), ImageCropActivity.class);
                intent.putExtra("imageuri",mCropImageUri.toString());
                startActivity(intent);

            } else {
                Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void setTheme(){
        RelativeLayout topPanel = (RelativeLayout) findViewById(R.id.topPanel);

        int color = sharePref.getshareprefdata(SharePref.THEMECOLOR);
        colorInt= sharePref.getshareprefdata(SharePref.THEMECOLOR);

        topPanel.setBackgroundColor(color);
       // followBtn.setBackgroundColor(color);
       // followers.setBackgroundColor(color);
        followBtn.setBackgroundColor(color);
        followers.setBackgroundColor(color);


//        followBtn.setBackgroundColor(color);
//        followers.setBackgroundColor(color);




    }

    @Override
    protected void onResume() {

        try {
            DisplayImageOptions defaultOptions;

            defaultOptions = new DisplayImageOptions.Builder()
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .build();
            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                    .defaultDisplayImageOptions(defaultOptions)
                    .build();
            ImageLoader.getInstance().init(config);
            String imageUrl="";
            if(!userID.equals(id+"")) {
                  imageUrl = profileData.getProfileuserpic();
            }else{
                imageUrl = sharePref.getshareprefdatastring(SharePref.PROFILEPIC);
            }

            ImageLoader.getInstance().displayImage(imageUrl, profileImage, defaultOptions);
        }catch (Exception e){

        }


        super.onResume();
    }

    public void profileInfo(){

        String appid = "d6847a8652cc7cbf9b92f8f634110fff";
        progressSweetAlertDialog.show();
        ApiInterafce apiService =
                ApiClient.getClient().create(ApiInterafce.class);

        Log.e("u",current_user_id+"");
        Log.e("id",id+"");

        Call<ProfileResponse> call = apiService.getProfile(appid,id,current_user_id);
        call.enqueue(new Callback<ProfileResponse>() {
            @Override
            public void onResponse(Call<ProfileResponse>call, Response<ProfileResponse> response) {

                //      Log.e("asd", "Number of movies received: " + response.body().getError()+"");


                boolean error = response.body().getError();

                String msg = response.body().getMessage();

                Log.e("error",error+"");
                Log.e("msg",msg+"");

                if(!error){

                    profileData = response.body().getProfileData();

                    setData();

                    progressSweetAlertDialog.dismiss();



                    Log.e("adas",numberOfFollower+"");
                    followers.setText(numberOfFollower+" Followers");

                    setData();

                }else {


                    progressSweetAlertDialog.dismiss();
                    Toast.makeText(getApplicationContext(),"Something is wrong, Try again later",Toast.LENGTH_LONG).show();

                }





            }

            @Override
            public void onFailure(Call<ProfileResponse>call, Throwable t) {
                // Log error here since request failed
                Log.e("das", t.toString());
                progressSweetAlertDialog.dismiss();
            }
        });


    }


    public void setData(){
        numberOfFollower = profileData.getTotalfollowers();

        Log.e("folo",numberOfFollower+"");

        String f = numberOfFollower + " Followers";

        votes.setText("Number of Votes: "+profileData.getTotalvotes()+"");
        followers.setText(numberOfFollower+" Followers");
        polls.setText("Number of Polls: "+profileData.getTotalpollscreated()+"");




        DisplayImageOptions defaultOptions;

        defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(defaultOptions)
                .build();
        ImageLoader.getInstance().init(config);


        String imageUrl=profileData.getProfileuserpic();

        ImageLoader.getInstance().displayImage(imageUrl, profileImage, defaultOptions);


        if(profileData.getFollowing()==0){
            followBtn.setEnabled(true);
            isFollowed = false;
          followBtn.setText("Follow");
         followBtn.setBackgroundColor(colorInt);

        }else{
            isFollowed = true;
            followBtn.setText("Unfollow");
//            followBtn.setEnabled(false);
            followBtn.setBackground(getResources().getDrawable(R.drawable.circular_button_grey));
        }



        name.setText(profileData.getProfileusername());
        created_by_name = profileData.getProfileusername();

        if(userID.equals(id+"")){
            followBtn.setBackground(getResources().getDrawable(R.drawable.circular_button_grey));
            title.setText("My Profile");
            followBtn.setVisibility(View.INVISIBLE);
            followBtn.setEnabled(false);
            followBtn.setVisibility(View.INVISIBLE);
        }else{
           // layout2.setBackgroundColor(colorInt);
            title.setText("User Profile");
        }

      //  Log.e("Polid",profileData.getQuestions().get(0).geId()+"");

        MyPollAdapter topicAdapter = new MyPollAdapter(getApplicationContext(),profileData.getQuestions());

        list.setAdapter(topicAdapter);

        if(profileData.getQuestions().size()==0){
            emptyView.setVisibility(View.VISIBLE);
            if(userID.equals(id+"")) {
                create_new_poll.setPaintFlags(create_new_poll.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                create_new_poll.setVisibility(View.VISIBLE);
            }

        }

        followers.setText(numberOfFollower+" Followers");
    }

    public void follow(){

        followBtn.setEnabled(false);


        if(followBtn.getText().toString().equalsIgnoreCase("Unfollow")){

            followBtn.setText("Follow");
            followBtn.setBackgroundColor(colorInt);

        }else{

            followBtn.setText("Unfollow");
//            followBtn.setEnabled(false);
            followBtn.setBackground(getResources().getDrawable(R.drawable.circular_button_grey));
        }


        //   String storeId = sharePref.getshareprefdatastring(SharePref.TOKEN);
        String storeId = "2";
        String appid = "d6847a8652cc7cbf9b92f8f634110fff";


        ApiInterafce apiService =
                ApiClient.getClient().create(ApiInterafce.class);

        Call<UpdateResponse> call = apiService.follow(appid,id,Integer.parseInt(userID));
        call.enqueue(new Callback<UpdateResponse>() {
            @Override
            public void onResponse(Call<UpdateResponse>call, Response<UpdateResponse> response) {

                //      Log.e("asd", "Number of movies received: " + response.body().getError()+"");


                boolean error = response.body().getError();

                String msg = response.body().getMessage();

                Log.e("error",error+"");
                Log.e("msg",msg+"");



                if(!error){

                    if(!isFollowed){
                        numberOfFollower++;
                       profileData.setFollowing(1);
                        isFollowed = true;
                        profileData.setTotalfollowers(numberOfFollower);
                    }else{
                        numberOfFollower--;
                        profileData.setFollowing(0);
                        isFollowed = false;
                        profileData.setTotalfollowers(numberOfFollower);
                    }

                    setData();

                }else {

                    Toast.makeText(context,"Something is wrong, Try again later",Toast.LENGTH_LONG).show();

                }



                followBtn.setEnabled(true);

            }

            @Override
            public void onFailure(Call<UpdateResponse>call, Throwable t) {
                // Log error here since request failed
                Log.e("das", t.toString());
                progressSweetAlertDialog.dismiss();
                followBtn.setEnabled(true);
            }
        });


    }

    public void unFollow(){

        doneDialog = new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE);
        doneDialog.setConfirmText("Unfollow");
        doneDialog.setCancelText("Cancel");
        doneDialog.setTitleText("Unfollow "+created_by_name+" ?");
        doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                follow();
                sweetAlertDialog.dismiss();
            }
        }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.dismiss();
            }
        });
        doneDialog.show();
        doneDialog.setCancelable(true);
    }

}
