package invertemotech.com.pakpoll.activity;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieEntry;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import invertemotech.com.pakpoll.R;
import invertemotech.com.pakpoll.retrofitmodel.GraphData;
import invertemotech.com.pakpoll.util.ConnectionDetector;
import invertemotech.com.pakpoll.util.SharePref;

public class GraphActivityWebview extends AppCompatActivity {

    ArrayList<PieEntry> provinceData;
    ArrayList<PieEntry> genderData;

    boolean isInternetPresent;

    SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;
    ConnectionDetector cd;
    PieChart chart1,chart2;
    int pollId;
    int male;
    int female ;
    int Sindh  ;
    int Punjab ;
    int Khyber ;
    int Balochistan ;
    int Kashmir  ;
    int Gilgit;

    String url = "http://rskyinvestment.com/pakpoll/graphdata/";

    GraphData graphData;
    SharePref sharePref;
    WebView webview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph_webview);

        pollId = getIntent().getIntExtra("id",0);
        cd = new ConnectionDetector(getApplicationContext());

        sharePref = new SharePref(getApplicationContext());
        isInternetPresent = cd.isConnectingToInternet();
        ImageButton backBtn = (ImageButton) findViewById(R.id.menu);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        webview = (WebView) findViewById(R.id.webview);


        setTheme();


        progressSweetAlertDialog = new SweetAlertDialog(GraphActivityWebview.this, SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);

        if(isInternetPresent) {
            progressSweetAlertDialog.show();
            loadWebview();

        }else{

            Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_LONG).show();
        }




    }


    public void setTheme(){
        RelativeLayout topPanel = (RelativeLayout) findViewById(R.id.topPanel);

        int color = sharePref.getshareprefdata(SharePref.THEMECOLOR);

        topPanel.setBackgroundColor(color);


    }



    public void loadWebview(){
        webview.setWebViewClient(new MyBrowser());

        webview.getSettings().setLoadsImagesAutomatically(true);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webview.loadUrl(url+pollId);

    }
    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            progressSweetAlertDialog.dismiss();
            super.onPageFinished(view, url);
        }
    }

    @SuppressLint("NewApi")
    @Override
    protected void onResume() {
        super.onResume();
        webview.onResume();
        // ...
    }

    @SuppressLint("NewApi")
    @Override
    protected void onPause() {
        webview.onPause();
        // ...
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        //  mWebView.onDestroy();
        // ...
        super.onDestroy();
    }


    public void checkNull(){

        if(male==0&&female==0){
            chart2.removeAllViews();
            chart2.setCenterText("No AppStatsData");
            TextView No2= (TextView) findViewById(R.id.noData2);
            No2.setText("No AppStatsData");
        }


        if(Khyber==0&& Sindh  ==0&& Punjab ==0&& Balochistan ==0&& Kashmir ==0&& Gilgit==0){
            chart1.removeAllViews();
            chart1.setCenterText("No AppStatsData");
            TextView No1= (TextView) findViewById(R.id.noData1);
            Log.e("b","b");
            No1.setText("No AppStatsData");


        }
    }


}
