package invertemotech.com.pakpoll.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.utils.DiskCacheUtils;
import com.nostra13.universalimageloader.utils.MemoryCacheUtils;

import cn.pedant.SweetAlert.SweetAlertDialog;
import invertemotech.com.pakpoll.R;
import invertemotech.com.pakpoll.retrofitmodel.ApiClient;
import invertemotech.com.pakpoll.retrofitmodel.ApiInterafce;
import invertemotech.com.pakpoll.retrofitmodel.SettingsResponse;
import invertemotech.com.pakpoll.util.ConnectionDetector;
import invertemotech.com.pakpoll.util.SharePref;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashScreen extends AppCompatActivity {

    protected boolean _active = true;
    protected int _splashTime = 5000;
    boolean isInternetOn;
    SharePref sharePref;
    String appid;
    String colorValue;
    ImageView posterView;
    String updateDateFromDb;
    String updateDateFromDevice="new";
    String posterUrl="new";
    DisplayImageOptions defaultOptions;
    ProgressBar progressWheel;
    ConnectionDetector cd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        sharePref = new SharePref(getApplicationContext());
        posterView = (ImageView) findViewById(R.id.poster);

        int greenColorValue = Color.parseColor("#008ECA");

        cd = new ConnectionDetector(getApplicationContext());
        isInternetOn = cd.isConnectingToInternet();

        sharePref.setshareprefdata(SharePref.THEMECOLOR,greenColorValue);

        progressWheel = (ProgressBar) findViewById(R.id.progress);

        if(sharePref.getshareprefdataBoolean(SharePref.HASCOME)){
            updateDateFromDevice = sharePref.getshareprefdatastring(SharePref.UPDATEDATE);
        }


        sharePref.setshareprefdataBoolean(SharePref.FIRSTTIME,true);

        sharePref.setshareprefdataBoolean(SharePref.HASCOME,true);
        posterUrl = sharePref.getshareprefdatastring(SharePref.POSTER);

        defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .defaultDisplayImageOptions(defaultOptions)
                .build();
        ImageLoader.getInstance().init(config);

        if(isInternetOn) {
            try {
                 getTopics();

              //  set_splashTime();
            }catch (Exception e){

            }

        }else
        {

         SweetAlertDialog   doneDialog = new SweetAlertDialog(SplashScreen.this, SweetAlertDialog.ERROR_TYPE);
            doneDialog.setTitleText("SORRY");
            doneDialog.setContentText("o internet Connection");
            doneDialog.setConfirmText("Ok");
            doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    sweetAlertDialog.dismiss();
                    finish();


                }
            });


        }


    }

    public void set_splashTime(){
        Thread splashTread = new Thread() {
            @Override
            public void run() {
                try {
                    int waited = 0;
                    while (_active && (waited < _splashTime)) {
                        sleep(100);
                        if (_active) {
                            waited += 300;
                        }
                    }
                } catch (Exception e) {

                } finally {

//                    startActivity(new Intent(SplashActivity.this,
//                            DirChooserSample.class));

                    startActivity(new Intent(SplashScreen.this,
                            RegLoginActivity.class));
                    finish();

                }
            };
        };
        splashTread.start();
    }

    public void getTopics(){

        //   String storeId = sharePref.getshareprefdatastring(SharePref.TOKEN);

        appid = "d6847a8652cc7cbf9b92f8f634110fff";



        ApiInterafce apiService =
                ApiClient.getClient().create(ApiInterafce.class);

        Call<SettingsResponse> call = apiService.getSettings(appid);
        call.enqueue(new Callback<SettingsResponse>() {
            @Override
            public void onResponse(Call<SettingsResponse>call, Response<SettingsResponse> response) {



                //      Log.e("asd", "Number of movies received: " + response.body().getError()+"");
               // topicsDataArrayList = response.body().getData();

                boolean error = response.body().getError();

                String msg = response.body().getMessage();



                Log.e("error",error+"");
                Log.e("msg",msg+"");


                if(!error){



                    posterUrl = response.body().getData().getPoster();
                    updateDateFromDb = response.body().getData().getPosterupdated();

                    colorValue = response.body().getData().getThemecolor();
                    setData();






                }else {
                    Toast.makeText(getApplicationContext(),"No internet Connection",Toast.LENGTH_LONG).show();

                }





            }

            @Override
            public void onFailure(Call<SettingsResponse>call, Throwable t) {
                // Log error here since request failed
                Log.e("das", t.toString());

                Toast.makeText(getApplicationContext(),"Something wrong!! Try again later.",Toast.LENGTH_LONG).show();
             //   progressSweetAlertDialog.dismiss();
            }
        });


    }

    public void setData(){



        int greenColorValue = Color.parseColor(colorValue);

        sharePref.setshareprefdata(SharePref.THEMECOLOR,greenColorValue);

        Log.e("updateDateFromDb",updateDateFromDb);
        Log.e("updateDateFromDevice",updateDateFromDevice);
        if(updateDateFromDb.equals(updateDateFromDevice)){





            ImageLoader.getInstance().displayImage(posterUrl, posterView, defaultOptions,   new SimpleImageLoadingListener() {

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    // Turn off cache in memory for this call getting the larger images.
                    // It will still be cached on disk if possible.
                    progressWheel.setVisibility(View.GONE);
                    set_splashTime();
               }
            });
        }else{

            DiskCacheUtils.removeFromCache(posterUrl, ImageLoader.getInstance().getDiskCache());

            MemoryCacheUtils.removeFromCache(posterUrl, ImageLoader.getInstance().getMemoryCache());

            ImageLoader.getInstance().displayImage(posterUrl, posterView, defaultOptions,   new SimpleImageLoadingListener() {

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    // Turn off cache in memory for this call getting the larger images.
                    // It will still be cached on disk if possible.
                    progressWheel.setVisibility(View.GONE);
                    set_splashTime();
                }
            });

            sharePref.setshareprefdatastring(SharePref.UPDATEDATE,updateDateFromDb);
        }
       // set_splashTime();


    }
}
