package invertemotech.com.pakpoll.activity;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import invertemotech.com.pakpoll.R;
import invertemotech.com.pakpoll.adapter.TopicAdapter;
import invertemotech.com.pakpoll.retrofitmodel.ApiClient;
import invertemotech.com.pakpoll.retrofitmodel.ApiInterafce;
import invertemotech.com.pakpoll.retrofitmodel.TopicsData;
import invertemotech.com.pakpoll.retrofitmodel.TopicsResponse;
import invertemotech.com.pakpoll.retrofitmodel.UpdateResponse;
import invertemotech.com.pakpoll.util.ConnectionDetector;
import invertemotech.com.pakpoll.util.SharePref;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TopicsActivitySecond extends AppCompatActivity  implements  TopicAdapter.onItemClickListener {

    String appid;
    ListView listView;

    Button done;

    TopicAdapter topicAdapter;
    ArrayList<TopicsData> topicsDataArrayList;
    Context context;
   public ArrayList<Boolean> checkArraylist;

    ConnectionDetector cd;

    boolean isInternetAvailable;
    
    String userid,selectedTopics;
    
    SharePref sharePref;

    String previousTopics;

    ArrayList previousTopicsList;


    SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topics_second);

        done = (Button) findViewById(R.id.done);

        context = TopicsActivitySecond.this;

        checkArraylist = new ArrayList<>();
        
        sharePref = new SharePref(getApplicationContext());
        listView = (ListView) findViewById(R.id.list);
       topicsDataArrayList = new ArrayList<>();

        previousTopics = sharePref.getshareprefdatastring(SharePref.TOPICS);
        
        userid = sharePref.getshareprefdatastring(SharePref.USERID);
        cd = new ConnectionDetector(getApplicationContext());
        isInternetAvailable = cd.isConnectingToInternet();

        progressSweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false); 
        ///checking if the item is clicked beofre or not. if clicked it will true. false is slected by default
        previousTopicsList = new ArrayList();

        Log.e("pr1",previousTopics);

        if(!previousTopics.equals("")){

            String[] shopTimes1 = previousTopics.split("\\,");

            for(int i=0;i<shopTimes1.length;i++){
                previousTopicsList.add(shopTimes1[i].replace(" ",""));

                Log.e("pr",previousTopicsList.get(i).toString());
            }
        }


        if(isInternetAvailable){
            getTopics();
        }

        ImageButton backBtn = (ImageButton) findViewById(R.id.backBtn);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(checkNullSeleted()) {
                    sendSelected();
                }else {
                    doneDialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
                    doneDialog.setTitleText("ERROR");
                    doneDialog.setContentText("Select At Least One Topics");
                    doneDialog.show();

                }

            }
        });

        setTheme();

//        ImageView test = (ImageView) findViewById(R.id.test);
//
//        Picasso.with(getApplicationContext()).load("www.rskyinvestment.com/pakpoll/resources/uploads/topics/politics-icon.png").into(test);


    }

    public void setTheme(){
        RelativeLayout topPanel = (RelativeLayout) findViewById(R.id.topPanel);

        int color = sharePref.getshareprefdata(SharePref.THEMECOLOR);

        topPanel.setBackgroundColor(color);
        done.setBackgroundColor(color);

    }


    public boolean checkNullSeleted(){
        boolean check = false;

        for(int i=0;i<checkArraylist.size();i++){

            if(checkArraylist.get(i)){
                check = true;
                break;
            }
        }

        return  check;

    }

    public String topicsGenerator(){

        String topics="";

        for(int i=0;i<checkArraylist.size();i++){
            if(checkArraylist.get(i)){
                if (topics.equals("")) {
                    topics =  topics +topicsDataArrayList.get(i).getId().toString();
                }else{
                    topics =  topics +" , " + topicsDataArrayList.get(i).getId().toString();
                }

            }
        }

        Log.e("topics",topics);

        return topics;
    }


    public void getTopics(){

        progressSweetAlertDialog.show();
        //   String storeId = sharePref.getshareprefdatastring(SharePref.TOKEN);

        appid = "d6847a8652cc7cbf9b92f8f634110fff";



        ApiInterafce apiService =
                ApiClient.getClient().create(ApiInterafce.class);

        Call<TopicsResponse> call = apiService.getTopicsList(appid);
        call.enqueue(new Callback<TopicsResponse>() {
            @Override
            public void onResponse(Call<TopicsResponse>call, Response<TopicsResponse> response) {


                topicsDataArrayList.clear();

                //      Log.e("asd", "Number of movies received: " + response.body().getError()+"");
                topicsDataArrayList = response.body().getData();

                boolean error = response.body().getError();

                String msg = response.body().getMessage();

                Log.e("error",error+"");
                Log.e("msg",msg+"");


                if(!error){

                    for(int i=0;i<topicsDataArrayList.size();i++){
                        String id = topicsDataArrayList.get(i).getId()+"";
                        if(previousTopicsList.contains(id)){
                            checkArraylist.add(true);
                        }else {
                            checkArraylist.add(false);
                        }
                    }


                    topicAdapter = new TopicAdapter(getApplicationContext(),topicsDataArrayList,checkArraylist);


                    listView.setAdapter(topicAdapter);


                    progressSweetAlertDialog.dismiss();



                }else {

                }


                Log.e("asd", "Number of movies received: " + topicsDataArrayList.size());

                // copyToDatabase(  categories,  topics,  questions,tests);

                progressSweetAlertDialog.dismiss();
//                Intent in = new Intent(getActivity(), LoginActivity.class);
//                startActivity(in);

                topicAdapter.setCustomObjectListener(new TopicAdapter.onItemClickListener() {
                    @Override
                    public void onItemClicked(int position) {

                        Log.e("check1",checkArraylist.get(position)+"");

                        if(!checkArraylist.get(position)){
                            checkArraylist.set(position,true);

                        }else {
                            checkArraylist.set(position,false);
                        }

                        Log.e("check2",checkArraylist.get(position)+"");
                        topicAdapter.notifyDataSetChanged();
                    }

                });



            }

            @Override
            public void onFailure(Call<TopicsResponse>call, Throwable t) {
                // Log error here since request failed
                Log.e("das", t.toString());
                progressSweetAlertDialog.dismiss();
            }
        });


    }


    public void sendSelected(){

        progressSweetAlertDialog.show();
        //   String storeId = sharePref.getshareprefdatastring(SharePref.TOKEN);
        String storeId = "2";
        String appid = "d6847a8652cc7cbf9b92f8f634110fff";

        Log.e("userID",userid);

        selectedTopics = topicsGenerator();
        ApiInterafce apiService =
                ApiClient.getClient().create(ApiInterafce.class);

        Call<UpdateResponse> call = apiService.sendTopics(appid,userid,selectedTopics);
        call.enqueue(new Callback<UpdateResponse>() {
            @Override
            public void onResponse(Call<UpdateResponse>call, Response<UpdateResponse> response) {

                //      Log.e("asd", "Number of movies received: " + response.body().getError()+"");


                boolean error = response.body().getError();

                String msg = response.body().getMessage();

                Log.e("error",error+"");
                Log.e("msg",msg+"");




                if(!error){

                    progressSweetAlertDialog.dismiss();
                    sharePref.setshareprefdatastring(SharePref.TOPICS,selectedTopics+"");

                    finish();


                }else {
                    progressSweetAlertDialog.dismiss();
                    doneDialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
                    doneDialog.setTitleText(msg);
                    // doneDialog.setContentText("Login unsuccessful");
                    doneDialog.show();

                }





            }

            @Override
            public void onFailure(Call<UpdateResponse>call, Throwable t) {
                // Log error here since request failed
                Log.e("das", t.toString());
                progressSweetAlertDialog.dismiss();
            }
        });


    }

    @Override
    public void onItemClicked(int position) {

    }
}
