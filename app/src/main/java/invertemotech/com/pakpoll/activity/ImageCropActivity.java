// "Therefore those skilled at the unorthodox
// are infinite as heaven and earth,
// inexhaustible as the great rivers.
// When they come to an end,
// they begin again,
// like the days and months;
// they die and are reborn,
// like the four seasons."
//
// - Sun Tsu,
// "The Art of War"

package invertemotech.com.pakpoll.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.utils.DiskCacheUtils;
import com.nostra13.universalimageloader.utils.MemoryCacheUtils;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

import cn.pedant.SweetAlert.SweetAlertDialog;
import invertemotech.com.pakpoll.R;
import invertemotech.com.pakpoll.fragment.CropFragment;
import invertemotech.com.pakpoll.retrofitmodel.ApiClient;
import invertemotech.com.pakpoll.retrofitmodel.ApiInterafce;
import invertemotech.com.pakpoll.retrofitmodel.UpdateImageResponse;
import invertemotech.com.pakpoll.util.CropImageViewOptions;
import invertemotech.com.pakpoll.util.SharePref;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ImageCropActivity extends AppCompatActivity
        implements CropImageView.OnSetImageUriCompleteListener, CropImageView.OnCropImageCompleteListener {

    //region: Fields and Consts

    File imagefile;
    DrawerLayout mDrawerLayout;

    private ActionBarDrawerToggle mDrawerToggle;

    SweetAlertDialog doneDialog,progressSweetAlertDialog;

    MultipartBody.Part body;
    private CropFragment mCurrentFragment;
    private CropImageView mCropImageView;
    private Uri mCropImageUri;
      CropImageViewOptions mCropImageViewOptions = new CropImageViewOptions();
    //endregion
    Uri imageUri;

    Button done,cancel;

    SharePref  sharePref;

    public void setCurrentFragment(CropFragment fragment) {
        mCurrentFragment = fragment;
    }

    public void setCurrentOptions(CropImageViewOptions options) {
        mCropImageViewOptions = options;

    }

    String userId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_main_rect);


        String extra = getIntent().getStringExtra("imageuri");
        Log.e("s",extra+" check");
        imageUri = Uri.parse(extra);
        done = (Button) findViewById(R.id.done);
        cancel = (Button) findViewById(R.id.cancel);
        mCropImageView = (CropImageView) findViewById(R.id.cropImageView);
        mCropImageView.setOnSetImageUriCompleteListener(this);
        mCropImageView.setOnCropImageCompleteListener(this);

        sharePref = new SharePref(getApplicationContext());

        userId = sharePref.getshareprefdatastring(SharePref.USERID);

        mCropImageView.setImageUriAsync(imageUri);

        mCropImageViewOptions = new CropImageViewOptions();
        mCropImageView.setScaleType(CropImageView.ScaleType.FIT_CENTER);
        CropImageViewOptions options = new CropImageViewOptions();
        mCropImageView.setFixedAspectRatio(true);
        progressSweetAlertDialog = new SweetAlertDialog(ImageCropActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bitmap cropped = mCropImageView.getCroppedImage();

                String random = UUID.randomUUID().toString();

                imagefile = new File(getCacheDir(), random+userId+".png");
                try {
                    imagefile.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }

               // Bitmap bitmap = cropped;
               // Bitmap yourBitmap;
                Bitmap bitmap = Bitmap.createScaledBitmap(cropped, 300, 300, true);

                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
                byte[] bitmapdata = bos.toByteArray();

//write the bytes in file
                FileOutputStream fos = null;
                try {
                    fos = new FileOutputStream(imagefile);
                    fos.write(bitmapdata);
                    fos.flush();
                    fos.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Log.e("filetest",imagefile.getPath());

                uploadImages();

// or (must subscribe to async event using cropImageView.setOnCropImageCompleteListener(listener))
                mCropImageView.getCroppedImageAsync();
            }
        });

        setTheme();

    }


    public void setCropImageViewOptions(CropImageViewOptions options) {
        mCropImageView.setScaleType(options.scaleType);
        mCropImageView.setCropShape(options.cropShape);
        mCropImageView.setGuidelines(options.guidelines);
        mCropImageView.setAspectRatio(options.aspectRatio.first, options.aspectRatio.second);
        mCropImageView.setFixedAspectRatio(options.fixAspectRatio);
        mCropImageView.setMultiTouchEnabled(options.multitouch);
        mCropImageView.setShowCropOverlay(options.showCropOverlay);
        mCropImageView.setShowProgressBar(options.showProgressBar);
        mCropImageView.setAutoZoomEnabled(options.autoZoomEnabled);
        mCropImageView.setMaxZoom(options.maxZoomLevel);
    }

    @Override
    public void onCropImageComplete(CropImageView view, CropImageView.CropResult result) {


        handleCropResult(result);

    }

    @Override
    public void onSetImageUriComplete(CropImageView view, Uri uri, Exception error) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                Log.e("url",resultUri.toString());

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    private void handleCropResult(CropImageView.CropResult result) {
        if (result.getError() == null) {


            if (result.getUri() != null) {
                Log.e("URI", result.getUri().toString()+" nm");
            }


        } else {
            Log.e("AIC", "Failed to crop image", result.getError());
            Toast.makeText(getApplicationContext(), "Image crop failed: " + result.getError().getMessage(), Toast.LENGTH_LONG).show();
        }
    }


    public void setTheme(){


        int color = sharePref.getshareprefdata(SharePref.THEMECOLOR);

        cancel.setBackgroundColor(color);
        done.setBackgroundColor(color);

    }


    public void uploadImages(){

        String appid = "d6847a8652cc7cbf9b92f8f634110fff";
        progressSweetAlertDialog.show();
        ApiInterafce apiService =
                ApiClient.getClient().create(ApiInterafce.class);

      //  Log.e("u",current_user_id+"");

        RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), imagefile);
        body = MultipartBody.Part.createFormData("photo", imagefile.getName(), reqFile);


        Call<UpdateImageResponse> call = apiService.postImage(appid,Integer.parseInt(userId),body);
        call.enqueue(new Callback<UpdateImageResponse>() {
            @Override
            public void onResponse(Call<UpdateImageResponse>call, Response<UpdateImageResponse> response) {

                //      Log.e("asd", "Number of movies received: " + response.body().getError()+"");


                boolean error = response.body().getError();

                String msg = response.body().getMessage();

                Log.e("error",error+"");
                Log.e("msg",msg+"");

                if(!error){

                    sharePref.setshareprefdatastring(SharePref.PROFILEPIC,response.body().getData()+"");
                    Log.e("image",response.body().getData());
                    progressSweetAlertDialog.dismiss();

                    DisplayImageOptions defaultOptions;

                    try {

                        DiskCacheUtils.removeFromCache(response.body().getData(), ImageLoader.getInstance().getDiskCache());

                        MemoryCacheUtils.removeFromCache(response.body().getData(), ImageLoader.getInstance().getMemoryCache());
                    }catch (Exception e){

                    }


                    Toast.makeText(getApplicationContext(),"Successfully Uploaded",Toast.LENGTH_LONG).show();

                    finish();


                }else {


                    progressSweetAlertDialog.dismiss();
                    Toast.makeText(getApplicationContext(),"Something is wrong, Try again later",Toast.LENGTH_LONG).show();

                }





            }

            @Override
            public void onFailure(Call<UpdateImageResponse>call, Throwable t) {
                // Log error here since request failed
                Log.e("das", t.toString());
                progressSweetAlertDialog.dismiss();
            }
        });


    }
}
