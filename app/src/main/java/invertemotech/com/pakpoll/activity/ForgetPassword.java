package invertemotech.com.pakpoll.activity;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import cn.pedant.SweetAlert.SweetAlertDialog;
import invertemotech.com.pakpoll.R;
import invertemotech.com.pakpoll.util.ConnectionDetector;
import invertemotech.com.pakpoll.util.SharePref;

public class ForgetPassword extends AppCompatActivity {

    TextView topTxt,submit;

    EditText email;

    ImageButton backBtn;

    int success = 0 ;
    String message;


    JSONObject json;

    String emailString;





    boolean isInternetAvailable;
    ConnectionDetector cd;

    Typeface myTypeface, custom;

    SharePref sharePref;

    TextView web,contact;


    SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);

        sharePref = new SharePref(getApplicationContext());

        backBtn = (ImageButton) findViewById(R.id.backBtn);
        email = (EditText) findViewById(R.id.email);

        submit = (TextView) findViewById(R.id.submit);



        topTxt = (TextView) findViewById(R.id.barText);




        setTheme();


        cd = new ConnectionDetector(getApplicationContext());

        isInternetAvailable = cd.isConnectingToInternet();


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                finish();
            }
        });


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                isInternetAvailable = cd.isConnectingToInternet();
                emailString =  email.getText().toString();
                progressSweetAlertDialog = new SweetAlertDialog(ForgetPassword.this, SweetAlertDialog.PROGRESS_TYPE);
                progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
                progressSweetAlertDialog.setTitleText("Loading");
                progressSweetAlertDialog.setCancelable(false);

                submit.setEnabled(true);

                if(!emailString.isEmpty()) {
                    if(isInternetAvailable) {

                    }else{

                        Toast.makeText(getApplicationContext(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();

                    }
                }else{

                    doneDialog = new SweetAlertDialog(ForgetPassword.this, SweetAlertDialog.SUCCESS_TYPE);
                    doneDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                    doneDialog.setContentText("Give your email address");
                    doneDialog.show();
                }
            }
        });



    }

    public void setTheme(){
        RelativeLayout topPanel = (RelativeLayout) findViewById(R.id.topBar);

        int color = sharePref.getshareprefdata(SharePref.THEMECOLOR);

        topPanel.setBackgroundColor(color);

        submit.setBackgroundColor(color);

    }

//    public void getTopics(){
//
//        //   String storeId = sharePref.getshareprefdatastring(SharePref.TOKEN);
//
//        appid = "d6847a8652cc7cbf9b92f8f634110fff";
//
//
//
//        ApiInterafce apiService =
//                ApiClient.getClient().create(ApiInterafce.class);
//
//        Call<SettingsResponse> call = apiService.getSettings(appid);
//        call.enqueue(new Callback<SettingsResponse>() {
//            @Override
//            public void onResponse(Call<SettingsResponse>call, Response<SettingsResponse> response) {
//
//
//
//                //      Log.e("asd", "Number of movies received: " + response.body().getError()+"");
//                // topicsDataArrayList = response.body().getData();
//
//                boolean error = response.body().getError();
//
//                String msg = response.body().getMessage();
//
//
//
//                Log.e("error",error+"");
//                Log.e("msg",msg+"");
//
//
//                if(!error){
//
//
//
//                    posterUrl = response.body().getData().getPoster();
//                    updateDateFromDb = response.body().getData().getPosterupdated();
//
//                    colorValue = response.body().getData().getThemecolor();
//                    setData();
//
//
//
//
//
//
//                }else {
//                    Toast.makeText(getApplicationContext(),"No internet Connection",Toast.LENGTH_LONG).show();
//
//                }
//
//
//
//
//
//            }
//
//            @Override
//            public void onFailure(Call<SettingsResponse>call, Throwable t) {
//                // Log error here since request failed
//                Log.e("das", t.toString());
//                //   progressSweetAlertDialog.dismiss();
//            }
//        });
//
//
//    }


}
