package invertemotech.com.pakpoll.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import invertemotech.com.pakpoll.R;
import invertemotech.com.pakpoll.adapter.CommentAdapter;
import invertemotech.com.pakpoll.retrofitmodel.ApiClient;
import invertemotech.com.pakpoll.retrofitmodel.ApiInterafce;
import invertemotech.com.pakpoll.retrofitmodel.CommentData;
import invertemotech.com.pakpoll.retrofitmodel.CommentsResponse;
import invertemotech.com.pakpoll.retrofitmodel.UpdateResponse;
import invertemotech.com.pakpoll.util.ConnectionDetector;
import invertemotech.com.pakpoll.util.SharePref;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static invertemotech.com.pakpoll.R.id.comment;

public class CommentActivity extends AppCompatActivity {

    ListView list;
    int pollId;

    boolean isInternetOn;
    SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;
    ConnectionDetector cd;

    ImageButton done;
    EditText commentEdt;
    String commentString,userid;
    SharePref sharePref;
    TextView info;
    ArrayList<CommentData> commentDataArrayList;

    CommentAdapter commentAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);

        pollId = getIntent().getIntExtra("id",0);
        list = (ListView) findViewById(R.id.list);
        ImageButton backBtn = (ImageButton) findViewById(R.id.menu);

        commentDataArrayList = new ArrayList<>();

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        sharePref = new SharePref(getApplicationContext());

        userid = sharePref.getshareprefdatastring(SharePref.USERID);

        cd = new ConnectionDetector(getApplicationContext());

        isInternetOn = cd.isConnectingToInternet();
        commentEdt = (EditText) findViewById(comment);
        info = (TextView) findViewById(R.id.info);
        done = (ImageButton) findViewById(R.id.done);







        commentAdapter = new CommentAdapter(getApplicationContext(),commentDataArrayList);

        list.setAdapter(commentAdapter);


        progressSweetAlertDialog = new SweetAlertDialog(CommentActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);

        if(isInternetOn) {

            comments();



        }else{

            Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_LONG).show();
        }




        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(isInternetOn) {

                    commentString = commentEdt.getText().toString();

                    if(!commentString.equals("")) {
                        sendComment();
                    }

                }else{

                    Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_LONG).show();
                }


            }
        });


        setTheme();

    }

    public void sendComment(){

        String appid = "d6847a8652cc7cbf9b92f8f634110fff";

        ApiInterafce apiService =
                ApiClient.getClient().create(ApiInterafce.class);

        Call<UpdateResponse> call = apiService.comment(appid,Integer.parseInt(userid),pollId,commentString);
        call.enqueue(new Callback<UpdateResponse>() {
            @Override
            public void onResponse(Call<UpdateResponse>call, Response<UpdateResponse> response) {

                //      Log.e("asd", "Number of movies received: " + response.body().getError()+"");


                boolean error = response.body().getError();

                String msg = response.body().getMessage();

                Log.e("error",error+"");
                Log.e("msg",msg+"");

                if(!error){

                    commentEdt.setText("");
                    CommentData commentData = new CommentData();
                    commentData.setComment(commentString);
                    commentData.setName(sharePref.getshareprefdatastring(SharePref.USERNAME));
                    commentData.setUserid(Integer.parseInt(userid));
                    commentData.setPicture("");
                    commentDataArrayList.add(commentData);

                    commentAdapter.notifyDataSetChanged();

                    info.setText((commentAdapter.getCount())+" Comments");
                    list.setSelection(commentAdapter.getCount()-1);


                }else {

                    Toast.makeText(getApplicationContext(),"Something is wrong, Try again later",Toast.LENGTH_LONG).show();

                }





            }

            @Override
            public void onFailure(Call<UpdateResponse>call, Throwable t) {
                // Log error here since request failed
                Log.e("das", t.toString());
                progressSweetAlertDialog.dismiss();
            }
        });


    }

    public void setTheme(){
        RelativeLayout topPanel = (RelativeLayout) findViewById(R.id.topPanel);

        int color = sharePref.getshareprefdata(SharePref.THEMECOLOR);

        topPanel.setBackgroundColor(color);


    }

    public void comments(){

        String appid = "d6847a8652cc7cbf9b92f8f634110fff";
        progressSweetAlertDialog.show();
        ApiInterafce apiService =
                ApiClient.getClient().create(ApiInterafce.class);

        Call<CommentsResponse> call = apiService.getComments(appid,pollId);
        call.enqueue(new Callback<CommentsResponse>() {
            @Override
            public void onResponse(Call<CommentsResponse>call, Response<CommentsResponse> response) {

                //      Log.e("asd", "Number of movies received: " + response.body().getError()+"");


                boolean error = response.body().getError();

                String msg = response.body().getMessage();

                Log.e("error",error+"");
                Log.e("msg",msg+"");

                if(!error){

                    commentDataArrayList = response.body().getData();

                      commentAdapter = new CommentAdapter(getApplicationContext(),commentDataArrayList);

                    list.setAdapter(commentAdapter);



                    info.setText((commentAdapter.getCount())+" Comments");

                    progressSweetAlertDialog.dismiss();



                }else {


                    progressSweetAlertDialog.dismiss();
                    Toast.makeText(getApplicationContext(),"Something is wrong, Try again later",Toast.LENGTH_LONG).show();

                }





            }

            @Override
            public void onFailure(Call<CommentsResponse>call, Throwable t) {
                // Log error here since request failed
                Log.e("das", t.toString());
                progressSweetAlertDialog.dismiss();
            }
        });


    }
}
