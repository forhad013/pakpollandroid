package invertemotech.com.pakpoll.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.utils.DiskCacheUtils;
import com.nostra13.universalimageloader.utils.MemoryCacheUtils;
import com.sw926.imagefileselector.ImageCropper;
import com.sw926.imagefileselector.ImageFileSelector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import invertemotech.com.pakpoll.R;
import invertemotech.com.pakpoll.luminous.pick.CustomGallery;
import invertemotech.com.pakpoll.retrofitmodel.ApiClient;
import invertemotech.com.pakpoll.retrofitmodel.ApiInterafce;
import invertemotech.com.pakpoll.retrofitmodel.UpdateImageResponse;
import invertemotech.com.pakpoll.util.ConnectionDetector;
import invertemotech.com.pakpoll.util.SharePref;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ImageUploadActivity extends AppCompatActivity {


    ImageCropper mImageCropper;

    ImageFileSelector mImageFileSelector;

    String userID,imageLink;

    Button exit,done,crop;

    ImageView imageView;

    TextView gallery,camera;

    String currentImageFile;

    ImageLoader imageLoader;

    MultipartBody.Part body;

    ImageButton backBtn;

    JSONObject json;


    String encodedFileString;

    int success;

    SharePref sharePref;
    String errorString;

    SweetAlertDialog doneDialog,progressSweetAlertDialog;

    String currentImageCategory;
    int current_user_id;
    ConnectionDetector cd;
    boolean isInternetPresent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_upload);

        sharePref = new SharePref(getApplicationContext());
        current_user_id = Integer.parseInt(sharePref.getshareprefdatastring(SharePref.USERID));

        progressSweetAlertDialog = new SweetAlertDialog(ImageUploadActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);



        cd = new ConnectionDetector(getApplicationContext());

        isInternetPresent = cd.isConnectingToInternet();





        backBtn = (ImageButton) findViewById(R.id.btnPrev);
          done = (Button)findViewById(R.id.done);
       crop = (Button)findViewById(R.id.crop);

         exit = (Button) findViewById(R.id.cancel);
         gallery = (TextView)findViewById(R.id.gallery);

         camera = (TextView) findViewById(R.id.camera);

          imageView = (ImageView) findViewById(R.id.image);


        sharePref = new SharePref(getApplicationContext());


        userID = sharePref.getshareprefdatastring(SharePref.USERID);




        // save = (Button) dialog.findViewById(R.id.submit);

        progressSweetAlertDialog = new SweetAlertDialog(ImageUploadActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);

        crop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                File file = new File(currentImageFile);

                mImageCropper.cropImage(file);

            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                done.setEnabled(false);
              if(  imageView.getDrawable() != null){
                  encodedFileString= encodeImages(currentImageFile);

                  File file = new File(currentImageFile);
                  RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
                    body = MultipartBody.Part.createFormData("photo", file.getName(), reqFile);

                  uploadImages();


              }

            }
        });

        mImageFileSelector = new ImageFileSelector(this);

        mImageFileSelector.setCallback(new ImageFileSelector.Callback() {
            @Override
            public void onSuccess(final String file) {
                if (!TextUtils.isEmpty(file)) {


                    final Bitmap bitmap = BitmapFactory.decodeFile(file);
                    File imageFile = new File(file);



                    currentImageFile =   imageFile.getPath();

                    Log.e("file", currentImageFile);
                    imageView.setImageBitmap(bitmap);
                    imageView.setVisibility(View.VISIBLE);
                    crop.setVisibility(View.VISIBLE);


                } else {
                    Toast.makeText(getApplicationContext(), "select image file error", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onError() {
                Toast.makeText(getApplicationContext(), "select image file error", Toast.LENGTH_LONG).show();
            }
        });

        mImageCropper = new ImageCropper(this);

        mImageCropper.setCallback(new ImageCropper.ImageCropperCallback() {
            @Override
            public void onCropperCallback(ImageCropper.CropperResult result, File srcFile, File outFile) {
                currentImageFile = "";
                //  mBtnCrop.setVisibility(View.GONE);
                if (result == ImageCropper.CropperResult.success) {
                    currentImageFile = outFile.getPath();

                    final Bitmap bitmap = BitmapFactory.decodeFile(String.valueOf(outFile));
                    File imageFile = new File(String.valueOf(outFile));



                    currentImageFile =   imageFile.getPath();

                    imageView.setImageBitmap(bitmap);
                    crop.setVisibility(View.VISIBLE);

                } else if (result == ImageCropper.CropperResult.error_illegal_input_file) {
                    Toast.makeText(getApplicationContext(), "input file error", Toast.LENGTH_LONG).show();
                } else if (result == ImageCropper.CropperResult.error_illegal_out_file) {
                    Toast.makeText(getApplicationContext(), "output file error", Toast.LENGTH_LONG).show();
                }
            }
        });



        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });



        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("file", "Asdsd");


                mImageFileSelector.selectImage(ImageUploadActivity.this);


            }
        });


        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mImageFileSelector.takePhoto(ImageUploadActivity.this);

            }
        });

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                currentImageFile="";

                finish();
            }
        });

        setTheme();



    }

    public void setTheme(){
        RelativeLayout topPanel = (RelativeLayout) findViewById(R.id.lin1);

        int color = sharePref.getshareprefdata(SharePref.THEMECOLOR);

        topPanel.setBackgroundColor(color);
        camera.setBackgroundColor(color);
        gallery.setBackgroundColor(color);
        done.setBackgroundColor(color);
        crop.setBackgroundColor(color);



    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    //    mImageFileSelector.onSaveInstanceState(outState);
     //   mImageCropper.onSaveInstanceState(outState);
    }




    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
       // mImageFileSelector.onRestoreInstanceState(savedInstanceState);
      //  mImageCropper.onRestoreInstanceState(savedInstanceState);
    }
    @SuppressWarnings("NullableProblems")
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mImageFileSelector.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mImageFileSelector.onActivityResult(requestCode, resultCode, data);
        mImageCropper.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
//            adapter.clear();
//
//            viewSwitcher.setDisplayedChild(1);
            String single_path = data.getStringExtra("single_path");
            //  imageLoader.displayImage("file://" + single_path, imgSinglePick);


            currentImageFile = single_path;


            // Uri uri = data.getPollData();

            Uri uri = Uri.parse("file://"+currentImageFile);

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);


                // Log.d(TAG, String.valueOf(bitmap));

//                ImageView imageView = (ImageView) findViewById(R.id.imageView);
                //  imageView.setImageBitmap(bitmap);


            } catch (IOException e) {
                e.printStackTrace();
            }

        } else if (requestCode == 200 && resultCode == Activity.RESULT_OK) {
            String[] all_path = data.getStringArrayExtra("all_path");

            ArrayList<CustomGallery> dataT = new ArrayList<CustomGallery>();

            for (String string : all_path) {
                CustomGallery item = new CustomGallery();
                item.sdcardPath = string;

                dataT.add(item);
            }


        }
    }




    public void uploadImages(){

        String appid = "d6847a8652cc7cbf9b92f8f634110fff";
        progressSweetAlertDialog.show();
        ApiInterafce apiService =
                ApiClient.getClient().create(ApiInterafce.class);

        Log.e("u",current_user_id+"");


        Call<UpdateImageResponse> call = apiService.postImage(appid,current_user_id,body);
        call.enqueue(new Callback<UpdateImageResponse>() {
            @Override
            public void onResponse(Call<UpdateImageResponse>call, Response<UpdateImageResponse> response) {

                //      Log.e("asd", "Number of movies received: " + response.body().getError()+"");


                boolean error = response.body().getError();

                String msg = response.body().getMessage();

                Log.e("error",error+"");
                Log.e("msg",msg+"");

                if(!error){

                    sharePref.setshareprefdatastring(SharePref.PROFILEPIC,response.body().getData()+"");

                    progressSweetAlertDialog.dismiss();

                    DisplayImageOptions defaultOptions;

                    try {

                        DiskCacheUtils.removeFromCache(response.body().getData(), ImageLoader.getInstance().getDiskCache());

                        MemoryCacheUtils.removeFromCache(response.body().getData(), ImageLoader.getInstance().getMemoryCache());
                    }catch (Exception e){

                    }


                    Toast.makeText(getApplicationContext(),"Successfully Uploaded",Toast.LENGTH_LONG).show();

                    finish();


                }else {


                    progressSweetAlertDialog.dismiss();
                    Toast.makeText(getApplicationContext(),"Something is wrong, Try again later",Toast.LENGTH_LONG).show();

                }


                done.setEnabled(true);



            }

            @Override
            public void onFailure(Call<UpdateImageResponse>call, Throwable t) {
                // Log error here since request failed
                Log.e("das", t.toString());
                progressSweetAlertDialog.dismiss();
            }
        });


    }



    public void getData(JSONObject json){

        String newline = System.getProperty("line.separator");

        try {
            JSONObject result = json.getJSONObject("results");


            JSONArray errors= result.getJSONArray("updateInfo");

            for (int i=0;i<errors.length();i++){

                JSONObject temp = errors.getJSONObject(i);


                String type = temp.getString("imageType");

                if(type.equals("additionalImage")){
                    String link = temp.getString("imageUrl");
                   // db.saveImages(userID,"additional",link);
                    Log.e("lo",link);
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    public String encodeImages(String filePath){

        String encodedString="";

        BitmapFactory.Options options = null;
        options = new BitmapFactory.Options();
        options.inSampleSize = 3;

        Bitmap bitmap;

        bitmap = BitmapFactory.decodeFile(filePath, options);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        // Must compress the Image to reduce image size to make
        // upload easy
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byte_arr = stream.toByteArray();
        // Encode Image to String
        encodedString = Base64.encodeToString(byte_arr,
                Base64.DEFAULT);


        return  encodedString;

    }
}
