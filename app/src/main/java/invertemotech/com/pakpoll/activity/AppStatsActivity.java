package invertemotech.com.pakpoll.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import cn.pedant.SweetAlert.SweetAlertDialog;
import invertemotech.com.pakpoll.R;
import invertemotech.com.pakpoll.retrofitmodel.ApiClient;
import invertemotech.com.pakpoll.retrofitmodel.ApiInterafce;
import invertemotech.com.pakpoll.retrofitmodel.AppStatsData;
import invertemotech.com.pakpoll.retrofitmodel.AppStatsResponse;
import invertemotech.com.pakpoll.util.ConnectionDetector;
import invertemotech.com.pakpoll.util.SharePref;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AppStatsActivity extends AppCompatActivity {

    TextView text1,text2,text3,title1,title2,title3;

    ImageView imageView1,imageView2,imageView3;
    ConnectionDetector cd;

    boolean isInternetPresent;

    SharePref sharePref;
    AppStatsData appStatsData;

    SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_stats);

        text1 = (TextView) findViewById(R.id.text1);
        text2 = (TextView) findViewById(R.id.text2);
        text3 = (TextView) findViewById(R.id.text3);
        title1 = (TextView) findViewById(R.id.title1);
        title2= (TextView) findViewById(R.id.title2);
        title3 = (TextView) findViewById(R.id.title3);
        imageView1 = (ImageView) findViewById(R.id.imageView1);
        imageView2 = (ImageView) findViewById(R.id.imageView2);
        imageView3 = (ImageView) findViewById(R.id.imageView3);

        sharePref = new SharePref(getApplicationContext());
        cd = new ConnectionDetector(getApplicationContext());

        isInternetPresent= cd.isConnectingToInternet();

        progressSweetAlertDialog = new SweetAlertDialog(AppStatsActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);


        ImageButton backBtn = (ImageButton) findViewById(R.id.menu);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        if(isInternetPresent){
            appStats();
        }

        setTheme();
    }



    public void appStats(){

        String appid = "d6847a8652cc7cbf9b92f8f634110fff";
        progressSweetAlertDialog.show();
        ApiInterafce apiService =
                ApiClient.getClient().create(ApiInterafce.class);

        Call<AppStatsResponse> call = apiService.getAppStats(appid);
        call.enqueue(new Callback<AppStatsResponse>() {
            @Override
            public void onResponse(Call<AppStatsResponse>call, Response<AppStatsResponse> response) {

                //      Log.e("asd", "Number of movies received: " + response.body().getError()+"");


                boolean error = response.body().getError();

                String msg = response.body().getMessage();

                Log.e("error",error+"");
                Log.e("msg",msg+"");

                if(!error){

                    appStatsData = response.body().getAppStatsData();


                    progressSweetAlertDialog.dismiss();
                    setData();


                }else {



                    progressSweetAlertDialog.dismiss();
                    Toast.makeText(getApplicationContext(),"Something is wrong, Try again later",Toast.LENGTH_LONG).show();

                }





            }

            @Override
            public void onFailure(Call<AppStatsResponse>call, Throwable t) {
                // Log error here since request failed
                Log.e("das", t.toString());
                progressSweetAlertDialog.dismiss();
            }
        });


    }

    public  void setData(){


        title1.setText(appStatsData.getRegisteredusers() + " Registered User");
        text1.setText(appStatsData.getRegistereduserstext());

        title2.setText(appStatsData.getPollscreated() + " Polls Created");
        text2.setText(appStatsData.getPollscreatedtext());

        title3.setText("Top Topics "+appStatsData.getPopularcategory());
        text3.setText(appStatsData.getRegistereduserstext());



        DisplayImageOptions defaultOptions;

        defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .defaultDisplayImageOptions(defaultOptions)
                .build();
        ImageLoader.getInstance().init(config);


//        String iconUrl=topicModelArrayList.get(position).getIcon().replace("\\","") ;
//        String backgroundUrl=topicModelArrayList.get(position).getBackgroundimage().replace("\\","") ;

        ImageLoader.getInstance().displayImage(appStatsData.getRegistereduserspic(), imageView1, defaultOptions);
        ImageLoader.getInstance().displayImage(appStatsData.getPollscreatedpic(), imageView2, defaultOptions);
        ImageLoader.getInstance().displayImage(appStatsData.getPopularcatimage(), imageView3, defaultOptions);

    }



    public void setTheme(){
        RelativeLayout topPanel = (RelativeLayout) findViewById(R.id.topPanel);

        int color = sharePref.getshareprefdata(SharePref.THEMECOLOR);

        topPanel.setBackgroundColor(color);


    }

}
