package invertemotech.com.pakpoll.activity;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import invertemotech.com.pakpoll.R;
import invertemotech.com.pakpoll.adapter.SpinnerAdapter;
import invertemotech.com.pakpoll.retrofitmodel.ApiClient;
import invertemotech.com.pakpoll.retrofitmodel.ApiInterafce;
import invertemotech.com.pakpoll.retrofitmodel.TopicsData;
import invertemotech.com.pakpoll.retrofitmodel.TopicsResponse;
import invertemotech.com.pakpoll.retrofitmodel.UpdateResponse;
import invertemotech.com.pakpoll.util.ConnectionDetector;
import invertemotech.com.pakpoll.util.SharePref;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewPoll extends AppCompatActivity {
    ArrayList topicsArraylist;

    TextView tags;

    LinearLayout lin1,lin2,lin3,lin4,lin5;
    RelativeLayout lin6;

    ImageButton addNew,cancelBtn1,cancelBtn2,cancelBtn3;

    Spinner topicsSpinner;

    String msg,questionString,option1String,option2String,option3String,option4String;

    Context context;
    public ArrayList<Boolean> checkArraylist;

    ConnectionDetector cd;

    boolean isInternetAvailable;

    String userid,selectedTopics;

    SharePref sharePref;
    
    String appid;

    EditText questionEdt,option1Edt,option2Edt,option3Edt,option4Edt;

    Button submit;

    ArrayList<TopicsData> topicsDataArrayList;
    SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_poll);
        topicsSpinner = (Spinner) findViewById(R.id.topics);
        lin3 = (LinearLayout) findViewById(R.id.lin3);
        lin4 = (LinearLayout) findViewById(R.id.lin4);
        lin5 = (LinearLayout) findViewById(R.id.lin5);
        lin6 = (RelativeLayout) findViewById(R.id.lin6);
        addNew = (ImageButton) findViewById(R.id.addNew);
        cancelBtn1 = (ImageButton) findViewById(R.id.cancel1);
        cancelBtn2 = (ImageButton) findViewById(R.id.cancel2);
        cancelBtn3 = (ImageButton) findViewById(R.id.cancel3);
        submit = (Button) findViewById(R.id.submit);
        questionEdt = (EditText) findViewById(R.id.question);
        option1Edt = (EditText) findViewById(R.id.option1);
        option2Edt = (EditText) findViewById(R.id.option2);
        option3Edt = (EditText) findViewById(R.id.option3);
        option4Edt = (EditText) findViewById(R.id.option4);



        tags = (TextView) findViewById(R.id.tags);
        context = NewPoll.this;

        checkArraylist = new ArrayList<>();
        topicsDataArrayList = new ArrayList<>();
        sharePref = new SharePref(getApplicationContext());

        userid = sharePref.getshareprefdatastring(SharePref.USERID);
        cd = new ConnectionDetector(getApplicationContext());
        isInternetAvailable = cd.isConnectingToInternet();

        progressSweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);


        topicsArraylist = new ArrayList();

        ImageButton backBtn = (ImageButton) findViewById(R.id.menu);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(nullCheck()){

                    if(checkNullSeleted()) {
                        option1String = option1Edt.getText().toString();
                        option2String = option2Edt.getText().toString();
                        option3String = option3Edt.getText().toString();
                        option4String = option4Edt.getText().toString();
                        questionString = questionEdt.getText().toString();

                        submitPoll();
                    }else {
                        doneDialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
                        doneDialog.setTitleText("OOPS");
                        doneDialog.setContentText("Select At Least One Tags");
                        doneDialog.show();
                    }

                }

            }
        });




        addNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                for (int i = 0; i < 2; i++) {
                    if (lin3.getVisibility() == View.GONE) {
                        lin3.setVisibility(View.VISIBLE);

                        break;
                    } else if (lin4.getVisibility() == View.GONE) {
                        lin4.setVisibility(View.VISIBLE);
                        cancelBtn1.setVisibility(View.INVISIBLE);
                        lin6.setVisibility(View.GONE);
                        break;
                    }
//                    else if (lin5.getVisibility() == View.GONE) {
//                        lin5.setVisibility(View.VISIBLE);
//                        lin6.setVisibility(View.GONE);
//                        cancelBtn2.setVisibility(View.INVISIBLE);
//                        break;
//                    }


                }
            }
        });

        cancelBtn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lin3.setVisibility(View.GONE);
                lin6.setVisibility(View.VISIBLE);


            }
        });
        cancelBtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lin4.setVisibility(View.GONE);
                lin6.setVisibility(View.VISIBLE);
                cancelBtn1.setVisibility(View.VISIBLE);
            }
        });
        cancelBtn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lin5.setVisibility(View.GONE);
                lin6.setVisibility(View.VISIBLE);
                cancelBtn2.setVisibility(View.VISIBLE);

            }
        });


        if(isInternetAvailable){
            getTopics();
        }

        tags.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showSpinnerDialog();
            }
        });

        setTheme();

    }


    public void setTheme(){
        RelativeLayout topPanel = (RelativeLayout) findViewById(R.id.topPanel);

        int color = sharePref.getshareprefdata(SharePref.THEMECOLOR);

        topPanel.setBackgroundColor(color);
        addNew.setBackgroundColor(color);
        submit.setBackgroundColor(color);

    }


    public void getTopics(){

        progressSweetAlertDialog.show();
        //   String storeId = sharePref.getshareprefdatastring(SharePref.TOKEN);

        appid = "d6847a8652cc7cbf9b92f8f634110fff";



        ApiInterafce apiService =
                ApiClient.getClient().create(ApiInterafce.class);

        Call<TopicsResponse> call = apiService.getTopicsList(appid);
        call.enqueue(new Callback<TopicsResponse>() {
            @Override
            public void onResponse(Call<TopicsResponse>call, Response<TopicsResponse> response) {


              //  topicsDataArrayList.clear();

                //      Log.e("asd", "Number of movies received: " + response.body().getError()+"");
                topicsDataArrayList = response.body().getData();

                boolean error = response.body().getError();

                String msg = response.body().getMessage();

                Log.e("error",error+"");
                Log.e("msg",msg+"");


                if(!error){

                    for(int i=0;i<topicsDataArrayList.size();i++){
                        checkArraylist.add(false);
                    }


                      topicsArraylist = new ArrayList();

                    for(int i=0;i<topicsDataArrayList.size();i++){

                        topicsArraylist.add(topicsDataArrayList.get(i).getTitle());

                    }


                    ArrayAdapter<String> adapter = new ArrayAdapter <String>(context, android.R.layout.simple_list_item_multiple_choice, topicsArraylist);


                    SpinnerAdapter spinnerAdapter = new SpinnerAdapter(context,topicsArraylist,checkArraylist);

                    topicsSpinner.setAdapter(spinnerAdapter);



//                    ArrayAdapter<String> spinAdapter1 = new ArrayAdapter<String>(context,
//                            R.layout.spinner_item, topicsArraylist);
//                    topicsSpinner.setAdapter(spinAdapter1);
              


                    progressSweetAlertDialog.dismiss();



                }else {

                }


                Log.e("asd", "Number of movies received: " + topicsDataArrayList.size());

                // copyToDatabase(  categories,  topics,  questions,tests);

                progressSweetAlertDialog.dismiss();
//                Intent in = new Intent(getActivity(), LoginActivity.class);
//                startActivity(in);




            }

            @Override
            public void onFailure(Call<TopicsResponse>call, Throwable t) {
                // Log error here since request failed
                Log.e("das", t.toString());
                progressSweetAlertDialog.dismiss();
            }
        });


    }

    public void showSpinnerDialog(){
        final Dialog dialog = new Dialog(this,  android.R.style.Theme_Dialog);
        // Include dialog.xml file
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);

        new ContextThemeWrapper(context, android.R.style.Theme_Dialog);
        dialog.setContentView(R.layout.spinner_dialog);
        // Set dialog title
        //dialog.setTitle("Category Add");

        dialog.show();

        TextView title = (TextView) dialog.findViewById(R.id.title);
        Button done = (Button) dialog.findViewById(R.id.done);


        Button exit = (Button) dialog.findViewById(R.id.cancel);
        final ListView list = (ListView) dialog.findViewById(R.id.list);


        final SpinnerAdapter spinnerAdapter = new SpinnerAdapter(context,topicsArraylist,checkArraylist);

        list.setAdapter(spinnerAdapter);

        int color = sharePref.getshareprefdata(SharePref.THEMECOLOR);


        title.setBackgroundColor(color);

        done.setBackgroundColor(color);

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String topics =topicsGenerator();

                if(topics.equals("")){
                    tags.setText("No Topics Selected" );
                }else {
                    tags.setText(topicsGenerator());
                }

                dialog.dismiss();

            }
        });

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        spinnerAdapter.setCustomObjectListener(new SpinnerAdapter.onItemClickListener() {
            @Override
            public void onItemClicked(int position) {
                Log.e("check1",checkArraylist.get(position)+"");

                if(!checkArraylist.get(position)){
                    checkArraylist.set(position,true);

                }else {
                    checkArraylist.set(position,false);
                }

                Log.e("check2",checkArraylist.get(position)+"");
                spinnerAdapter.notifyDataSetChanged();
            }
        });

    }

    public String topicsGenerator(){

        String topics="";

        for(int i=0;i<checkArraylist.size();i++){
            if(checkArraylist.get(i)){
                if (topics.equals("")) {
                    topics =  topics +topicsDataArrayList.get(i).getTitle().toString();
                }else{
                    topics =  topics +", " + topicsDataArrayList.get(i).getTitle().toString();
                }

            }
        }

        Log.e("topics",topics);

        return topics;
    }

    public void submitPoll(){

        progressSweetAlertDialog.show();
        //   String storeId = sharePref.getshareprefdatastring(SharePref.TOKEN);
        String userID = sharePref.getshareprefdatastring(SharePref.USERID);
        String appid = "d6847a8652cc7cbf9b92f8f634110fff";



        ApiInterafce apiService =
                ApiClient.getClient().create(ApiInterafce.class);

        Call<UpdateResponse> call = apiService.submitPoll(appid,Integer.parseInt(userID),getTags(),questionString,option1String,option2String,option3String,option4String);
        call.enqueue(new Callback<UpdateResponse>() {
            @Override
            public void onResponse(Call<UpdateResponse>call, Response<UpdateResponse> response) {

                //      Log.e("asd", "Number of movies received: " + response.body().getError()+"");


                boolean error = response.body().getError();

                String msg = response.body().getMessage();

                Log.e("error",error+"");
                Log.e("msg",msg+"");



                if(!error){


                    progressSweetAlertDialog.dismiss();


                    doneDialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
                    doneDialog.setTitleText(msg);
                    doneDialog.setConfirmText("Ok");
                    doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();
                            finish();
                        }
                    });
                    doneDialog.show();




                }else {
                    progressSweetAlertDialog.dismiss();
                    doneDialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
                    doneDialog.setTitleText(msg);
                    // doneDialog.setContentText("Login unsuccessful");
                    doneDialog.show();

                }





            }

            @Override
            public void onFailure(Call<UpdateResponse>call, Throwable t) {
                // Log error here since request failed
                Log.e("das", t.toString());
                progressSweetAlertDialog.dismiss();
            }
        });


    }

    public boolean checkNullSeleted(){
        boolean check = false;

        for(int i=0;i<checkArraylist.size();i++){

            if(checkArraylist.get(i)){
                check = true;
                break;
            }
        }

        return  check;

    }


    public String getTags(){

        String topics="";

        for(int i=0;i<checkArraylist.size();i++){
            if(checkArraylist.get(i)){
                if (topics.equals("")) {
                    topics =  topics +topicsDataArrayList.get(i).getId().toString();
                }else{
                    topics =  topics +"," + topicsDataArrayList.get(i).getId().toString();
                }

            }
        }

        Log.e("topics",topics);

        return topics;
    }



    public boolean nullCheck() {
        boolean flag = false;



        if (!questionEdt.getText().toString().trim().equalsIgnoreCase("")) {
            if (!option1Edt.getText().toString().trim().equalsIgnoreCase("")) {
                if (!option2Edt.getText().toString().trim().equalsIgnoreCase("")) {

                        return true;

                } else {
                    msg = "Please Enter Your Question!";
                }

            } else {
                msg = "Please Enter Option 1 !";
            }

        } else {
            msg = "Please Enter Option 2 !";
        }


        if(!flag) {
            new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("OOPS...")
                    .setContentText(msg)
                    .show();
        }



        return flag;


    }

}
