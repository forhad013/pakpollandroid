package invertemotech.com.pakpoll.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import invertemotech.com.pakpoll.R;
import invertemotech.com.pakpoll.retrofitmodel.ApiClient;
import invertemotech.com.pakpoll.retrofitmodel.ApiInterafce;
import invertemotech.com.pakpoll.retrofitmodel.GraphData;
import invertemotech.com.pakpoll.retrofitmodel.GraphResponse;
import invertemotech.com.pakpoll.util.ConnectionDetector;
import invertemotech.com.pakpoll.util.SharePref;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GraphActivity extends AppCompatActivity {

    ArrayList<PieEntry> provinceData;
    ArrayList<PieEntry> genderData;

    boolean isInternetPresent;

    SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;
    ConnectionDetector cd;
    PieChart chart1,chart2;
    int pollId;
    int male;
    int female ;
    int Sindh  ;
    int Punjab ;
    int Khyber ;
    int Balochistan ;
    int Kashmir  ;
    int Gilgit;

    GraphData graphData;
    SharePref sharePref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);

        pollId = getIntent().getIntExtra("id",0);
        cd = new ConnectionDetector(getApplicationContext());

        sharePref = new SharePref(getApplicationContext());
        isInternetPresent = cd.isConnectingToInternet();
        ImageButton backBtn = (ImageButton) findViewById(R.id.menu);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        chart1 = (PieChart) findViewById(R.id.chart1);
        chart2 = (PieChart) findViewById(R.id.chart2);


        progressSweetAlertDialog = new SweetAlertDialog(GraphActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);

        if(isInternetPresent) {

             graph();

        }else{

            Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_LONG).show();
        }

        setTheme();
        setGraph();


    }


    public void setTheme(){
        RelativeLayout topPanel = (RelativeLayout) findViewById(R.id.topPanel);

        int color = sharePref.getshareprefdata(SharePref.THEMECOLOR);

        topPanel.setBackgroundColor(color);


    }



    public void setGraph(){

        male         = 2;
        female       = 1;
        Sindh        = 0;
        Punjab       = 0;
        Khyber       = 0;
        Balochistan  = 1;
        Kashmir      = 1;
        Gilgit =       0;

        try {
            male = graphData.getMalevoters();
            female = graphData.getFemalevoters();
            Sindh = graphData.getSindhvoters();
            Punjab = graphData.getPunjabvoters();
            Khyber = graphData.getKhybervoters();
            Balochistan = graphData.getBalochistanvoters();
            Kashmir = graphData.getKashmirvoters();
            Gilgit = graphData.getGilgitvoters();
        }catch (Exception e){

        }


        Log.e("g",Gilgit+"");
        Log.e("g",Sindh+"");
        Log.e("g",Punjab+"");
        Log.e("g",Khyber+"");
        Log.e("g",Balochistan+"");
        Log.e("g",Kashmir+"");
        Log.e("g",female+"");
        Log.e("g",male+"");




        provinceData = new ArrayList<PieEntry>();
        provinceData.add(new PieEntry((float)(Sindh),"Sindh"));
        provinceData.add(new PieEntry((float)(Punjab),"Punjab"));
        provinceData.add(new PieEntry((float)(Khyber),"Khyber Pakh.."));
        provinceData.add(new PieEntry((float)(Balochistan),"Balochistan"));
        provinceData.add(new PieEntry((float)(Kashmir),"Kashmir"));
        provinceData.add(new PieEntry((float)(Gilgit),"Gilgit-Baltis.."));




        genderData = new ArrayList<>();
        genderData.add(new PieEntry((float)(male),"Male"));
        genderData.add(new PieEntry((float)(female),"Female"));



        setChart1();

        setChart2();
    }


    public void setChart1(){

      //  chart1.setUsePercentValues(true);
        // chart2.setDescription("");
         chart1.setExtraOffsets(5, 10, 5, 5);
        chart1.setNoDataText("No GraphData");

        chart1.setDragDecelerationFrictionCoef(0.95f);

//       95f tf = Typeface.createFromAsset(getAssets(), "OpenSans-Regular.ttf");
//
//        ChartRev.setCenterTextTypeface(Typeface.createFromAsset(getAssets(), "OpenSans-Light.ttf"));
        //    ChartRev.setCenterText(generateCenterSpannableText());

        //  chart2.setDrawHoleEnabled(true);
        chart1.setEntryLabelColor(R.color.app_color);
//        chart2.setTransparentCircleColor(Color.WHITE);
//        chart2.setTransparentCircleAlpha(110);

        chart1.setHoleRadius(0f);
        chart1.setTransparentCircleRadius(0f);
        chart1.setDrawEntryLabels(false);
        //  chart2.setDrawCenterText(true);

        //  chart2.setRotationAngle(0);
        // enable rotation of the chart by touch
        chart1.setRotationEnabled(true);
        chart1.setHighlightPerTapEnabled(true);
        chart1.setDescription(null);

        chart1.setUsePercentValues(false);
        chart1.setDrawEntryLabels(false);

        // ChartRev.setUnit(" €");
        // ChartRev.setDrawUnitsInChart(true);

        // add a selection listener
        //ChartRev.setOnChartValueSelectedListener(getActivity());

        setDataExp1();

        chart1.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        // ChartRev.spin(2000, 0, 360);

        Legend l = chart1.getLegend();
        l.setPosition(Legend.LegendPosition.RIGHT_OF_CHART);
        l.setXEntrySpace(0f);


        l.setTextColor(Color.BLACK);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);
        l.setTextSize(8f);
    }

    private void setDataExp1() {



        ArrayList<PieEntry> yVals1 = new ArrayList<PieEntry>();

        yVals1 = provinceData;



        PieDataSet dataSet = new PieDataSet(yVals1, "Province");
        dataSet.setSliceSpace(0f);
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);

        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);

        data.setValueTextColor(Color.BLACK);
        // data.setValueTypeface(tf);
        chart1.setData(data);

        // undo all highlights
        chart1.highlightValues(null);

        chart1.invalidate();
    }


    public void setChart2(){

        chart2.setUsePercentValues(true);
        // chart2.setDescription("");
        chart2.setExtraOffsets(5, 10, 5, 5);
        chart2.setNoDataText("No GraphData");

        chart2.setDragDecelerationFrictionCoef(0.95f);

//       95f tf = Typeface.createFromAsset(getAssets(), "OpenSans-Regular.ttf");
//
//        ChartRev.setCenterTextTypeface(Typeface.createFromAsset(getAssets(), "OpenSans-Light.ttf"));
        //    ChartRev.setCenterText(generateCenterSpannableText());

         chart2.setDrawHoleEnabled(true);
        chart2.setEntryLabelColor(R.color.app_color);
//        chart2.setTransparentCircleColor(Color.WHITE);
//        chart2.setTransparentCircleAlpha(110);

       chart2.setHoleRadius(60f);
       chart2.setTransparentCircleRadius(0f);
       // chart2.setDrawEntryLabels(true);

      //  chart2.setDrawCenterText(true);

      //  chart2.setRotationAngle(0);
        // enable rotation of the chart by touch
        chart2.setRotationEnabled(false);
        chart2.setHighlightPerTapEnabled(true);
        chart2.setDescription(null);


        // ChartRev.setUnit(" €");
        // ChartRev.setDrawUnitsInChart(true);

        // add a selection listener
        //ChartRev.setOnChartValueSelectedListener(getActivity());

        setDataExp2();

        chart2.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        // ChartRev.spin(2000, 0, 360);

        Legend l = chart2.getLegend();
        l.setPosition(Legend.LegendPosition.RIGHT_OF_CHART);
        l.setXEntrySpace(0f);


        l.setTextColor(Color.BLACK);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);
        l.setTextSize(8f);
    }

    private void setDataExp2() {



        ArrayList<PieEntry> yVals1 = new ArrayList<PieEntry>();

        yVals1 = genderData;



        PieDataSet dataSet = new PieDataSet(yVals1, "Gender");
        dataSet.setSliceSpace(0f);
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);

        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);

        data.setValueTextColor(Color.BLACK);
        // data.setValueTypeface(tf);
        chart2.setData(data);

        // undo all highlights
        chart2.highlightValues(null);

        chart2.invalidate();

        checkNull();
    }



    public void checkNull(){

        if(male==0&&female==0){
            chart2.removeAllViews();
            chart2.setCenterText("No AppStatsData");
            TextView No2= (TextView) findViewById(R.id.noData2);
            No2.setText("No AppStatsData");
        }


        if(Khyber==0&& Sindh  ==0&& Punjab ==0&& Balochistan ==0&& Kashmir ==0&& Gilgit==0){
            chart1.removeAllViews();
            chart1.setCenterText("No AppStatsData");
            TextView No1= (TextView) findViewById(R.id.noData1);
            Log.e("b","b");
            No1.setText("No AppStatsData");


        }
    }


    public void graph(){

        String appid = "d6847a8652cc7cbf9b92f8f634110fff";
        progressSweetAlertDialog.show();
        ApiInterafce apiService =
                ApiClient.getClient().create(ApiInterafce.class);

        Call<GraphResponse> call = apiService.getPollStats(appid,pollId);
        call.enqueue(new Callback<GraphResponse>() {
            @Override
            public void onResponse(Call<GraphResponse>call, Response<GraphResponse> response) {

                //      Log.e("asd", "Number of movies received: " + response.body().getError()+"");


                boolean error = response.body().getError();

                String msg = response.body().getMessage();

                Log.e("error",error+"");
                Log.e("msg",msg+"");

                if(!error){

                 graphData = response.body().getGraphData();
//                    male = response.body().getGraphData().getMalevoters();
//                    female = response.body().getGraphData().getFemalevoters();
//                    Sindh = response.body().getGraphData().getSindhvoters();
//                    Punjab = response.body().getGraphData().getPunjabvoters();
//                    Khyber = response.body().getGraphData().getKhybervoters();
//                    Balochistan = response.body().getGraphData().getBalochistanvoters();
//                    Kashmir = response.body().getGraphData().getKashmirvoters();
//                    Gilgit = response.body().getGraphData().getGilgitvoters();
                  //  Log.e("male",graphData.getMalevoters()+"");

                    progressSweetAlertDialog.dismiss();
                    setGraph();


                }else {

                    ArrayList<PieEntry> yVals1 = new ArrayList<PieEntry>();

                    PieDataSet dataSet = new PieDataSet(yVals1, "Gender");
                    PieData data = new PieData(dataSet);
                    chart1.invalidate();
                    chart2.invalidate();
                    chart2.setData(data);
                    ArrayList<PieEntry> yVals2 = new ArrayList<PieEntry>();

                    PieDataSet dataSet1 = new PieDataSet(yVals2, "Province");
                    PieData data1 = new PieData(dataSet1);
                    chart1.setData(data1);
                    chart1.setDrawCenterText(true);
                    chart2.setDrawCenterText(true);
                    chart1.setCenterText("No ProfileData");
                    chart2.setCenterText("No ProfileData");
                    Description description = new Description();
                    description.setText("No ProfileData");
                    description.setPosition(0,0);
                    chart1.setDescription(description);
                    chart2.setDescription(description);
                    TextView No1= (TextView) findViewById(R.id.noData1);
                    TextView No2= (TextView) findViewById(R.id.noData2);
                    No1.setText("No ProfileData");
                    No2.setText("No ProfileData");

                    progressSweetAlertDialog.dismiss();
                    Toast.makeText(getApplicationContext(),"Something is wrong, Try again later",Toast.LENGTH_LONG).show();

                }





            }

            @Override
            public void onFailure(Call<GraphResponse>call, Throwable t) {
                // Log error here since request failed
                Log.e("das", t.toString());
                progressSweetAlertDialog.dismiss();
            }
        });


    }
}
