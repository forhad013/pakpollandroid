package invertemotech.com.pakpoll.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.theartofdev.edmodo.cropper.CropImage;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import invertemotech.com.pakpoll.R;
import invertemotech.com.pakpoll.adapter.NavDrawerListAdapter;
import invertemotech.com.pakpoll.fragment.Fragment_home;
import invertemotech.com.pakpoll.fragment.Fragment_oldPoll;
import invertemotech.com.pakpoll.retrofitmodel.ApiClient;
import invertemotech.com.pakpoll.retrofitmodel.ApiInterafce;
import invertemotech.com.pakpoll.retrofitmodel.UpdateResponse;
import invertemotech.com.pakpoll.util.ConnectionDetector;
import invertemotech.com.pakpoll.util.SharePref;
import io.fabric.sdk.android.Fabric;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HomeActivity extends AppCompatActivity implements AdapterView.OnItemClickListener  {

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ImageView mDrawerfooter;
    private ImageButton mDrawerToggle, myProfile, addPoll,notification;

    public int newMessageCounter = 0;

    SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;
    TextView changePicture;

    SharePref sharePref;
    boolean isInternetOn;
    private NavDrawerListAdapter adapter;
    private ArrayList<String> navDrawerItems;
    private Uri mCropImageUri;
    int userid;
    ConnectionDetector cd;
    private ArrayList<Integer> navDrawerItemsImages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TwitterAuthConfig authConfig =  new TwitterAuthConfig("dPS4uCbAWttvyESB2rYXzUhy6", "zs3DEX3yQny2iUPGQxWx9A1B2zN8tBe7thshwp7l3v12bFmTdw");
        Fabric.with(this, new TwitterCore(authConfig), new TweetComposer());
        setContentView(R.layout.activity_home);


        cd = new ConnectionDetector(getApplicationContext());


        isInternetOn = cd.isConnectingToInternet();

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);


        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        mDrawerToggle = (ImageButton) findViewById(R.id.menu);
        myProfile = (ImageButton) findViewById(R.id.profile);
        addPoll = (ImageButton) findViewById(R.id.addPoll);
        notification = (ImageButton) findViewById(R.id.notification);

        sharePref = new SharePref(getApplicationContext());

        userid = Integer.parseInt(sharePref.getshareprefdatastring(SharePref.USERID));
        progressSweetAlertDialog = new SweetAlertDialog(HomeActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);

        Log.e("currentid", userid + "");
        navDrawerItems = new ArrayList<>();
        navDrawerItemsImages = new ArrayList<>();
        navDrawerItems.add("");
        navDrawerItems.add("");
        navDrawerItems.add("Choose Your Topics");
        navDrawerItems.add("Home");
        navDrawerItems.add("Old Polls");
        navDrawerItems.add("Your Suggestion");
        navDrawerItems.add("My Profile");
        navDrawerItems.add("App Stats");
        navDrawerItems.add("Hot Poll");
        navDrawerItems.add("Log Out");


        navDrawerItemsImages.add(0);
        navDrawerItemsImages.add(0);
        navDrawerItemsImages.add(0);
        navDrawerItemsImages.add(R.drawable.ic_home);
        navDrawerItemsImages.add(R.drawable.ic_old_polls);
        navDrawerItemsImages.add(R.drawable.ic_edit);
        navDrawerItemsImages.add(R.drawable.ic_profile);
        navDrawerItemsImages.add(R.drawable.ic_stats);
        navDrawerItemsImages.add(R.drawable.ic_flame);
        navDrawerItemsImages.add(R.drawable.ic_logout);


        adapter = new NavDrawerListAdapter(getApplicationContext(), navDrawerItems, navDrawerItemsImages);


        setTheme();


        mDrawerList.setAdapter(adapter);


        adapter.setCustomObjectListener(new NavDrawerListAdapter.onProfilePicListener() {
            @Override
            public void onProfilePicClicked() {
                Log.e("home", "home");
                if (CropImage.isExplicitCameraPermissionRequired(getApplicationContext())) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA}, CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE);
                } else {
                    CropImage.startPickImageActivity(HomeActivity.this);
                }
            }


        });




        mDrawerToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
                    mDrawerLayout.closeDrawer(mDrawerList);
                } else {
                    mDrawerLayout.openDrawer(mDrawerList);
                }
            }
        });


        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AdminNotificationActivity.class);



                startActivity(intent);
            }
        });


        myProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent intent = new Intent(getApplicationContext(), MyPollActivity.class);

                intent.putExtra("id", userid);

                startActivity(intent);

            }
        });

        addPoll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent intent = new Intent(getApplicationContext(), NewPoll.class);


                startActivity(intent);

            }
        });


        LoadHome();
        mDrawerList.setOnItemClickListener(this);

    }

    @Override
    protected void onResume() {
        adapter = new NavDrawerListAdapter(getApplicationContext(), navDrawerItems, navDrawerItemsImages);


        mDrawerList.setAdapter(adapter);
        adapter.setCustomObjectListener(new NavDrawerListAdapter.onProfilePicListener() {
            @Override
            public void onProfilePicClicked() {
                Log.e("home", "home");
                if (CropImage.isExplicitCameraPermissionRequired(getApplicationContext())) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA}, CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE);
                } else {
                    CropImage.startPickImageActivity(HomeActivity.this);
                }
            }


        });



        if(sharePref.getshareprefdataBoolean(SharePref.FIRSTTIME)){
            if(isInternetOn)
                Log.e("sendCounter", "sendCounter");
            sendCounter();
        }

        if(isInternetOn){
            sendOnlineStatus(1);

            Log.e("firstTime",sharePref.getshareprefdataBoolean(SharePref.FIRSTTIME)+"");
        }

        super.onResume();



    }

    @Override
    protected void onDestroy() {
        if(isInternetOn){
            sendOnlineStatus(0);
        }


        sharePref.setshareprefdataBoolean(SharePref.FIRSTTIME,false);
        super.onDestroy();
    }




    @Override
    @SuppressLint("NewApi")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == AppCompatActivity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage,
            // but we don't know if we need to for the URI so the simplest is to try open the stream and see if we get error.
            boolean requirePermissions = false;
            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {

                // request permissions and handle the result in onRequestPermissionsResult()
                requirePermissions = true;
                mCropImageUri = imageUri;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
            } else {

                //        mCurrentFragment.setImageUri(imageUri);

                Intent intent = new Intent(getApplicationContext(), ImageCropActivity.class);
                intent.putExtra("imageuri", imageUri.toString());
                startActivity(intent);

            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                CropImage.startPickImageActivity(this);
            } else {
                Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
            }
        }
        if (requestCode == CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE) {
            if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // mCurrentFragment.setImageUri(mCropImageUri);

                Intent intent = new Intent(getApplicationContext(), ImageCropActivity.class);
                intent.putExtra("imageuri", mCropImageUri.toString());
                startActivity(intent);

            } else {
                Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
            }
        }
    }


    public void LoadHome() {

        Fragment fragment = new Fragment_home();
        FragmentManager mFragmentManager = getSupportFragmentManager();

        FragmentTransaction ft = mFragmentManager.beginTransaction();


        ft.add(R.id.frame_container, fragment);

        ft.commit();

    }


    public void LoadOldPolls() {

        Fragment fragment = new Fragment_oldPoll();
        FragmentManager mFragmentManager = getSupportFragmentManager();

        FragmentTransaction ft = mFragmentManager.beginTransaction();


        ft.add(R.id.frame_container, fragment);

        ft.commit();

    }

    private void displayView(int position) {



        switch (position) {
            case 0:

                break;
            case 1:

                String link= "https://play.google.com/store/apps/details?id="+getApplicationContext().getPackageName();

                        Intent intent6 = new Intent();
                intent6.setAction(Intent.ACTION_SEND);

                intent6.setType("text/plain");
                intent6.putExtra(Intent.EXTRA_TEXT, link);
                startActivity(Intent.createChooser(intent6, "Share"));

                break;


            case 2:

                Intent intent7 = new Intent(getApplicationContext(), TopicsActivitySecond.class);


                startActivity(intent7);


                break;
            case 5:
                showSuggestionBox();

                break;
            case 3:
                LoadHome();

                break;
            case 4:
                LoadOldPolls();

                break;
            case 6:

                Intent intent = new Intent(getApplicationContext(), MyPollActivity.class);

                intent.putExtra("id", userid);
                startActivity(intent);


                break;
            case 7:

                Intent intent1 = new Intent(getApplicationContext(), AppStatsActivity.class);


                startActivity(intent1);


                break;
            case 8:


                Intent intent2 = new Intent(getApplicationContext(), HotPollActivity.class);


                startActivity(intent2);

                break;

            case 9:


                sharePref.setshareprefdataBoolean(SharePref.LOGEDIN, false);


                Intent intent5 = new Intent(getApplicationContext(), RegLoginActivity.class);


                startActivity(intent5);
                finish();

                break;


            default:
                break;
        }


        mDrawerList.setSelection(position);

        mDrawerLayout.closeDrawer(mDrawerList);
    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        // Toast.makeText(getApplicationContext(),"clciked "+i,Toast.LENGTH_LONG).show();
        displayView(i);
    }


    public void showSuggestionBox() {

        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(HomeActivity.this);
        View mView = layoutInflaterAndroid.inflate(R.layout.custom_suggestion_dialog, null);
        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(HomeActivity.this);
        alertDialogBuilderUserInput.setView(mView);

        final EditText userInputDialogEditText = (EditText) mView.findViewById(R.id.userInputDialog);
        alertDialogBuilderUserInput
                .setCancelable(false)
                .setPositiveButton("Send", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {
                        // ToDo get user input here

                        sendSuggestion(userInputDialogEditText.getText().toString());
                    }
                })

                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                dialogBox.cancel();
                            }
                        });

        AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
        alertDialogAndroid.show();
    }

    public void setTheme() {
        Toolbar topPanel = (Toolbar) findViewById(R.id.tool_bar);

        int color = sharePref.getshareprefdata(SharePref.THEMECOLOR);

        topPanel.setBackgroundColor(color);


    }


    public void sendSuggestion(String suggestion) {

        //   String storeId = sharePref.getshareprefdatastring(SharePref.TOKEN);
        String storeId = "2";
        String appid = "d6847a8652cc7cbf9b92f8f634110fff";

        progressSweetAlertDialog.show();

        ApiInterafce apiService =
                ApiClient.getClient().create(ApiInterafce.class);

        Call<UpdateResponse> call = apiService.suggestion(appid, userid, suggestion);
        call.enqueue(new Callback<UpdateResponse>() {
            @Override
            public void onResponse(Call<UpdateResponse> call, Response<UpdateResponse> response) {

                //      Log.e("asd", "Number of movies received: " + response.body().getError()+"");


                boolean error = response.body().getError();

                String msg = response.body().getMessage();

                Log.e("error", error + "");
                Log.e("msg", msg + "");


                if (!error) {

                    Toast.makeText(getApplicationContext(), "Thanks For Your Suggestion", Toast.LENGTH_LONG).show();
                    progressSweetAlertDialog.dismiss();

                } else {

                    Toast.makeText(getApplicationContext(), "Something is wrong, Try again later", Toast.LENGTH_LONG).show();
                    progressSweetAlertDialog.dismiss();
                }


            }

            @Override
            public void onFailure(Call<UpdateResponse> call, Throwable t) {
                // Log error here since request failed
                Log.e("das", t.toString());
                progressSweetAlertDialog.dismiss();
            }
        });


    }



    public void sendCounter() {




        ApiInterafce apiService =
                ApiClient.getClient().create(ApiInterafce.class);

        Call<UpdateResponse> call = apiService.counter( userid);
        call.enqueue(new Callback<UpdateResponse>() {
            @Override
            public void onResponse(Call<UpdateResponse> call, Response<UpdateResponse> response) {

                //      Log.e("asd", "Number of movies received: " + response.body().getError()+"");


                boolean error = response.body().getError();

                String msg = response.body().getMessage();

                Log.e("error", error + "");
                Log.e("msg", msg + "");


                if (!error) {

                   sharePref.setshareprefdataBoolean(SharePref.FIRSTTIME,false);

                } else {

                }


            }

            @Override
            public void onFailure(Call<UpdateResponse> call, Throwable t) {
                // Log error here since request failed

            }
        });


    }


    public void sendOnlineStatus(final int status) {




        ApiInterafce apiService =
                ApiClient.getClient().create(ApiInterafce.class);

        Call<UpdateResponse> call = apiService.setonlinestatus( userid,status);
        call.enqueue(new Callback<UpdateResponse>() {
            @Override
            public void onResponse(Call<UpdateResponse> call, Response<UpdateResponse> response) {

                //      Log.e("asd", "Number of movies received: " + response.body().getError()+"");


                boolean error = response.body().getError();

                String msg = response.body().getMessage();

                Log.e("error", error + "");
                Log.e("msg", msg + "");


                if (!error) {

                    Log.e("online", status + "");
                } else {

                }


            }

            @Override
            public void onFailure(Call<UpdateResponse> call, Throwable t) {
                // Log error here since request failed

            }
        });


    }
}
