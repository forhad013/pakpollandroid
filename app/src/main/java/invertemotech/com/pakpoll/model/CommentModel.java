package invertemotech.com.pakpoll.model;
/**
 * Created by HP on 02/08/2015.
 */
public final class CommentModel {





  public String getTopicID() {
    return topicID;
  }

  public void setTopicID(String topicID) {
    this.topicID = topicID;
  }

  public String getTopicName() {
    return topicName;
  }

  public void setTopicName(String topicName) {
    this.topicName = topicName;
  }

  public String getTopicIcon() {
    return topicIcon;
  }

  public void setTopicIcon(String topicIcon) {
    this.topicIcon = topicIcon;
  }

  public String getTopicImage() {
    return topicImage;
  }

  public void setTopicImage(String topicImage) {
    this.topicImage = topicImage;
  }
  public String topicID;

  public CommentModel(String topicID, String topicName, String topicIcon, String topicImage) {
    this.topicID = topicID;
    this.topicName = topicName;
    this.topicIcon = topicIcon;
    this.topicImage = topicImage;
  }

  public String topicName;
  public String topicIcon;
  public String topicImage;

}




