/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package invertemotech.com.pakpoll.gcm;

import android.content.Context;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import org.json.JSONException;
import org.json.JSONObject;

import invertemotech.com.pakpoll.util.ApplicationClass;
import invertemotech.com.pakpoll.util.SharePref;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";


    SharePref sharePref;

    Context context;



    @Override
    public void onCreate() {

        context = getApplicationContext();

        super.onCreate();
    }



    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.

        String refreshedToken="";
        String gcm ="";


        try {
          refreshedToken = FirebaseInstanceId.getInstance().getToken()+"";
        Log.d(TAG, "Refreshed token: " + refreshedToken);

            if(!refreshedToken.equals("null")) {
                JSONObject jsonObj = new JSONObject(refreshedToken);

                gcm = jsonObj.getString("token");

                Log.e("gcm", gcm);
                sendRegistrationToServer(gcm);
            }

        } catch (JSONException e) {


            gcm =refreshedToken;
            Log.e("gcm", gcm);
            sendRegistrationToServer(gcm);
                    e.printStackTrace();
        }
        //   String msg = FirebaseInstanceId.getInstance().getToken();

    //   String gcmRegCode = getString(R.string.msg_token_fmt, refreshedToken);


       // Log.e("Gcm",gcmRegCode);





        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.

    }
    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {

         sharePref = new SharePref(ApplicationClass.getAppContext());

        sharePref.setshareprefdatastring(SharePref.GCM_REGCODE,token);
        sharePref.setshareprefdataBoolean(SharePref.LOGEDIN,false);


        // TODO: Implement this method to send token to your app server.
    }
}
