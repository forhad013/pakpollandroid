package invertemotech.com.pakpoll.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import invertemotech.com.pakpoll.R;
import invertemotech.com.pakpoll.retrofitmodel.HotPollData;
import invertemotech.com.pakpoll.util.SharePref;


public class HotPollAdapter extends BaseAdapter {





	ArrayList<HotPollData>  hotPollDataArrayList;
	SharePref sharePref;

	private Context context;
	SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss");
	//SimpleDateFormat sdf1 = new SimpleDateFormat ("MMMM d, yyyy | hh:mm a");

	SimpleDateFormat sdf1 = new SimpleDateFormat ("MMMM d, yyyy");


	public HotPollAdapter(Context context,
						  ArrayList<HotPollData> hotPollDataArrayList) {

		// TODO Auto-generated constructor stub
		this.context = context;
		this.hotPollDataArrayList = hotPollDataArrayList;


		sharePref = new SharePref(context);




	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return hotPollDataArrayList.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;


	//	if (view == null) {

			view = LayoutInflater.from(context).inflate(
					R.layout.hot_poll_item, parent, false);

	//	}

		CircleImageView image = (CircleImageView) view.findViewById(R.id.profile_image);

		TextView question = (TextView) view.findViewById(R.id.question);
		TextView profile_name = (TextView) view.findViewById(R.id.name);
		TextView date = (TextView) view.findViewById(R.id.date);
		TextView topics = (TextView) view.findViewById(R.id.category);
		TextView commenter = (TextView) view.findViewById(R.id.lastCommenter);
		TextView comment = (TextView) view.findViewById(R.id.comment);
		TextView totalComment = (TextView) view.findViewById(R.id.commentNumber);

		RelativeLayout commentLayout = (RelativeLayout) view.findViewById(R.id.commentLayout);

		question.setText(hotPollDataArrayList.get(position).getQuestion());
		profile_name.setText(hotPollDataArrayList.get(position).getName());
		date.setText(hotPollDataArrayList.get(position).getCreatedAt());
		topics.setText(hotPollDataArrayList.get(position).getTopics());
		commenter.setText(hotPollDataArrayList.get(position).getCommentedByName());
		comment.setText(hotPollDataArrayList.get(position).getComment());
		totalComment.setText(hotPollDataArrayList.get(position).getTotalcomments()+"");

		if(hotPollDataArrayList.get(position).getTotalcomments()==0){
			commentLayout.setVisibility(View.GONE);
		}else{
			commentLayout.setVisibility(View.VISIBLE);
		}



		int color = sharePref.getshareprefdata(SharePref.THEMECOLOR);


		DisplayImageOptions defaultOptions;

		defaultOptions = new DisplayImageOptions.Builder()
				.cacheInMemory(true)
				.cacheOnDisk(true)
				.build();
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
				.defaultDisplayImageOptions(defaultOptions)
				.build();
		ImageLoader.getInstance().init(config);


		String imageUrl=hotPollDataArrayList.get(position).getPicture();

		ImageLoader.getInstance().displayImage(imageUrl, image, defaultOptions);

		return view;
	}

}