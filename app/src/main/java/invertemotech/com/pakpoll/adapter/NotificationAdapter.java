package invertemotech.com.pakpoll.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import invertemotech.com.pakpoll.R;
import invertemotech.com.pakpoll.retrofitmodel.Notifications;


public class NotificationAdapter extends BaseAdapter {


	String remainingTime;
	Calendar calendar;

	ArrayList<Notifications> NotifIcationArrayList;

	private Context context;
	SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss");
	//SimpleDateFormat sdf1 = new SimpleDateFormat ("MMMM d, yyyy | hh:mm a");

	SimpleDateFormat sdf1 = new SimpleDateFormat ("MMMM d, yyyy");

	Date currentTime;
	Date date;
	public NotificationAdapter(Context context,
							   ArrayList<Notifications> NotifIcationArrayList) {

		// TODO Auto-generated constructor stub
		this.context = context;
		this.NotifIcationArrayList = NotifIcationArrayList;

		Calendar now = Calendar.getInstance();

		currentTime = now.getTime();


		Log.e("cureen",currentTime+"");


	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return NotifIcationArrayList.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;


	//	if (view == null) {

			view = LayoutInflater.from(context).inflate(
					R.layout.notication_item, parent, false);
	//	}

		TextView notification = (TextView) view.findViewById(R.id.notification);
		TextView time = (TextView) view.findViewById(R.id.time);
		TextView title = (TextView) view.findViewById(R.id.title);

		notification.setText(NotifIcationArrayList.get(position).getNotifytext());
		title.setText(NotifIcationArrayList.get(position).getTitle());


		try {
			  date = sdf.parse(NotifIcationArrayList.get(position).getCreatedAt());
		} catch (ParseException e) {
			e.printStackTrace();
		}


		Calendar c = Calendar.getInstance();

		c.setTime(date);
		c.setTimeZone(TimeZone.getDefault());

		Log.e("Date",date+"");

		String s=  getCurrentTimezoneOffset();
		s= s.replace("G","");
		s= s.replace("M","");
		s= s.replace("T","");


		boolean hasPlusSign = s.contains("+");

		// Log.e("asd",hasPlusSign+"");
		if(hasPlusSign){
			s= s.replace("+","");
			String[] parts = s.split(":");
			c.add(Calendar.HOUR,Integer.parseInt(parts[0]));
			c.add(Calendar.MINUTE,Integer.parseInt(parts[1]));
		}else{
			s=   s.replace("-","");
			String[] parts = s.split(":");
			int k = - Integer.parseInt(parts[0]);
			int j = - Integer.parseInt(parts[1]);

			c.add(Calendar.HOUR,k);
			c.add(Calendar.MINUTE,j);
		}

		date = c.getTime();

		long diff = currentTime.getTime() - date.getTime();


		long diffMinutes = diff / (60 * 1000) % 60;
		long diffHours = diff / (60 * 60 * 1000) % 24;
		long diffDays = diff / (24 * 60 * 60 * 1000);
		remainingTime = "";
		if (diffDays != 0) {

			if(diffDays==1){

				remainingTime = diffDays + " day ago ";
			}else {
				remainingTime = diffDays + " days ago";
			}
		}
		else if (diffHours != 0) {
			if(diffMinutes>40){
				diffHours++;
			}
			if(diffHours==1){
				remainingTime =  diffHours + " hour ago";
			}else {
				remainingTime =  diffHours + " hours ago";
			}

		}
		else if (diffMinutes != 0) {
			if(diffMinutes==1){
				remainingTime =  diffMinutes + " minute ago";
			}else {
				remainingTime =  diffMinutes + " minutes ago";
			}

		}

		time.setText(remainingTime);

		return view;
	}
	public String getCurrentTimezoneOffset() {

		TimeZone tz = TimeZone.getDefault();
		Calendar cal = GregorianCalendar.getInstance(tz);
		int offsetInMillis = tz.getOffset(cal.getTimeInMillis());

		String offset = String.format("%02d:%02d", Math.abs(offsetInMillis / 3600000), Math.abs((offsetInMillis / 60000) % 60));
		offset = "GMT"+(offsetInMillis >= 0 ? "+" : "-") + offset;

		return offset;
	}
}