package invertemotech.com.pakpoll.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import invertemotech.com.pakpoll.R;
import invertemotech.com.pakpoll.util.SharePref;

public class NavDrawerListAdapter extends BaseAdapter {
	
	private Context context;
	private ArrayList<String> navDrawerItems;

	private ArrayList<Integer> navDrawerItemsImages;

	public onProfilePicListener callback;

	String userID;
	SharePref sharePref;
	Typeface typeface;
	
	public NavDrawerListAdapter(Context context, ArrayList<String> navDrawerItems, ArrayList<Integer> navDrawerItemsImages){
		this.context = context;
		this.navDrawerItems = navDrawerItems;
		this.navDrawerItemsImages = navDrawerItemsImages;
		sharePref = new SharePref(context);


		userID= sharePref.getshareprefdatastring(SharePref.USERID);
//		typeface = Typeface.createFromAsset(context.getAssets(), "fonts/HelveticaNeueLTStd-Md.otf");

	}

	public interface onProfilePicListener {
		public void onProfilePicClicked();

	}

	public void setCustomObjectListener(onProfilePicListener callback) {
		Log.e("onCus","onCus");
		Log.e("listener",callback+" nm");
		this.callback = callback;


		Log.e("listenercallback",callback+" nm");
	}


	@Override
	public int getCount() {
		return navDrawerItems.size();
	}

	@Override
	public Object getItem(int position) {
		return navDrawerItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}



	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {





		if (convertView == null) {
			if(position==0) {
				convertView = LayoutInflater.from(context).inflate(
						R.layout.drawer_item_image, parent, false);
				CircleImageView changePicture = (CircleImageView)convertView. findViewById(R.id.profile_image);
				TextView txtTitle = (TextView) convertView.findViewById(R.id.title);

				// setCustomObjectListener(callback);

				changePicture.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View view) {
//						Intent intent = new Intent(context, MyPollActivity.class);
//						intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//						intent.putExtra("id",Integer.parseInt(userID));
//						context.startActivity(intent);

						Log.e("callback",callback+"");
						if (callback != null) {
							callback.onProfilePicClicked();
						}


						}
				});

				String name = sharePref.getshareprefdatastring(SharePref.USERNAME);
				String[] firstname = name.split("\\ ");

				txtTitle.setText("Welcome, "+firstname[0]);
				String iconUrl = sharePref.getshareprefdatastring(SharePref.PROFILEPIC);



				if(!iconUrl.equals("new")) {

					DisplayImageOptions defaultOptions;

					defaultOptions = new DisplayImageOptions.Builder()
							.cacheInMemory(true)
							.cacheOnDisk(true)
							.build();
					ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
							.defaultDisplayImageOptions(defaultOptions)
							.build();
					ImageLoader.getInstance().init(config);

					ImageLoader.getInstance().displayImage(iconUrl, changePicture, defaultOptions);

					Log.e("pr", iconUrl);
				}
					else {
					changePicture.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_profile_add));
				}

			}
			else if(position==1){
				convertView = LayoutInflater.from(context).inflate(
						R.layout.drawer_item_share, parent, false);


			}	else if(position==2){
				convertView = LayoutInflater.from(context).inflate(
						R.layout.drawer_item_choose, parent, false);


			}

			else {
				convertView = LayoutInflater.from(context).inflate(
						R.layout.drawer_item, parent, false);
				ImageView imgIcon = (ImageView) convertView.findViewById(R.id.icon);
				TextView txtTitle = (TextView) convertView.findViewById(R.id.title);

				//txtTitle.setTypeface(typeface);

				if(position!=0||position!=1) {
					// imgIcon.setImageResource(navDrawerItemsImages.get(position));
					txtTitle.setText(navDrawerItems.get(position));
					imgIcon.setImageResource(navDrawerItemsImages.get(position));
				}
			}
		}




//


    
        
        return convertView;
	}
	
	

}
