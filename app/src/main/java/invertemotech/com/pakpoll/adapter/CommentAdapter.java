package invertemotech.com.pakpoll.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import invertemotech.com.pakpoll.R;
import invertemotech.com.pakpoll.activity.MyPollActivity;
import invertemotech.com.pakpoll.retrofitmodel.CommentData;


public class CommentAdapter extends BaseAdapter {





	ArrayList<CommentData> commentDataArrayList;

	private Context context;
	SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss");
	//SimpleDateFormat sdf1 = new SimpleDateFormat ("MMMM d, yyyy | hh:mm a");

	SimpleDateFormat sdf1 = new SimpleDateFormat ("MMMM d, yyyy");


	public CommentAdapter(Context context,
						  ArrayList<CommentData> commentDataArrayList) {

		// TODO Auto-generated constructor stub
		this.context = context;
		this.commentDataArrayList = commentDataArrayList;






	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return commentDataArrayList.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;


	//	if (view == null) {

			view = LayoutInflater.from(context).inflate(
					R.layout.comment_item, parent, false);
	//	}

		TextView name = (TextView) view.findViewById(R.id.name);
		TextView comment = (TextView) view.findViewById(R.id.comment);
		CircleImageView circleImageView = (CircleImageView) view.findViewById(R.id.profile_image);


		name.setText(commentDataArrayList.get(position).getName());
		comment.setText(commentDataArrayList.get(position).getComment());

		name.setHovered(true);


		DisplayImageOptions defaultOptions;

		defaultOptions = new DisplayImageOptions.Builder()
				.cacheInMemory(true)
				.cacheOnDisk(true)
				.build();
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
				.defaultDisplayImageOptions(defaultOptions)
				.build();
		ImageLoader.getInstance().init(config);


		String imageUrl=commentDataArrayList.get(position).getPicture();

		ImageLoader.getInstance().displayImage(imageUrl, circleImageView, defaultOptions);



		name.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {


				Intent in = new Intent(context, MyPollActivity.class);

				in.putExtra("id",commentDataArrayList.get(position).getUserid());
				in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(in);
			}
		});




		return view;
	}

}