package invertemotech.com.pakpoll.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import invertemotech.com.pakpoll.R;
import invertemotech.com.pakpoll.retrofitmodel.Question;


public class MyPollAdapter extends BaseAdapter {





	ArrayList<Question> questionArrayList;

	private Context context;
	SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss");
	//SimpleDateFormat sdf1 = new SimpleDateFormat ("MMMM d, yyyy | hh:mm a");

	SimpleDateFormat sdf1 = new SimpleDateFormat ("MMMM d, yyyy");


	public MyPollAdapter(Context context,
						 ArrayList<Question> questionArrayList) {

		// TODO Auto-generated constructor stub
		this.context = context;
		this.questionArrayList = questionArrayList;






	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return questionArrayList.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;


	//	if (view == null) {

			view = LayoutInflater.from(context).inflate(
					R.layout.my_poll_item, parent, false);
	//	}


		TextView poll = (TextView) view.findViewById(R.id.poll);

		poll.setText(questionArrayList.get(position).getQuestion());





		return view;
	}

}