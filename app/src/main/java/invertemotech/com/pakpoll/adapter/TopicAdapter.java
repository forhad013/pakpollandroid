package invertemotech.com.pakpoll.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import invertemotech.com.pakpoll.R;
import invertemotech.com.pakpoll.retrofitmodel.TopicsData;


public class TopicAdapter extends BaseAdapter {


	onItemClickListener callback;
	 LinearLayout background;

	ArrayList<TopicsData> topicModelArrayList;

	private Context mcontext;
	SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss");
	//SimpleDateFormat sdf1 = new SimpleDateFormat ("MMMM d, yyyy | hh:mm a");

	SimpleDateFormat sdf1 = new SimpleDateFormat ("MMMM d, yyyy");

	ArrayList<Boolean> checkArraylist;
	public TopicAdapter(Context mcontext,
						ArrayList<TopicsData> topicModelArrayList,ArrayList<Boolean> checkArraylist) {

		// TODO Auto-generated constructor stub
		this.mcontext = mcontext;
		this.topicModelArrayList = topicModelArrayList;
		this.callback = null;
		this.checkArraylist = checkArraylist;




	}


	public interface onItemClickListener {
		public void onItemClicked(int position);

	}

	public void setCustomObjectListener(onItemClickListener listener) {
		this.callback = listener;
	}


	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return topicModelArrayList.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;




			view = LayoutInflater.from(mcontext).inflate(
					R.layout.topic_item, parent, false);
			background = (LinearLayout) view.findViewById(R.id.backgorund);

		ImageView icon = (ImageView) view.findViewById(R.id.icon);

		TextView title = (TextView) view.findViewById(R.id.title);

		CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkbox);


		RoundedImageView topicsImage = (RoundedImageView) view.findViewById(R.id.topicImage);




		setCustomObjectListener(callback);


		view.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				if(callback!=null){
					callback.onItemClicked(position);
				}else{

					Log.d("TEST", callback+"");
				}


			}
		});


		if(checkArraylist.get(position)){
			background.setBackground(null);
			checkBox.setChecked(true);
		}else{
			checkBox.setChecked(false);
			setBacground(position);
		}

		DisplayImageOptions defaultOptions;

		defaultOptions = new DisplayImageOptions.Builder()
				.cacheInMemory(true)
				.cacheOnDisk(true)
				.build();
		  ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(mcontext)
		.defaultDisplayImageOptions(defaultOptions)
		  .build();
		     ImageLoader.getInstance().init(config);


		String iconUrl=topicModelArrayList.get(position).getIcon().replace("\\","") ;
		String backgroundUrl=topicModelArrayList.get(position).getBackgroundimage().replace("\\","") ;

		ImageLoader.getInstance().displayImage(iconUrl, icon, defaultOptions);
		ImageLoader.getInstance().displayImage(backgroundUrl, topicsImage, defaultOptions);

//		Picasso.with(mcontext).load(iconUrl).into(icon);
//		Picasso.with(mcontext).load(backgroundUrl).into(topicsImage);

		Log.e("iconUrl",iconUrl);
		Log.e("backgroundUrl",backgroundUrl);



//		icon.setImageResource(R.drawable.app_icon);
//		topicsImage.setImageResource(R.drawable.bg1);

		title.setText(topicModelArrayList.get(position).getTitle());

//			view.setOnClickListener(new View.OnClickListener() {
//				@Override
//				public void onClick(View view) {
//					background.setBackground(null);
//					((TopicsActivity) mcontext).checkArraylist.set(position,true);
//				}
//			});
//
//			if(((TopicsActivity) mcontext).checkArraylist.get(position)){
//				background.setBackground(null);
//			}else {
//				background.setBackgroundDrawable(mcontext.getDrawable(R.drawable.round_background_topics));
//			}





		return view;
	}


	public void setBacground(int position){
		if(position%10==1){
			background.setBackgroundResource(R.drawable.round_background_topics);
		}else if(position%10==2){
			background.setBackgroundResource(R.drawable.round_background_topics1);
		}else if(position%10==3){
			background.setBackgroundResource(R.drawable.round_background_topics);
		}
		else if(position%10==4){
			background.setBackgroundResource(R.drawable.round_background_topics1);
		}else if(position%10==5){
			background.setBackgroundResource(R.drawable.round_background_topics);
		}
		else if(position%10==6){
			background.setBackgroundResource(R.drawable.round_background_topics1);
		}else if(position%10==7){
			background.setBackgroundResource(R.drawable.round_background_topics);
		}
		else if(position%10==8){
			background.setBackgroundResource(R.drawable.round_background_topics1);
		}else if(position%10==9){
			background.setBackgroundResource(R.drawable.round_background_topics);
		}
		else if(position%10==0){
			background.setBackgroundResource(R.drawable.round_background_topics1);
		}

	}

}