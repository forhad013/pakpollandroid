package invertemotech.com.pakpoll.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import invertemotech.com.pakpoll.R;


public class SpinnerAdapter extends BaseAdapter {


	onItemClickListener callback;
	 LinearLayout background;

	ArrayList<String> topicsDataArraylist;

	private Context mcontext;
	SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss");
	//SimpleDateFormat sdf1 = new SimpleDateFormat ("MMMM d, yyyy | hh:mm a");

	SimpleDateFormat sdf1 = new SimpleDateFormat ("MMMM d, yyyy");

	ArrayList<Boolean> checkArraylist;
	public SpinnerAdapter(Context mcontext,
						  ArrayList<String> topicsDataArraylist, ArrayList<Boolean> checkArraylist) {

		// TODO Auto-generated constructor stub
		this.mcontext = mcontext;
		this.topicsDataArraylist = topicsDataArraylist;
		this.callback = null;
		this.checkArraylist = checkArraylist;




	}


	public interface onItemClickListener {
		public void onItemClicked(int position);

	}

	public void setCustomObjectListener(onItemClickListener listener) {
		Log.e("onCus","onCus");
		this.callback = listener;
		Log.e("callback",callback+"");
	}


	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return topicsDataArraylist.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;




			view = LayoutInflater.from(mcontext).inflate(
					R.layout.spinner_item_multi, parent, false);
			background = (LinearLayout) view.findViewById(R.id.backgorund);

		final CheckBox checkbox = (CheckBox) view.findViewById(R.id.checkBox);

		TextView title = (TextView) view.findViewById(R.id.title);






	//	setCustomObjectListener(callback);


		view.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				if(!checkArraylist.get(position)){
				if(getChecked()<5) {
					if (callback != null) {
						callback.onItemClicked(position);


					} else {

						Log.d("TEST", callback + "");
					}


					if (checkArraylist.get(position)) {
						checkbox.setChecked(true);
					} else {
						checkbox.setChecked(false);
					}
				}
				}else{
					checkbox.setChecked(false);
					if (callback != null) {
						callback.onItemClicked(position);


					} else {

						Log.d("TEST", callback + "");
					}
				}



			}
		});




		if(checkArraylist.get(position)){
			 checkbox.setChecked(true);
		}else{
			checkbox.setChecked(false);
		}





		title.setText(topicsDataArraylist.get(position).toString());






		return view;
	}

	public int getChecked(){
		int x=0;

		for(int i=0;i<checkArraylist.size();i++){
			if(checkArraylist.get(i)){
				x++;
			}
		}

		Log.e("checked",x+"");
		return  x;
	}


	public void setBacground(int position){
		if(position%10==1){
			background.setBackgroundResource(R.drawable.round_background_topics);
		}else if(position%10==2){
			background.setBackgroundResource(R.drawable.round_background_topics1);
		}else if(position%10==3){
			background.setBackgroundResource(R.drawable.round_background_topics);
		}
		else if(position%10==4){
			background.setBackgroundResource(R.drawable.round_background_topics1);
		}else if(position%10==5){
			background.setBackgroundResource(R.drawable.round_background_topics);
		}
		else if(position%10==6){
			background.setBackgroundResource(R.drawable.round_background_topics1);
		}else if(position%10==7){
			background.setBackgroundResource(R.drawable.round_background_topics);
		}
		else if(position%10==8){
			background.setBackgroundResource(R.drawable.round_background_topics1);
		}else if(position%10==9){
			background.setBackgroundResource(R.drawable.round_background_topics);
		}
		else if(position%10==0){
			background.setBackgroundResource(R.drawable.round_background_topics1);
		}

	}

}