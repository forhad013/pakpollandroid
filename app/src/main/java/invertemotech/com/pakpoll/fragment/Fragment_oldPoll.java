package invertemotech.com.pakpoll.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;
import invertemotech.com.pakpoll.R;
import invertemotech.com.pakpoll.activity.CommentActivity;
import invertemotech.com.pakpoll.activity.GraphActivity;
import invertemotech.com.pakpoll.activity.MyPollActivity;
import invertemotech.com.pakpoll.activity.NewPoll;
import invertemotech.com.pakpoll.retrofitmodel.ApiClient;
import invertemotech.com.pakpoll.retrofitmodel.ApiInterafce;
import invertemotech.com.pakpoll.retrofitmodel.PollData;
import invertemotech.com.pakpoll.retrofitmodel.PollsResponse;
import invertemotech.com.pakpoll.retrofitmodel.UpdateResponse;
import invertemotech.com.pakpoll.util.ConnectionDetector;
import invertemotech.com.pakpoll.util.SharePref;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Fragment_oldPoll extends Fragment {

    TextView moderate, home;

    View first, second;

    int questionId;

    Boolean isInternetPresent = false;
    boolean isFollowed,isLiked;

    JSONObject json;

    ConnectionDetector cd;
    String userId;

    int created_by_id;


    View v;

    SharePref sharePref;

    String created_by_name;
    SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;
    int success;
    String message;

    int numberOfVoteInteger,numberOfFollower;

    boolean hasNext = true;
    PollData data;


    String option1Text;
    String option2Text;
    String option3Text;
    String option4Text;
    ListView list;

    int skip=0;

    Context context;

    CircleImageView profileImage;

    TextView profileName,followers,question,numberOfVote;

    ImageButton commentBtn, statsBtn,likeBtn,userAdd;

    RadioButton option1,option2,option3,option4;

    ProgressBar progressBar1,progressBar2,progressBar3,progressBar4;
    ImageButton nextBtn;
    RelativeLayout layout3,layout4;

    int pageNumber=0;

    int pollId;

    int option1Vote,option2Vote,option3Vote,option4Vote;

    String selectedOption;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Get the view from fragmenttab1.xml
        View view = inflater.inflate(R.layout.fragment_home, container,
                false);
        v=view;

        cd = new ConnectionDetector(getActivity());

        context = getActivity();
        isInternetPresent = cd.isConnectingToInternet();

        data = new PollData();

        sharePref = new SharePref(getActivity());

        profileImage = (CircleImageView) view.findViewById(R.id.profile_image);

        likeBtn = (ImageButton) view.findViewById(R.id.likeBtn);
        userAdd = (ImageButton) view.findViewById(R.id.addUserBtn);

        userId = sharePref.getshareprefdatastring(SharePref.USERID);


        commentBtn = (ImageButton) view.findViewById(R.id.commentBtn);
        statsBtn = (ImageButton) view.findViewById(R.id.statsBtn);

        profileName = (TextView) view.findViewById(R.id.name);
        followers = (TextView) view.findViewById(R.id.follower);
        question = (TextView) view.findViewById(R.id.question);
        numberOfVote = (TextView) view.findViewById(R.id.vote);
        nextBtn = (ImageButton) view.findViewById(R.id.next);

        layout3 = (RelativeLayout) view.findViewById(R.id.layout3);
        layout4 = (RelativeLayout) view.findViewById(R.id.layout4);

        option1 = (RadioButton) view.findViewById(R.id.option1);
        option2 = (RadioButton) view.findViewById(R.id.option2);
        option3 = (RadioButton) view.findViewById(R.id.option3);
        option4 = (RadioButton) view.findViewById(R.id.option4);


        progressBar1 = (ProgressBar) view.findViewById(R.id.progress1);
        progressBar2 = (ProgressBar) view.findViewById(R.id.progress2);
        progressBar3 = (ProgressBar) view.findViewById(R.id.progress3);
        progressBar4 = (ProgressBar) view.findViewById(R.id.progress4);


        progressSweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);

        commentBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), CommentActivity.class);
                intent.putExtra("id",pollId);
                startActivity(intent);
            }
        });

        statsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), GraphActivity.class);
                intent.putExtra("id",pollId);
                startActivity(intent);
            }
        });

        if(isInternetPresent){
            getPOLL();
        }


        likeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(isInternetPresent) {
                    like();
                }else{

                    Toast.makeText(context,"No Internet Connection",Toast.LENGTH_LONG).show();
                }

            }
        });

        userAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isInternetPresent) {


                    //Log.e("isFollw",data.getIsfollowed()+"");
                    if(data.getIsfollowed()==0){
                        follow();
                    }else{
                        unFollow();
                    }

                }else{

                    Toast.makeText(context,"No Internet Connection",Toast.LENGTH_LONG).show();
                }

            }
        });



        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isInternetPresent){

                    if(hasNext) {

                        getPOLL();
                    }else {
                        Toast.makeText(context,"No More Poll",Toast.LENGTH_LONG).show();

                    }
                }else{
                    Toast.makeText(context,"No Internet Connection",Toast.LENGTH_LONG).show();

                }
            }
        });

        option1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isInternetPresent) {
                    selectedOption = "1";
                    vote();
                }else{

                    Toast.makeText(context,"No Internet Connection",Toast.LENGTH_LONG).show();
                }
            }
        });

        option2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(isInternetPresent) {
                    selectedOption = "2";
                    vote();
                }else{

                    Toast.makeText(context,"No Internet Connection",Toast.LENGTH_LONG).show();
                }
            }
        });

        option3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(isInternetPresent) {
                    selectedOption = "3";
                    vote();
                }else{

                    Toast.makeText(context,"No Internet Connection",Toast.LENGTH_LONG).show();
                }
            }
        });

        option4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(isInternetPresent) {
                    selectedOption = "4";
                    vote();
                }else{

                    Toast.makeText(context,"No Internet Connection",Toast.LENGTH_LONG).show();
                }
            }
        });

        profileName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), MyPollActivity.class);
                intent.putExtra("id",created_by_id);
                startActivity(intent);


            }
        });


        setTheme();
        return view;
    }


    public void setTheme(){


        int color = sharePref.getshareprefdata(SharePref.THEMECOLOR);


        followers.setBackgroundColor(color);

    }


    public void getPOLL(){

        progressSweetAlertDialog.show();
        //   String storeId = sharePref.getshareprefdatastring(SharePref.TOKEN);
        String storeId = "2";
        String appid = "d6847a8652cc7cbf9b92f8f634110fff";


        commentBtn.setEnabled(false);
        userAdd.setEnabled(false);
        likeBtn.setEnabled(false);
        statsBtn.setEnabled(false);


        //Log.e("pageNumber",pageNumber+"");

        ApiInterafce apiService =
                ApiClient.getClient().create(ApiInterafce.class);

      //  Call<PollsResponse> call = apiService.getPoll( appid,Integer.parseInt(userId),pageNumber);
        Call<PollsResponse> call = apiService.getOldPoll( appid,Integer.parseInt(userId),pageNumber);

        //Log.e("oa",userId+"");
        //Log.e("page",pageNumber+"");
        call.enqueue(new Callback<PollsResponse>() {
            @Override
            public void onResponse(Call<PollsResponse>call, Response<PollsResponse> response) {

                //      Log.e("asd", "Number of movies received: " + response.body().getError()+"");
         //       Log.e("a",response.body().toString());

                boolean error = response.body().getError();

                String msg = response.body().getMessage();

                //Log.e("error",error+"");
                //Log.e("msg",msg+"");



                if(!error){

                    skip = 1;

                    data = response.body().getPollData();
                    setData();

                    progressSweetAlertDialog.dismiss();

                }else {
                    progressSweetAlertDialog.dismiss();
//                    doneDialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
//                    doneDialog.setTitleText("");
//                    doneDialog.setContentText(msg);
//                    doneDialog.show();

                AlertDialog.Builder builder = new AlertDialog.Builder(context,R.style.MyDialogTheme);
                    builder.setTitle("Sorry!!");

                            builder.setMessage(msg);

                    builder.setPositiveButton(Html.fromHtml("<font color='#008ECA'>See Old Poll</font>"),
                            new DialogInterface.OnClickListener()
                            {
                                public void onClick(DialogInterface dialog, int id)
                                {
                                    dialog.cancel();
                                }
                            });

                    builder.setNegativeButton(Html.fromHtml("<font color='#008ECA'>Create A New Poll</font>"),
                            new DialogInterface.OnClickListener()
                            {
                                public void onClick(DialogInterface dialog, int id)
                                {
                                    context.startActivity(new Intent(context, NewPoll.class));
                                    //dialog.cancel();
                                }
                            });

                    builder.setNeutralButton(Html.fromHtml("<font color='#f05759'>Close</font>"),
                            new DialogInterface.OnClickListener()
                            {
                                public void onClick(DialogInterface dialog, int id)
                                {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alertDialog = builder.create();


                    // show it
                    alertDialog.show();
                    alertDialog.setCancelable(false);
                    option1.setVisibility(View.INVISIBLE);
                    option2.setVisibility(View.INVISIBLE);
                    option3.setVisibility(View.INVISIBLE);
                    option4.setVisibility(View.INVISIBLE);

                }





            }

            @Override
            public void onFailure(Call<PollsResponse>call, Throwable t) {
                // Log error here since request failed
                //Log.e("das", t.toString());
                progressSweetAlertDialog.dismiss();
            }
        });


    }


    public void vote(){


        option1.setEnabled(false);
        option2.setEnabled(false);
        option3.setEnabled(false);
        option4.setEnabled(false);

        //   String storeId = sharePref.getshareprefdatastring(SharePref.TOKEN);
        String storeId = "2";
        String appid = "d6847a8652cc7cbf9b92f8f634110fff";


        ApiInterafce apiService =
                ApiClient.getClient().create(ApiInterafce.class);

        Call<UpdateResponse> call = apiService.vote( appid,Integer.parseInt(userId),questionId,selectedOption);
        call.enqueue(new Callback<UpdateResponse>() {
            @Override
            public void onResponse(Call<UpdateResponse>call, Response<UpdateResponse> response) {

                //      Log.e("asd", "Number of movies received: " + response.body().getError()+"");


                boolean error = response.body().getError();

                String msg = response.body().getMessage();

                //Log.e("error",error+"");
                //Log.e("msg",msg+"");



                if(!error){
                    skip = 0;
                    calculateVote();
                }else {

                    Toast.makeText(context,"Something is wrong, Try again later",Toast.LENGTH_LONG).show();

                }





            }

            @Override
            public void onFailure(Call<UpdateResponse>call, Throwable t) {
                // Log error here since request failed
                //Log.e("das", t.toString());
                progressSweetAlertDialog.dismiss();

                option1.setEnabled(true);
                option2.setEnabled(true);
                option3.setEnabled(true);
                option4.setEnabled(true);
            }
        });


    }


    public void follow(){

        //   String storeId = sharePref.getshareprefdatastring(SharePref.TOKEN);
        String storeId = "2";
        String appid = "d6847a8652cc7cbf9b92f8f634110fff";


        ApiInterafce apiService =
                ApiClient.getClient().create(ApiInterafce.class);

        Call<UpdateResponse> call = apiService.follow(appid,created_by_id,Integer.parseInt(userId));
        call.enqueue(new Callback<UpdateResponse>() {
            @Override
            public void onResponse(Call<UpdateResponse>call, Response<UpdateResponse> response) {

                //      Log.e("asd", "Number of movies received: " + response.body().getError()+"");


                boolean error = response.body().getError();

                String msg = response.body().getMessage();

                //Log.e("error",error+"");
                //Log.e("msg",msg+"");



                if(!error){

                    if(!isFollowed){
                        data.setIsfollowed(1);
                        isFollowed = true;
                        numberOfFollower++;
                        data.setTotalfollowers(numberOfFollower);
                    }else{
                        numberOfFollower--;
                        data.setIsfollowed(0);
                        data.setTotalfollowers(numberOfFollower);
                    }

                  //  setPollByIdData();

                    if(data.getIsfollowed()==0){
                        isFollowed = false;
                        userAdd.setImageResource(R.drawable.ic_user_add);

                    }else{
                        isFollowed = true;
                        userAdd.setImageResource(R.drawable.ic_user_add_active);
                    }


                    followers.setText(numberOfFollower+" Followers");

                }else {

                    Toast.makeText(context,"Something is wrong, Try again later",Toast.LENGTH_LONG).show();

                }





            }

            @Override
            public void onFailure(Call<UpdateResponse>call, Throwable t) {
                // Log error here since request failed
                //Log.e("das", t.toString());

            }
        });


    }

    public void like(){
        if(data.getIsliked()==1){
            likeBtn.setImageResource(R.drawable.ic_fav);
            isLiked = false;
        }else{
            likeBtn.setImageResource(R.drawable.ic_fav_active);
            isLiked = true;
        }
        //   String storeId = sharePref.getshareprefdatastring(SharePref.TOKEN);
        String storeId = "2";
        String appid = "d6847a8652cc7cbf9b92f8f634110fff";


        ApiInterafce apiService =
                ApiClient.getClient().create(ApiInterafce.class);

        //Log.e("userid",userId+"");

        Call<UpdateResponse> call = apiService.liked(appid,questionId,Integer.parseInt(userId));
        call.enqueue(new Callback<UpdateResponse>() {
            @Override
            public void onResponse(Call<UpdateResponse>call, Response<UpdateResponse> response) {

                //      Log.e("asd", "Number of movies received: " + response.body().getError()+"");


                boolean error = response.body().getError();

                String msg = response.body().getMessage();

                //Log.e("error",error+"");
                //Log.e("msg",msg+"");



                if(!error){

                  if(!isLiked){
                      data.setIsliked(1);
                  }else{
                      data.setIsliked(0);
                  }


                    if(data.getIsliked()==0){
                        likeBtn.setImageResource(R.drawable.ic_fav);
                        isLiked = false;
                    }else{
                        likeBtn.setImageResource(R.drawable.ic_fav_active);
                        isLiked = true;
                    }
                    //setPollByIdData();

                }else {
                    if(data.getIsliked()==1){
                        likeBtn.setImageResource(R.drawable.ic_fav);
                        isLiked = false;
                    }else{
                        likeBtn.setImageResource(R.drawable.ic_fav_active);
                        isLiked = true;
                    }
                    Toast.makeText(context,"Something is wrong, Try again later",Toast.LENGTH_LONG).show();

                }





            }

            @Override
            public void onFailure(Call<UpdateResponse>call, Throwable t) {
                // Log error here since request failed
                //Log.e("das", t.toString());
                progressSweetAlertDialog.dismiss();
            }
        });


    }


    public void calculateVote(){


   //     android:button="@android:color/transparent"



        Drawable progressDrawable = getResources().getDrawable(R.drawable.transparent);
        option1.setButtonDrawable(progressDrawable);
        option2.setButtonDrawable(progressDrawable);
        option3.setButtonDrawable(progressDrawable);
        option4.setButtonDrawable(progressDrawable);
//          if(selectedOption.equals("1")){
//            option1Vote++;
//
//         }
//           if(selectedOption.equals("2")){
//            option2Vote++;
//         }
//           if(selectedOption.equals("3")){
//            option3Vote++;
//         }
//           if(selectedOption.equals("4")){
//            option4Vote++;
//         }
//        numberOfVoteInteger++;
//
//        data.setTotalvotes(numberOfVoteInteger);

        numberOfVote.setText(numberOfVoteInteger+" votes");

        int per1,per2,per3,per4;
        per1 =getPercentage(numberOfVoteInteger,option1Vote);
        per2 =getPercentage(numberOfVoteInteger,option2Vote);
        per3 =getPercentage(numberOfVoteInteger,option3Vote);
        per4 =getPercentage(numberOfVoteInteger,option4Vote);

        progressBar1.setProgress(getPercentage(numberOfVoteInteger,option1Vote));
        progressBar2.setProgress(getPercentage(numberOfVoteInteger,option2Vote));
        progressBar3.setProgress(getPercentage(numberOfVoteInteger,option3Vote));
        progressBar4.setProgress(getPercentage(numberOfVoteInteger,option4Vote));




        option1.setText(per1+"%  "+option1Text  );
        option2.setText(per2+"%  "+option2Text );
        option3.setText(per3+"%  "+option3Text );
        option4.setText(per4+"%  "+option4Text );



        data.getPoll().setOption1(per1+"%  "+option1Text  );
        data.getPoll().setOption2(per2+"%  "+option2Text );
        data.getPoll().setOption3(per3+"%  "+option3Text );
        data.getPoll().setOption4(per4+"%  "+option4Text );

        ArrayList<Integer> arr = new ArrayList<>();

        arr.add(per1);
        arr.add(per2);
        arr.add(per3);
        arr.add(per4);

        String maxVote = null;
        int max=0;
        for (int i=0; i<arr.size(); i++){
            if ( max < arr.get(i)) {
                max = arr.get(i);
                maxVote= (i+1)+"";
            }
        }

          progressDrawable = getResources().getDrawable(R.drawable.progress);


        if(!maxVote.equals("0")) {
            if (maxVote.equals("1")) {
                progressBar1.setProgressDrawable(progressDrawable);
            }

            if (maxVote.equals("2")) {
                progressBar2.setProgressDrawable(progressDrawable);
            }

            if (maxVote.equals("3")) {
                progressBar3.setProgressDrawable(progressDrawable);
            }

            if (maxVote.equals("4")) {
                progressBar4.setProgressDrawable(progressDrawable);
            }
        }


    }


    public int getPercentage(int total,int vote){

        int f;
        String percentage;
        double x =0;

        if(vote!=0) {
           x = ((double)vote/total) * 100;
            //Log.e("x",x+"");

//            Log.e("vote",vote+"");
//            Log.e("total",total+"");
            DecimalFormat df = new DecimalFormat("#");

            String s = df.format(x);
            //Log.e("s",s+"");

              f = Integer.parseInt(s+"");


        }else {
            f=0;
        }
        return f;
    }


    public void setData(){

        progressBar1.setProgress(0);
        progressBar2.setProgress(0);
        progressBar3.setProgress(0);
        progressBar4.setProgress(0);


        option1.setEnabled(false);
        option2.setEnabled(false);
        option3.setEnabled(false);
        option4.setEnabled(false);

        Drawable progressDrawable = getResources().getDrawable(R.drawable.radio_btn);
        option1.setButtonDrawable(progressDrawable);
        option2.setButtonDrawable(progressDrawable);
        option3.setButtonDrawable(progressDrawable);
        option4.setButtonDrawable(progressDrawable);


        commentBtn.setEnabled(true);
        userAdd.setEnabled(true);
        likeBtn.setEnabled(true);
        statsBtn.setEnabled(true);




        numberOfVoteInteger =data.getTotalvotes();
        //Log.e("a",data.getTotalpolls()+"");

        numberOfFollower = data.getTotalfollowers();

        profileName.setText(data.getName());
      //  numberOfVote.setText(data.getTotalvotes());
        followers.setText(data.getTotalfollowers()+" Followers");
        numberOfVote.setText(data.getTotalvotes()+" Votes");

        question.setText(data.getPoll().getQuestion());




        DisplayImageOptions defaultOptions;

        defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .defaultDisplayImageOptions(defaultOptions)
                .build();
        ImageLoader.getInstance().init(config);


        String imageUrl=data.getProfilepic();

        ImageLoader.getInstance().displayImage(imageUrl, profileImage, defaultOptions);



        questionId = data.getPoll().getId();

        option1.setText(data.getPoll().getOption1());
        option2.setText(data.getPoll().getOption2());

        String option3String = data.getPoll().getOption3()+"";
        String option4String = data.getPoll().getOption4()+"";

        option1Text = data.getPoll().getOption1();
        option2Text = data.getPoll().getOption2();
        option3Text = data.getPoll().getOption3();
        option4Text = data.getPoll().getOption4();

        created_by_id = data.getPoll().getCreatedBy();

        pollId = data.getPoll().getId();
        created_by_name = data.getName();

        //Log.e("us",userId);
        //Log.e("created_by_id",created_by_id+"");

        if(userId.equals(created_by_id+"")){
            userAdd.setEnabled(false);
        }

        if(!option3String.equals("")) {
            option3.setText(data.getPoll().getOption3());
        }else{
            layout3.setVisibility(View.INVISIBLE);
        }
        if(!option4String.equals("")) {
            option4.setText(data.getPoll().getOption4());
        }else{
            layout4.setVisibility(View.INVISIBLE);
        }


        if(data.getIsliked()==0){
            likeBtn.setImageResource(R.drawable.ic_fav);
            isLiked = false;
        }else{
            likeBtn.setImageResource(R.drawable.ic_fav_active);
            isLiked = true;
        }

        if(data.getIsfollowed()==0){
            isFollowed = false;
            userAdd.setImageResource(R.drawable.ic_user_add);

        }else{
            isFollowed = true;
            userAdd.setImageResource(R.drawable.ic_user_add_active);
        }

      //  Log.e("total",data.getTotalpolls()+"");
       // Log.e("pageNumber",(pageNumber)+"");
//        if(data.getTotalpolls()==(pageNumber+1)){
//
//            hasNext = false;
//        }

        pageNumber  = data.getNextpollid();


        Log.e("pageNumber",pageNumber+"");
        option1Vote = data.getOption1();
        option2Vote = data.getOption2();
        option3Vote = data.getOption3();
        option4Vote = data.getOption4();

        calculateVote();
    }

    public void unFollow(){

        doneDialog = new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE);
        doneDialog.setConfirmText("Unfollow");
        doneDialog.setCancelText("Cancel");
        doneDialog.setTitleText("Unfollow "+created_by_name+" ?");
       doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
           @Override
           public void onClick(SweetAlertDialog sweetAlertDialog) {
               follow();
               sweetAlertDialog.dismiss();
           }
       }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
           @Override
           public void onClick(SweetAlertDialog sweetAlertDialog) {
               sweetAlertDialog.dismiss();
           }
       });
        doneDialog.show();
        doneDialog.setCancelable(true);
    }

}