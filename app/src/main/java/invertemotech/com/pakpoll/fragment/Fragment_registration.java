package invertemotech.com.pakpoll.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import cn.pedant.SweetAlert.SweetAlertDialog;
import invertemotech.com.pakpoll.R;
import invertemotech.com.pakpoll.activity.TopicsActivity;
import invertemotech.com.pakpoll.gcm.MyFirebaseInstanceIDService;
import invertemotech.com.pakpoll.retrofitmodel.ApiClient;
import invertemotech.com.pakpoll.retrofitmodel.ApiInterafce;
import invertemotech.com.pakpoll.retrofitmodel.RegResponse;
import invertemotech.com.pakpoll.util.ConnectionDetector;
import invertemotech.com.pakpoll.util.SharePref;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;


public class Fragment_registration extends Fragment {


	String msg;
	Button submit;
	LoginButton loginButton;
	EditText nameEdt,ageEdt,emailEdt,passwordEdt;
	Spinner gender,province;

	ImageView fbBtn;
	SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;
	String gcmRegCode,emailString,passwordString,provinceString,genderString,nameString,ageString;
	SharePref sharePref;
	boolean isInternetAvailable;
	ConnectionDetector cd;
	Context context;

	String fbSocialID,fbEmail,fbFullname,fbFirstName,fbLastname;
	private CallbackManager callbackManager;
	@Override
	public View onCreateView(LayoutInflater inflater, final ViewGroup container,
							 Bundle savedInstanceState) {
		// Get the view from fragmenttab1.xml

		context = getActivity();
		FacebookSdk.sdkInitialize(getActivity());

		//AppEventsLogger.activateApp(context);
		AppEventsLogger.activateApp(context,"1633846546910844");
		//AppEventsLogger.activateApp(context,R.string.facebook_app_id+"");
		View view = inflater.inflate(R.layout.fragment_registration, container,
				false);

		cd = new ConnectionDetector(context);
		loginButton = (LoginButton)view.findViewById(R.id.login_button);



		loginButton.setReadPermissions("email","public_profile");

		sharePref = new SharePref(getActivity());
		isInternetAvailable = cd.isConnectingToInternet();
		gender = (Spinner) view.findViewById(R.id.gender);
		province = (Spinner) view.findViewById(R.id.province);
		nameEdt = (EditText) view.findViewById(R.id.name);
		ageEdt = (EditText) view.findViewById(R.id.age);
		emailEdt = (EditText) view.findViewById(R.id.email);
		passwordEdt = (EditText) view.findViewById(R.id.password);
		gcmRegCode = sharePref.getshareprefdatastring(SharePref.GCM_REGCODE);
		// getActionBar().setDisplayHomeAsUpEnabled(true);
		// getActionBar().setHomeButtonEnabled(true);
		progressSweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
		progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
		progressSweetAlertDialog.setTitleText("Loading");
		progressSweetAlertDialog.setCancelable(false);

		fbBtn = (ImageView) view.findViewById(R.id.fb_btn);

		LoginManager.getInstance().logOut();

		callbackManager = CallbackManager.Factory.create();



					submit= (Button) view.findViewById(R.id.signup);

		ArrayList genderItem = new ArrayList();
		genderItem.add("Male");
		genderItem.add("Female");

		ArrayAdapter<String> spinAdapter = new ArrayAdapter<String>(context,
				R.layout.spinner_item, genderItem);


		gender.setAdapter(spinAdapter);

		ArrayList ProvinceItem = new ArrayList();
		ProvinceItem.add("Sindh");
		ProvinceItem.add("Punjab");
		ProvinceItem.add("Khyber Pakhtunkhwa");
		ProvinceItem.add("Balochistan");
		ProvinceItem.add("Kashmir");
		ProvinceItem.add("Gilgit-Baltistan");


		ArrayAdapter<String> spinAdapter1 = new ArrayAdapter<String>(context,
				R.layout.spinner_item, ProvinceItem);


		province.setAdapter(spinAdapter1);


		if (gcmRegCode.equals("")) {
			if(isInternetAvailable) {

				MyFirebaseInstanceIDService myFirebaseInstanceIDService = new MyFirebaseInstanceIDService();
				myFirebaseInstanceIDService.onTokenRefresh();


//				String refreshedToken = FirebaseInstanceId.getInstance().getToken();
//				Log.e("re",refreshedToken);

 
			} else{
			new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
					.setTitleText("Oops...")
					.setContentText("No Internet Conncetion")
					.show();
			
		}
		}


		fbBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {


				LoginManager.getInstance().logInWithReadPermissions(getActivity(), Arrays.asList("public_profile", "email"));

			}
		});


		loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
			@Override
			public void onSuccess(LoginResult loginResult) {



				GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
						new GraphRequest.GraphJSONObjectCallback() {
							@Override
							public void onCompleted(JSONObject object, GraphResponse response) {
								try {

									Log.e("fbInfo",object+"" );
									fbSocialID =object.getString("id");
									fbEmail = object.getString("email");
									fbFullname = object.getString("name");

									if(fbFullname.split("\\w+").length>1){

										fbLastname = fbFullname.substring(fbFullname.lastIndexOf(" ")+1);
										fbFirstName = fbFullname.substring(0, fbFullname.lastIndexOf(' '));
									}
									else{
										fbFirstName = fbFullname;
									}



//                                    while(true){
//                                        if(!gcmRegCode.isEmpty()) {
//                                            break;
//                                        }
//
//                                    }
									if(isInternetAvailable) {
										//new AsyncTaskFBlogin().execute();
									}else{
										Toast.makeText(getApplicationContext(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();

									}
//                                    Log.e("fbInfo","Hi, " + fbFirstName +" "+ fbLastname);
//                                    Log.e("fbInfo","Hi, " + object.getString("name") +" "+ object.getString("email"));
//

								} catch(JSONException ex) {

									ex.printStackTrace();
								}
							}
						});
				Bundle parameters = new Bundle();
				parameters.putString("fields", "id,name,email,gender, birthday");
				request.setParameters(parameters);
				request.executeAsync();
			}


			@Override
			public void onCancel() {

			}

			@Override
			public void onError(FacebookException e) {

			}
		});



		submit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				cd = new ConnectionDetector(context);

				isInternetAvailable = cd.isConnectingToInternet();
				gcmRegCode = sharePref.getshareprefdatastring(SharePref.GCM_REGCODE);
				Log.e("gcm",sharePref.getshareprefdatastring(SharePref.GCM_REGCODE));

				emailString = emailEdt.getText().toString();
				passwordString = passwordEdt.getText().toString();
				nameString = nameEdt.getText().toString();
				ageString = ageEdt.getText().toString();
				passwordString = passwordEdt.getText().toString();

				provinceString = province.getSelectedItem().toString();
				genderString = gender.getSelectedItem().toString();






				if (gcmRegCode.equals("")) {

					MyFirebaseInstanceIDService myFirebaseInstanceIDService = new MyFirebaseInstanceIDService();
					myFirebaseInstanceIDService.onTokenRefresh();

				} else {
					if (nullCheck()) {
						if (isValidEmailAddress(emailString)) {
							if (isInternetAvailable) {
								register();

							} else {
								new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
										.setTitleText("Oops...")
										.setContentText("No Internet Connection")
										.show();
							}
						} else {
							new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
									.setTitleText("Oops...")
									.setContentText("Invalid Email Address")
									.show();
						}
						//new AsyncTaskRegValid().execute();

					}
				}


//				Intent intent = new Intent(getActivity(), TopicsActivity.class);
//				startActivity(intent);

			}
		});
		setTheme();
		return view;
	}



	public void setTheme(){


		int color = sharePref.getshareprefdata(SharePref.THEMECOLOR);

		submit.setBackgroundColor(color);

	}


	public void register(){

		progressSweetAlertDialog.show();
		//   String storeId = sharePref.getshareprefdatastring(SharePref.TOKEN);
		String storeId = "2";
		String appid = "d6847a8652cc7cbf9b92f8f634110fff";


		ApiInterafce apiService =
				ApiClient.getClient().create(ApiInterafce.class);

		Call<RegResponse> call = apiService.regitser( appid,nameString,genderString,ageString,provinceString,emailString,passwordString,gcmRegCode);
		call.enqueue(new Callback<RegResponse>() {
			@Override
			public void onResponse(Call<RegResponse>call, Response<RegResponse> response) {

				//      Log.e("asd", "Number of movies received: " + response.body().getError()+"");


				boolean error = response.body().getError();

				String msg = response.body().getMessage();

				Log.e("error",error+"");
				Log.e("msg",msg+"");
				Log.e("msg",response.body().getData()+"");


				if(!error){


					sharePref.setshareprefdatastring(SharePref.USERNAME,nameString);
					sharePref.setshareprefdatastring(SharePref.USERID,response.body().getData().getUserid()+"");

					sharePref.setshareprefdatastring(SharePref.USEREMAIL,emailString+"");
					sharePref.setshareprefdatastring(SharePref.PROFILEPIC,"new");
					sharePref.setshareprefdataBoolean(SharePref.LOGEDIN, true);
					sharePref.setshareprefdatastring(SharePref.USERPASSWWORD,passwordString+"");
					progressSweetAlertDialog.dismiss();


					doneDialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
					doneDialog.setTitleText(msg);
					doneDialog.setConfirmText("Ok");
					doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
						@Override
						public void onClick(SweetAlertDialog sweetAlertDialog) {
							sweetAlertDialog.dismiss();
							Intent intent = new Intent(context, TopicsActivity.class);
							startActivity(intent);
							getActivity().finish();
						}
					});
					doneDialog.show();




				}else {
					progressSweetAlertDialog.dismiss();
					doneDialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
					doneDialog.setTitleText("ERROR");
				    doneDialog.setContentText(msg);
					doneDialog.show();

				}





			}

			@Override
			public void onFailure(Call<RegResponse>call, Throwable t) {
				// Log error here since request failed
				Log.e("das", t.toString());
				progressSweetAlertDialog.dismiss();
			}
		});


	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		callbackManager.onActivityResult(requestCode, resultCode, data);
	}
	public boolean nullCheck() {
		boolean flag = false;



		if (!emailEdt.getText().toString().trim().equalsIgnoreCase("")) {
			if (!passwordEdt.getText().toString().trim().equalsIgnoreCase("")) {
				if (!nameEdt.getText().toString().trim().equalsIgnoreCase("")) {
					if (!ageEdt.getText().toString().trim().equalsIgnoreCase("")) {
						return true;
					}else {
						msg = "Please Enter Your Age!";
					}
				} else {
					msg = "Please Enter Your Name!";
				}

			} else {
				msg = "Please Enter Password!";
			}

		} else {
			msg = "Please Your email!";
		}


		if(!flag) {
			new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
					.setTitleText("Oops...")
					.setContentText(msg)
					.show();
		}



		return flag;


	}


	public boolean isValidEmailAddress(String email) {

		if(email.contains(".com")) {
			String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
			java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
			java.util.regex.Matcher m = p.matcher(email);
			return m.matches();
		}else{
			return false;
		}
	}

}