package invertemotech.com.pakpoll.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONObject;

import cn.pedant.SweetAlert.SweetAlertDialog;
import invertemotech.com.pakpoll.R;
import invertemotech.com.pakpoll.activity.ForgetPassword;
import invertemotech.com.pakpoll.activity.HomeActivity;
import invertemotech.com.pakpoll.gcm.MyFirebaseInstanceIDService;
import invertemotech.com.pakpoll.retrofitmodel.ApiClient;
import invertemotech.com.pakpoll.retrofitmodel.ApiInterafce;
import invertemotech.com.pakpoll.retrofitmodel.LoginResponse;
import invertemotech.com.pakpoll.util.ConnectionDetector;
import invertemotech.com.pakpoll.util.SharePref;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Fragment_login extends Fragment {

	TextView moderate, home;

	View first, second;
	private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	Boolean isInternetPresent = false;

	ConnectionDetector cd;
	int success;
	String message;

	JSONObject json;
	EditText emailTxt,passwordtxt;
	SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;
	String gcmRegCode,emailString,passwordString;

	SharePref sharePref;

	Context context;
	boolean isInternetAvailable;
	ListView list;

	Button signIn;

	String msg;
	TextView forgotPassword;



	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		// Get the view from fragmenttab1.xml
		View view = inflater.inflate(R.layout.fragment_login, container,
				false);


		context = getActivity();

		sharePref = new SharePref(context);

		emailTxt = (EditText) view.findViewById(R.id.email);
		passwordtxt = (EditText) view.findViewById(R.id.password);
		signIn = (Button) view.findViewById(R.id.login);

		forgotPassword = (TextView) view.findViewById(R.id.forget);
		cd = new ConnectionDetector(context);

		try{
			emailTxt.setText(sharePref.getshareprefdatastring(SharePref.USEREMAIL));
		}catch (Exception e){

		}


		isInternetAvailable = cd.isConnectingToInternet();
		gcmRegCode = sharePref.getshareprefdatastring(SharePref.GCM_REGCODE);

		progressSweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
		progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
		progressSweetAlertDialog.setTitleText("Loading");
		progressSweetAlertDialog.setCancelable(false);

		if (gcmRegCode.equals("")) {
			if(isInternetAvailable) {

				MyFirebaseInstanceIDService myFirebaseInstanceIDService = new MyFirebaseInstanceIDService();
				myFirebaseInstanceIDService.onTokenRefresh();


			} else{
				new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
						.setTitleText("Oops...")
						.setContentText("No Internet Conncetion")
						.show();

			}
		}

		forgotPassword.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				Intent intent = new Intent(context, ForgetPassword.class);
				startActivity(intent);

			}
		});

		setTheme();

		signIn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				emailString = emailTxt.getText().toString();
				passwordString = passwordtxt.getText().toString();
				gcmRegCode = sharePref.getshareprefdatastring(SharePref.GCM_REGCODE);

				if (isInternetAvailable) {
					if (gcmRegCode.equals("")) {
						MyFirebaseInstanceIDService myFirebaseInstanceIDService = new MyFirebaseInstanceIDService();
						myFirebaseInstanceIDService.onTokenRefresh();
					} else {
						if (nullCheck()) {

							login();
						}
					}

				}else{
					new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
							.setTitleText("Oops...")
							.setContentText("No Internet Conncetion")
							.show();
				}
			}
		});



		return view;
	}



	public void setTheme(){


		int color = sharePref.getshareprefdata(SharePref.THEMECOLOR);

		signIn.setBackgroundColor(color);

	}

	public void login(){

		progressSweetAlertDialog.show();
		//   String storeId = sharePref.getshareprefdatastring(SharePref.TOKEN);
		String storeId = "2";
		String appid = "d6847a8652cc7cbf9b92f8f634110fff";


		ApiInterafce apiService =
				ApiClient.getClient().create(ApiInterafce.class);

		Call<LoginResponse> call = apiService.login( appid,emailString,passwordString,gcmRegCode);
		call.enqueue(new Callback<LoginResponse>() {
			@Override
			public void onResponse(Call<LoginResponse>call, Response<LoginResponse> response) {

				//      Log.e("asd", "Number of movies received: " + response.body().getError()+"");


				boolean error = response.body().getError();

				String msg = response.body().getMessage();

				Log.e("error",error+"");
				Log.e("msg",msg+"");
				Log.e("data",response.body().getLoginData()+"");


				if(!error){


					sharePref.setshareprefdatastring(SharePref.USERNAME,response.body().getLoginData().getName());

					sharePref.setshareprefdatastring(SharePref.USEREMAIL,emailString+"");
					sharePref.setshareprefdataBoolean(SharePref.LOGEDIN, true);
					sharePref.setshareprefdatastring(SharePref.USERPASSWWORD,passwordString+"");
					sharePref.setshareprefdatastring(SharePref.USERPROVINCE,response.body().getLoginData().getProvince()+"");
					sharePref.setshareprefdatastring(SharePref.GENDER,response.body().getLoginData().getGender()+"");
					sharePref.setshareprefdatastring(SharePref.AGE,response.body().getLoginData().getAge()+"");
					sharePref.setshareprefdatastring(SharePref.USERID,response.body().getLoginData().getId()+"");
					sharePref.setshareprefdatastring(SharePref.PROFILEPIC,response.body().getLoginData().getPicture()+"");
					sharePref.setshareprefdatastring(SharePref.TOPICS,response.body().getLoginData().getTopics()+"");

					progressSweetAlertDialog.dismiss();


					doneDialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
					doneDialog.setTitleText(msg);
					doneDialog.setConfirmText("Ok");
					doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
						@Override
						public void onClick(SweetAlertDialog sweetAlertDialog) {
							sweetAlertDialog.dismiss();
							Intent intent = new Intent(context, HomeActivity.class);
							startActivity(intent);
							getActivity().finish();
						}
					});
					doneDialog.show();




				}else {
					progressSweetAlertDialog.dismiss();
					doneDialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
					doneDialog.setTitleText(msg);
					// doneDialog.setContentText("Login unsuccessful");
					doneDialog.show();

				}





			}

			@Override
			public void onFailure(Call<LoginResponse>call, Throwable t) {
				// Log error here since request failed
				Log.e("das", t.toString());
				progressSweetAlertDialog.dismiss();
			}
		});


	}



	public boolean nullCheck() {
		boolean flag = false;



		if (!emailTxt.getText().toString().trim().equalsIgnoreCase("")) {
			if (!passwordtxt.getText().toString().trim().equalsIgnoreCase("")) {
						return true;

			} else {
				msg = "Please Enter Password!";
			}

		} else {
			msg = "Please Your email!";
		}


		if(!flag) {
			new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
					.setTitleText("Oops...")
					.setContentText(msg)
					.show();
		}



		return flag;


	}


}