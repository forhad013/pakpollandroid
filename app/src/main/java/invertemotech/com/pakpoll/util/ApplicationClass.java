package invertemotech.com.pakpoll.util;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.util.Log;

/**
 * Created by Mac on 11/22/16.
 */

public class ApplicationClass extends Application {

    private static Context mContext;
    SharePref sharePref;

    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        mContext = getApplicationContext();

        sharePref = new SharePref(mContext);

        sharePref.setshareprefdataBoolean(SharePref.FIRSTTIME,true);


        Log.e("firstTime",sharePref.getshareprefdataBoolean(SharePref.FIRSTTIME)+"");
    }



    public static Context getAppContext() {
        return mContext;
    }


    @Override
    public void onTerminate() {

        sharePref.setshareprefdataBoolean(SharePref.FIRSTTIME,false);

        Log.e("last",sharePref.getshareprefdataBoolean(SharePref.FIRSTTIME)+"");
        super.onTerminate();


    }
}