/* Copyright 2014 Sheldon Neilson www.neilson.co.za
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */
package invertemotech.com.pakpoll.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.SimpleDateFormat;


/* 
 * usage:  
 * DatabaseSetup.init(egActivityOrContext); 
 * DatabaseSetup.createEntry() or DatabaseSetup.getContactNames() or DatabaseSetup.getDb() 
 * DatabaseSetup.deactivate() then job done 
 */

public class Database extends SQLiteOpenHelper {
    static Database instance = null;
    static SQLiteDatabase database = null;
    SimpleDateFormat formatDate = new SimpleDateFormat("hh:mm a");


    static final String DATABASE_NAME = "DB";
    static final int DATABASE_VERSION = 1;

    public static final String REMINDER_TABLE = "reminder";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_PRINT_ID = "print_id";

    public static final String COLUMN_PRODUCT_ID = "pro_id";
    public static final String COLUMN_PRODUCT_NAME = "pro_name";
    public static final String COLUMN_CATEGORY_ID = "cat_id";
    public static final String COLUMN_CATEGORY_NAME = "cat_name";
    public static final String COLUMN_TIME = "time";
    public static final String COLUMN_DATE = "date";
    public static final String COLUMN_LIFETIME = "lifetime";
    public static final String COLUMN_PRINTEDBY = "printedBy";
    public static final String COLUMN_DATE_ND_TIME = "dateNdTime";


    public static void init(Context context) {
        if (null == instance) {
            instance = new Database(context);
        }
    }

    public static SQLiteDatabase getDatabase() {
        if (null == database) {
            database = instance.getWritableDatabase();
        }
        return database;
    }

    public static void deactivate() {
        if (null != database && database.isOpen()) {
            database.close();
        }
        database = null;
        instance = null;
    }


    public static int deleteEntry(int id) {
        return getDatabase().delete(REMINDER_TABLE, COLUMN_ID + "=" + id, null);
    }

    public static int deleteAll() {
        return getDatabase().delete(REMINDER_TABLE, "1", null);
    }


    public Database(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL("CREATE TABLE IF NOT EXISTS " + REMINDER_TABLE + " ( "
                + COLUMN_ID + " INTEGER primary key AUTOINCREMENT, "
                + COLUMN_PRINT_ID + " INTEGER , "
                + COLUMN_PRODUCT_ID + " INTEGER NOT NULL, "
                + COLUMN_PRODUCT_NAME + " TEXT NOT NULL, "
                + COLUMN_CATEGORY_ID + " TEXT NOT NULL, "

                + COLUMN_CATEGORY_NAME + " TEXT NOT NULL, "
                + COLUMN_DATE + " TEXT NOT NULL, "
                + COLUMN_TIME + " TEXT NOT NULL, "
                + COLUMN_PRINTEDBY + " TEXT NOT NULL, "
                + COLUMN_DATE_ND_TIME + " TEXT NOT NULL, "
                + COLUMN_LIFETIME + " TEXT NOT NULL)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + REMINDER_TABLE);
        onCreate(db);
    }

    public void saveReminder(String print_ID, String product_ID, String product_Name, String category_ID, String category_Name, String date, String time, String lifetime, String dateNDtime, String printedby) {
        SQLiteDatabase db = this.getReadableDatabase();


        ContentValues cv = new ContentValues();

        cv.put(COLUMN_PRINT_ID, print_ID);

        cv.put(COLUMN_PRODUCT_ID, product_ID);

        cv.put(COLUMN_CATEGORY_ID, category_ID);
        cv.put(COLUMN_PRODUCT_NAME, product_Name);

        cv.put(COLUMN_CATEGORY_NAME, category_Name);
        cv.put(COLUMN_TIME, time);
        cv.put(COLUMN_DATE, date);
        cv.put(COLUMN_PRINTEDBY, printedby);
        cv.put(COLUMN_DATE_ND_TIME, dateNDtime);
        cv.put(COLUMN_LIFETIME, lifetime);


        db.insert(REMINDER_TABLE, null, cv);

        //db.update("STATUS", cv, ID + "=?", new String[] { "1" });
        db.close();

    }

    public void updateReminder(String ID, String print_ID, String product_ID, String product_Name, String category_ID, String category_Name, String date, String time, String lifetime) {
        SQLiteDatabase db = this.getReadableDatabase();


        ContentValues cv = new ContentValues();
        cv.put(COLUMN_ID, ID);
        cv.put(COLUMN_PRINT_ID, print_ID);

        cv.put(COLUMN_PRODUCT_ID, product_ID);

        cv.put(COLUMN_CATEGORY_ID, category_ID);
        cv.put(COLUMN_PRODUCT_NAME, product_Name);

        cv.put(COLUMN_CATEGORY_NAME, category_Name);
        cv.put(COLUMN_TIME, time);
        cv.put(COLUMN_DATE, date);
        cv.put(COLUMN_LIFETIME, lifetime);

        db.update(REMINDER_TABLE, cv, COLUMN_ID + " =?", new String[]{ID});

        db.close();
    }

    public boolean checkDataAvailable(String printID) {
        Cursor c;
        String searchDate = "";

        boolean status = false;


        SQLiteDatabase db = this.getReadableDatabase();
        String retrieve = "select * from " + REMINDER_TABLE + " where " + COLUMN_PRINT_ID + " = " + printID;

        Log.e("check", retrieve);


        c = db.rawQuery(retrieve, null);
        if (c.getCount() != 0) {

            status = true;
        }

        db.close();

        Log.e("status", status + "");


        return status;

    }

    //
//	public void saveCategory(String name, String type) {
//		SQLiteDatabase db = this.getReadableDatabase();
//
//
//
//
//		ContentValues cv = new ContentValues();
//
//		cv.put(COLUMN_CATEGORY_NAME, name);
//		cv.put(COLUMN_CATEGORY_TYPE, type);
//
//
//
//
////		String ID = "ID";
//		//	 String retrieve = "INSERT INTO myorder(restaurant,food_item,item_number,status) values (reset,'Rangamati,Chittagong','Kaptai Lake is a man made lake in south-eastern Bangladesh. It is located in the Kaptai Upazila under Rangamati District of Chittagong Division. The lake was created as a result of building the Kaptai Dam on the Karnaphuli River, as part of the Karnaphuli Hydro-electric project. The Kaptai Lake average depth is 100 feet  and maximum depth is 490 feet.','22.5009397','92.2136081');" ;
//		// Cursor cursor = db.rawQuery(retrieve, null);
//
//		db.insert(REMINDER_TABLE, null, cv);
//
//		//db.update("STATUS", cv, ID + "=?", new String[] { "1" });
//		db.close();
//
//	}
//
//
//
//	public ArrayList getList() {
//		Cursor c;
//		String searchDate = "";
//
//
//
//		SQLiteDatabase db = this.getReadableDatabase();
//		String retrieve = "select * from "+REMINDER_TABLE ;
//
//		Log.e("check", retrieve);
//
//
//		ArrayList<ReminderModel> reminderModelArrayList;
//		reminderModelArrayList = new ArrayList<>();
//		c = db.rawQuery(retrieve, null);
//		if (c.getCount() != 0) {
//
//			if (c != null) {
//				if (c.moveToFirst()) {
//					do {
//
//
//						String ID = c.getString(c.getColumnIndex(COLUMN_ID));
//						String title = c.getString(c.getColumnIndex(COLUMN_REMINDER_TITLE));
//						String data = c.getString(c.getColumnIndex(COLUMN_REMINDER_DATA));
//						String status = c.getString(c.getColumnIndex(COLUMN_REMINDER_ACTIVE));
//						String time = c.getString(c.getColumnIndex(COLUMN_REMINDER_TIME));
//						String note = c.getString(c.getColumnIndex(COLUMN_REMINDER_NOTE));
//						String option = c.getString(c.getColumnIndex(COLUMN_REMINDER_OPTION));
//
//						String reminder_ID = c.getString(c.getColumnIndex(COLUMN_REMINDER_ID));
//
//
//
//						ReminderModel reminderModel = new ReminderModel(title,data,option,note,time,status,ID,reminder_ID);
//
//
//						reminderModelArrayList.add(reminderModel);
//
//					} while (c.moveToNext());
//
//				}
//
//			}
//
//		}
//
//		db.close();
//
//
//		return reminderModelArrayList;
//
//	}

}